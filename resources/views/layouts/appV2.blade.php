<!DOCTYPE html>
<html lang="en-US" dir="ltr"  ng-app="acestra">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="ACESTRA has evolved into a competent software development company providing versatile, scalable, modular solutions."/>
    <meta name="keywords" content="acestranetworks, acestra, Our Expertise, Portfolio, Retail & Ecommerce, Our Products, IT company"/>
    <meta name="author" content="Suthaharan Tharmalingam"/>
    <!--
    Document Title
    =============================================
    -->
    <title><?php
      if(isset($title)){ echo $title; } ?> - Acestra Networks</title>
    <!--
    Favicons
    =============================================
    -->
<link rel="apple-touch-icon" sizes="57x57" href="/websiteV2/img/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/websiteV2/img/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/websiteV2/img/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/websiteV2/img/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/websiteV2/img/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/websiteV2/img/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/websiteV2/img/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/websiteV2/img/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/websiteV2/img/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/websiteV2/img/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/websiteV2/img/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/websiteV2/img/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/websiteV2/img/fav/favicon-16x16.png">
<link href="https://fonts.googleapis.com/css?family=Roboto|Ubuntu" rel="stylesheet">
<link rel="manifest" href="/websiteV2/img/fav/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/websiteV2/img/fav/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    <!--
    Preloader
    =============================================

    -->
    <style>
      body main {
          opacity: 0;
          filter: alpha(opacity=0);
      }

    </style>
    <script type="text/javascript">
      paceOptions = {
          ajax: false,
          document: true,
          restartOnPushState: false,
          restartOnRequestAfter: false
      }
    </script>
    <link href="/websiteV2/assets/lib/pace/themes/blue/pace-theme-center-simple.css" rel="stylesheet">

    <script data-pace-options='{ "elements": { "selectors": [".selector"] }, "startOnPageLoad": false }' src="/websiteV2/assets/lib/pace/pace.min.js"></script>
    <!--
    Stylesheets
    =============================================

    -->
    <link href="/websiteV2/assets/lib/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="/websiteV2/assets/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/websiteV2/assets/lib/animate-css/animate.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/websiteV2/assets/lib/menuzord/css/menuzord.css">
    <link href="/websiteV2/assets/lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="/websiteV2/assets/lib/owl.carousel.2.1.0/assets/owl.carousel.min.css" rel="stylesheet">
<!--     <link href="/websiteV2/assets/css/style.css" rel="stylesheet"> -->
        <link id="color-scheme" href="/websiteV2/assets/css/colors/custom.css" rel="stylesheet">
        <link href="/websiteV2/assets/css/style.css" rel="stylesheet">
        <link href="/websiteV2/assets/css/particle.css" rel="stylesheet">
        <link href="/websiteV2/assets/css/owl.theme.default.css" rel="stylesheet">
        <!-- <link href="/websiteV2/assets/css/component.css" rel="stylesheet"> -->

         <link href="/websiteV2/css/custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/2.0.0/dark-bottom.css" />
  </head>
  <body data-spy="scroll" data-target=".menuzord" data-offset="60" class="pace-done">
  <main>
      <div id="menuzord" class="menuzord clearfix navigationbar-animation"><a href="/" class="menuzord-brand"><img src="/websiteV2/img/logo.png" class="default-logo" alt="default-logo"/><img src="/websiteV2/img/logo_thumb1.png" class="dark-logo" alt="dark-logo"/></a>
        <ul class="menuzord-menu menuzord-right">
          <li><a href="/">Home</a></li>
          <li><a href="/aboutus">About</a></li>
          <li><a href="#">Services</a>
            <ul class="dropdown">
              <li><a href="/services">Our Proficiency</a></li>
                <li><a href="/portfolio">Portfolio</a></li>
            </ul>
          </li>
          <li><a href="/products">Products</a></li>
          <li><a href="/blog/">Blog</a></li>
          <li><a href="#">Contact</a>
            <ul class="dropdown">
                <li><a href="/quote">Quote</a></li>
                <li><a href="/careers">Careers</a></li>
                 <li><a href="/contact">Contact Us</a></li>
            </ul>
          </li>
           <li><a href="#"><i class="ion-android-globe"></i></a>
            <ul class="dropdown" >
                <li><a href="/english">English</a></li>
                <li><a href="/french">French</a></li>
                 <li><a href="/arabic">Arabic </a></li>
            </ul>
          </li>
        </ul>
      </div>
    @yield('content')
    <section id="footer-widgets" class="footer-widgets section">
          <div class="container">
            <div class="row">
              <div class="col-md-3">
                <div id="textWidget" class="textWidget mountainWidget">
                  <!-- <h4>ABOUT US</h4>
                  <div class="line"></div>
                  <p>ACESTRA has evolved into a competent software development company providing versatile, scalable, modular solutions. We have grown from a ..</p><a href="/aboutus" class="btn btn-mountain-link wow fadeInUp">Read more <span class="ion-ios-arrow-forward"></span></a> -->
                  <h4>INDIA </h4>
                  <div class="line"></div>
                  <ul>
                   <li class="col1 "><strong>Acestra Networks pvt ltd.</strong></li>
                    <li class="col1">2/2 Venkatesa Agraharam Street,</li>
                    <li class="col1">Mylapore, Chennai-600004,India</li>
                    <li class="col1"><i class="ion-ios-telephone"></i>&nbsp;Phone : +91 44 24629069</li>
                    <li class="col1"><i class="ion-printer"></i>&nbsp;Fax : +91 44 45010069</li>
                    <li class="col1"><i class="ion-ios-email"></i>@include('include.mailto')</li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3">
                <div id="linksWidget" class="linkswidget mountainWidget">
                  <h4>CANADA </h4>
                  <div class="line"></div>
                  <ul>
                    <li class="col1 "><strong>Acestra Networks pvt ltd.</strong></li>
                    <li class="col1">104 - 1920 Ellesmere Road, Suite#343,</li>
                    <li class="col1">Toronto, ON M1H 3G1</li>
                    <li class="col1"><i class="ion-ios-telephone"></i>&nbsp;Phone : +1 646 434 6519</li>
                    <li class="col1"><i class="ion-printer"></i>&nbsp;Fax : +1 855 378 0378</li>
                    <li class="col1"><i class="ion-ios-email"></i>@include('include.mailto')</li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3">
                <div id="linksWidget" class="linkswidget mountainWidget">
                  <h4>SRI LANKA </h4>
                  <div class="line"></div>
                 <!--  <p class="col1 "><strong>Acestra Networks pvt ltd.</strong></p> -->

                  <ul>
                    <li class="col1 "><strong>Acestra Networks pvt ltd.</strong></li>
                    <li class="col1">No. 194, Sri Saranankara Road,</li>
                    <li class="col1">Dehiwela-10350</li>
                    <li class="col1"><i class="ion-ios-telephone"></i>&nbsp;Phone : +94 <?php echo config('services.srilanka_number');?></li>
                    <li class="col1"><i class="ion-ios-email"></i>@include('include.mailto')</li>
                  </ul>
                </div>
              </div>
              <div class="col-md-3">
                <div id="flickr-widget" class="flickr-widget mountainWidget">
                  <h4>GET IN TOUCH</h4>
                  <div class="line"></div>
                  <p>Join our mailing list to stay up to date and get notices about our new releases!</p>
                  <div class="form-group __web-inspector-hide-shortcut__ padding-top">
                  <p class="col1"><strong >Subscribe</strong></p>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" class="token">
                  <input type="email" required="required" placeholder="E-Mail Address" class="form-control email_req" />
                  <i  type="button" id="subs"  class="ion-ios-paperplane-outline pull-right submit_icon subscriber_submit"></i>
                  <p class="mesg sub_error" ></p>

                  <!-- <h4 class="btn-list text-center">
                    <button type="button" id="subs" class="subscriber_submit clearfix btn btn-mountain-o btn-o-white btn-round">Submit</button>
                  </h4> -->
              </div>
                </div>
              </div>
            </div>
          </div>
          <a href="#header" class="footer-btn">
            <div class="r45"><span class="ion-chevron-up wow fadeInUp"></span></div></a>
        </section>
      <!-- Footer-->
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <p>Copyrights © 2014- <?php echo date("Y");?> All Rights Reserved by Acestra Networks.<br/><span><a href="/terms">Terms of use </a>|<a href="/privacy">Privacy Policy</a></span></p>
            </div>
            <div class="col-sm-6">
              <div class="social-icons">
              <a href="https://www.facebook.com/Acestra-Network-1560470734248986/" class="footer-social" target="_blank"><span class="ion-social-facebook"></span></a>
              <a href="https://twitter.com/acestranetwork" class="footer-social" target="_blank"><span class="ion-social-twitter"></span></a>
              <a href="https://plus.google.com/u/0/109596073917112772912" class="footer-social"><span class="ion-social-googleplus" target="_blank"></span></a>
              <a href="https://www.linkedin.com/company/acestra-networks-pvt.-ltd." class="footer-social" target="_blank"><span class="ion-social-linkedin"></span></a></div>
            </div>
          </div>
          <!-- /.row-->
        </div>
        <!-- /.container-->
      </footer>
    </main>

    <!--
    JavaScripts
    =============================================
    -->


    <script src="/websiteV2/assets/lib/jquery/dist/jquery.js"></script>
    <script src="/websiteV2/assets/lib/bootstrap/dist/js/bootstrap.js"></script>
    <script src="/websiteV2/assets/lib/jquery/dist/text-typer.js"></script>
    <script src="/websiteV2/assets/lib/jquery-easing/jquery.easing.min.js"></script>
    <script src="/websiteV2/assets/lib/jquery.stellar/jquery.stellar.js"></script>
    <script src="/websiteV2/assets/lib/smoothscroll-for-websites/SmoothScroll.js"></script>
    <script src="/websiteV2/assets/js/jquery.scrollbar.min.js"></script>
<script>
jQuery(document).ready(function(){
    jQuery('.scroll').scrollbar();
});
jQuery(document).ready(function(){
    jQuery('.scroll2').scrollbar();
});

</script>
    <!-- <script src="/websiteV2/assets/js/jquery.nicescroll.min.js"></script>
    <script type="text/javascript">
    $(".scroll").niceScroll({
                cursorcolor: "#4285b0",
                cursoropacitymin: 0.3,
                background: "#cedbec",
                cursorborder: "0",
                autohidemode: true,
                cursorminheight: 30,
      horizrailenabled:false
    });

    $(".scroll2").niceScroll({
                cursorcolor: "#4285b0",
                cursoropacitymin: 0.3,
                background: "#cedbec",
                cursorborder: "0",
                autohidemode: false,
                cursorminheight: 30,
                horizrailenabled:false
    });
    //Activa el nicescroll cuando esta oculto
    $(".scroll").scroll(function(){
      $(".scroll").getNiceScroll().resize();
    }); -->

  </script>
    <script src="/websiteV2/assets/lib/menuzord/js/menuzord.js"></script>
    <script src="/websiteV2/assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="/websiteV2/assets/lib/wow/dist/wow.js"></script>
    <script src="/websiteV2/assets/lib/masonry/dist/masonry.pkgd.min.js"></script>
    <script src="/websiteV2/assets/lib/owl.carousel.2.1.0/owl.carousel.min.js"></script>
    <script src="/websiteV2/assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="/websiteV2/assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="/websiteV2/assets/lib/waypoints/lib/jquery.waypoints.js"></script>
    <script src="/websiteV2/assets/lib/waypoints/lib/shortcuts/inview.js"></script>
        <script src="/websiteV2/assets/lib/Radial-Progress-Bar/radial-progress-bar.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.7/angular.min.js"></script>
    <script type="text/javascript" src="/websiteV2/angular/app.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/2.0.0/cookieconsent.min.js"></script>
    <script type="text/javascript">
      window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":"https://acestranetworks.com/privacy","theme":"dark-bottom"};
  </script>


    <script>
  $(document).ready(function(){
  // Add scrollspy to <body>
  $('body').scrollspy({target: ".well-sticky", offset: 50});

  // Add smooth scrolling on all links inside the navbar
  $("#well a").on('click', function(event) {
  // Make sure this.hash has a value before overriding default behavior
  if (this.hash !== "") {
    // Prevent default anchor click behavior
    event.preventDefault();

    // Store hash
    var hash = this.hash;

    // Using jQuery's animate() method to add smooth page scroll
    // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
    $('html, body').animate({
      scrollTop: $(hash).offset().top
    }, 800, function(){

      // Add hash (#) to URL when done scrolling (default click behavior)
      window.location.hash = hash;
    });
  }  // End if
  });
  });
  </script>
    <!-- Please update the api key with your own key-->
     <script src="https://maps.google.com/maps/api/js?key=AIzaSyDTtEGdjx9jpfeUbUclbTkOzkhFuHyGtfA"></script>
    <script src="/websiteV2/assets/lib/gmaps/gmaps.js"></script>
    <script src="/websiteV2/assets/js/main.js"></script>
     <script>
  $(document).ready(function() {
    //alert('hi');
    $(".refreshCaptcha").click(function(){
        $("img#captchaimg").remove();
        var id = Math.random();
        $('<img id="captchaimg" src="/captcha?id='+id+'" class="img-rounded" alt="captcha_image"/>' ).appendTo("#captchadiv");
        id ='';
    });

    $(".subscriber_submit").click(function(){
      var email=$(".email_req").val();
      var token=$(".token").val();
      if(email==""){
        $(".mesg").css("color","red");
        $(".mesg").html("Please enter email");

      }
      else{
         $.ajax({
        url: "subsciber_post",
        type: "post",
        data: {"email_id":email, "_token":token} ,
        success: function (response) {
          if(response=="success"){

            $(".mesg").css("color","green");
             $(".mesg").html("You have sucessfully subscribed");
             $(".email_req").val("");
             $(".email_req").addClass('success');
              setTimeout(function(){
                   $(".success_email").css("display","none");
              }, 2000);
          }
          else{
                  $(".mesg").css("color","red");
                  $(".mesg").html("email already exists");

          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
      }
    });
    $(".maps_iframe").click(function(){
      $("iframe").css("pointer-events","auto");
    });
});
</script>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-71299802-1', 'auto');
    ga('send', 'pageview');
</script>
  </body>
</html>
