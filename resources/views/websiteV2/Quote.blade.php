@extends('layouts/appV2')
@section('content')

   <section class="padding-top-80">
  <div id="particles-js5" >
            <div class="container" id="innerheader2" >

          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="">
                <h1 class="header-title_new_part_pro header-title_new_part"><span
     class="txt-rotate"
     data-period="2000"
     data-rotate='[ "Quote"]'></span></h1>
                <!-- <p class=" inner-title hidden-xs">Acestra Network provides innovative solutions and top quality services for a great variety of business organizations. We can help you by developing and maintaining software solutions, upgrade technology and migrate services to cloud.</p></h1> -->
              </div>
              <!-- <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Quote</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>Quote</li>
                </ol>
              </div> -->
            </div>
          </div>
        </div></div>
      </section>
      <!-- /#innerheader2-->
      <section id="contact" class="contact-form section">
        <div class="container">
          <div class="row">
            <div class="col-md-12 contact-header sec-head four">
              <h2>Get in touch</h2>
              <p class="wow fadeInUp">If you face any trouble, you can always let our dedicated support team help you. They are ready for you 24/7.</p>

            </div>
            <div class="col-md-6 contact-desc">
              <div data-wow-delay="0.1s" class="call-us contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-telephone"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Call us</h5><span>+91 44 24629069</span>
                </div>
              </div>
              <div data-wow-delay="0.2s" class="address contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-location"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Address</h5><span>2/2 Venkatesa Agraharam Street,<br/>Mylapore,Chennai-600004,India</span>
                </div>
              </div>
              <div data-wow-delay="0.3s" class="email contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-at"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Email us</h5>
                   @include('include.mailto')
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="contact-fields">
             <!--    <form id="contactForm" action="php/contact.php" method="post" name="contactform">
                  Your Name:
                  <div data-wow-delay="0.1s" class="form-group wow fadeInUp">
                    <input id="name" type="text" placeholder="Your Name" name="name" required /="required /" class="form-control"/>
                  </div>
                  Your Email:
                  <div data-wow-delay="0.2s" class="form-group wow fadeInUp">
                    <input id="email" type="email" placeholder="Your Email" name="email" required /="required" class="form-control"/>
                  </div>
                  Contact Number:
                  <div data-wow-delay="0.3s" class="form-group wow fadeInUp">
                    <input id="number" type="number" placeholder="Contact Number" name="number" required="required" class="form-control"/>
                  </div>
                  Message Subject:
                  <div data-wow-delay="0.4s" class="form-group wow fadeInUp">
                    <input id="name" type="text" placeholder="Subject" name="Subject" required="required" class="form-control"/>
                  </div>
                   Expected project start date:
                   <div data-wow-delay="0.5s" class="form-group wow fadeInUp">
                     <label class="radio-inline">
                        <input type="radio" name="optradio">1 Month
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="optradio">3 Months
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="optradio">6 Months
                      </label>
                      <label class="radio-inline">
                        <input type="radio" name="optradio">Not Decided
                      </label>
                    </div>
                    Upload project related doc:
                    <div data-wow-delay="0.6s" class="form-group wow fadeInUp">
                      <input type="file" id="myFile">
                    </div>
                    Message:
                  <div data-wow-delay="0.7s" class="form-group wow fadeInUp">
                    <textarea rows="6" placeholder="Message" name="message" class="form-control"></textarea>
                  </div>
                  Security code:
                  <div data-wow-delay="0.8s" class="form-group wow fadeInUp">
                    <input id="textcode" type="text" placeholder="Security code" name="textcode" required="required" class="form-control"/>
                  </div>

                  <div id="contactFormResponse"></div>
                  <button id="cfsubmit" type="submit" data-wow-delay="0.2s" class="btn btn-mountain btn-block wow fadeInUp">Submit</button>
                </form> -->
                 <form  action="quote_post" method="post" enctype="multipart/form-data" onsubmit="return validateForm()">    <!---->
                  <input type="hidden" name="_token" value="{{ csrf_token() }}" class="token">
         <!--  @if ($errors->has())
          <div class="col-sm-12 ">
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong>Error! &#128546;</strong>
              @foreach ($errors->all() as $error)
              <span class="err_line">

                <span class="sr-only">Errors</span>
                <p class="error_message">{{ $error }} <span class="glyphicon glyphicon-exclamation-sign" style="padding-left:10px;" aria-hidden="true"></span></p>
              </span>
              @endforeach
            </div>
          </div>
          @endif   -->
          @if ($message = Session::get('success'))
          <div class="col-sm-12 ">
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Success  &#128522; </h4>
              {!! $message !!}
            </div>
          </div>
          @endif
          @if ($error = Session::get('alert'))
          <div class="col-sm-12 ">
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Error  &#128546; </h4>
              {!! $error !!}
            </div>
          </div>
          @endif
                 <div class="col-sm-12 highlight" style="display:none">
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Error  &#128546; </h4>
             <span class="highlightmsg"></span>
            </div>
          </div>
        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6">
              <label>Your Name:</label>

              <input type="text" placeholder="Enter Your name minimum 3 char" name="name" class="textbox_align form-control validate{{ $errors->has('name') ? ' has-error' : '' }}" id="name" value="{{ old('name') }}" required pattern="^([a-zA-Z][a-z]{2,20})$" />
              <!-- <div id="error"></div> -->
               @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
              <label>Your Email:</label>

              <input type="text" placeholder="Enter Your Valid Email" name="email" class="textbox_align form-control validate{{ $errors->has('email') ? ' has-error' : '' }}" id="email" value="{{ old('email') }}" required
               pattern="^([a-zA-Z0-9]((\.|\+)?[a-zA-Z0-9-_]+)*@([a-zA-Z0-9](-?[a-zA-Z0-9]+)*(\.))+[a-zA-Z]{2,64})$" />
              <div id="error"></div>
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>


          </div>

            <label>Contact Number:</label>

            <input type="text" placeholder="Enter 10 digit Number" name="phone" class="textbox_align form-control validate {{ $errors->has('phone') ? ' has-error' : '' }}" id="contact" value="{{ old('phone') }}" required pattern="^(([0-9]{10}))$"/>
            @if ($errors->has('phone'))
                  <span class="help-block">
                      <strong>{{ $errors->first('phone') }}</strong>
                  </span>
              @endif



            <label>Message Subject:</label>

            <input type="text" placeholder="Enter message subject" name="subject" class="textbox_align form-control validate {{ $errors->has('subject') ? ' has-error' : '' }}" id="subject" value="{{ old('subject') }}" required pattern="^([a-zA-Z][a-z]{2,20})$"/>
             @if ($errors->has('subject'))
                  <span class="help-block">
                      <strong>{{ $errors->first('subject') }}</strong>
                  </span>
              @endif

            <label>Expected project start date:</label> <br>
            <?php   echo old('expected_project_start_date');  ?>
            <input type="radio" name="expected_project_start_date" value="1 Month" <?php if(old('expected_project_start_date')== "1 Month") { echo 'checked="checked"'; } ?>> <b class="quote_option">1 Month </b>
            <input type="radio" name="expected_project_start_date" value="3 Month" <?php if(old('expected_project_start_date')== "3 Month") { echo 'checked="checked"'; } ?> > <b class="quote_option" >3 Months </b>
            <input type="radio" name="expected_project_start_date" value="6 Month" <?php if(old('expected_project_start_date')== "6 Month") { echo 'checked="checked"'; } ?> > <b class="quote_option">6 Months </b>
            <input type="radio" name="expected_project_start_date" value="Not decided" <?php if(old('expected_project_start_date')== "Not decided") { echo 'checked="checked"'; } ?>> <b class="quote_option">Not Decided </b><br><br>
            <label class="file_browse">Upload project related doc</label>
            <input type="file" size="chars" name="document_name" class="validate" id="document">  <br>
            <label class="file_browse">Message: </label> <br>
            <textarea name="message" class="form-control textbox_align validate{{ $errors->has('message') ? ' has-error' : '' }}" id="message" type="text" required pattern="^([a-zA-Z][a-z]{2,20})$" minlength="10" maxlength="200" />{{old('message')}}</textarea>
            @if ($errors->has('message'))
                  <span class="help-block">
                      <strong>{{ $errors->first('message') }}</strong>
                  </span>
              @endif
           <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group" >
                <label for="captcha">Security code</label>
                <input name="captcha" type="text" class="form-control validate {{ $errors->has('message') ? ' has-error' : '' }}" placeholder="captcha" autocomplete="off" id="captcha">
                @if ($errors->has('captcha'))
                  <span class="help-block">
                      <strong>{{ $errors->first('captcha') }}</strong>
                  </span>
              @endif
              </div>
              <div class="row">
                <div class="col-xs-2">
                  <center><div class="form-group" id="captchadiv">
                    <img src="{{ url('captcha') }}" id="captchaimg" alt="captcha_img" />
                  </div></center>
                </div>
                <div class="col-xs-10 refresh_div text-left">
                  <a href="#" class="refreshCaptcha text-success bg-success">
                    <i class="fa fa-refresh"></i></a>
                  </div>
                </div>
                 <button id="cfsubmit" type="submit" data-wow-delay="0.2s" class="btn btn-mountain btn-block wow fadeInUp">Submit</button>
                <!-- captacha -->
                <!-- <div class="col-lg-6 col-md-6 col-sm-12 captcha_block">
                  <button type="submit" class="btn btn-primary btn-md">Send Message</button>
                  <button type="button" class="btn btn-warning">Reset</button>
                </div>
              </div>  -->

           </div>
         </form>
              </div>
            </div>
          </div>
        </div>
      </section>
      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
       <script src="/websiteV2/assets/lib/jquery/dist/app11.js"></script>

     @stop
