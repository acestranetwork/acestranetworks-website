@extends('layouts/appV2')
@section('content')


      <section class="padding-top-80" >
  <div id="particles-js4" >

        <div class="container" id="innerheader2" class="innerheader innerheader2">
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="inner-title">
                <h1 class="header-title_new_part_pro header-title_new_part"><span
     class="txt-rotate"
     data-period="2000"
     data-rotate='[ "About Us", "We Are Acestra"]'></span></h1>
              </div>
              <!-- <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>About us</li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
        </div>
      </section>

            <!-- /#innerheader2-->
      <section id="video" class="video video1 section">
        <div class="container">
          <div class="">
            <div class="col-md-12 sec-head one">
              <h3 data-wow-delay="0.1s" class="section-title wow fadeInUp">Join hands with Acestra to success</h3>

              <p data-wow-delay="0.2s" class="wow fadeInUp">Since inception, ACESTRA has evolved into a competent software development company providing versatile, scalable, modular solutions. We have grown from a small group of dedicated software programmers to a full scale software development house in both Web and mobile app development .</p>

            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-7 padding-top-20">
                  <p data-wow-delay="0.3s" class="wow fadeInUp">We are a group of Passionate, dedicated and trusted professionals in software & hardware engineering, analysts, programmers, ecommerce professionals, web design professionals and mobile application developers</p> <p data-wow-delay="0.4s" class="wow fadeInUp">We have supported businesses across a broad range of industries that include manufacturing, healthcare, financial services, legal, government, real estate, retail and non-profit organisations. With our strengths and experience in project management, creative design, software technology and ecommerce development, ACESTRA NETWORKS helps its CLIENTS improve performance, gain advanced business focus, higher quality standards and most importantly attain increased profitability.</p>
                </div>
                <div class="col-sm-3 col-sm-push-1">
                  <div class="post-img-wrap">
                     <img src="/websiteV2/img/join.jpg" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
        <!-- /#innerheader1-->
      <section id="cta2" class="cta2">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="cta2-text">
                <h4 class="text-center"><sup><i class="ion-quote"></i></sup>&nbsp;OUR MISSION is to do original, innovative work that is inspired by our clients brand, business objectives and budget and deliver value for money.&nbsp;<sup><i class="ion-quote rotate"></i></sup></h4>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="cta4" class="cta4 bgsec">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="title-subtitle text-center">
               <!-- <img src="/websiteV2/img/team.jpg" class="img-responsive">  -->
                <p>Our goal is to execute your projects using enhanced state of art software. our dedicated and well-equipped team strives to deliver prompt and accurate results.</p>
                 <div style="margin-bottom:25px;" class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
     <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
    <script src="/websiteV2/assets/js/app3.js"></script>

@stop
