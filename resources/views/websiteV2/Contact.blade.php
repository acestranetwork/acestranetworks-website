
@extends('layouts/appV2')
@section('content')

  <section class="padding-top-80" >
    <div  id="particles-js7">
        <div class="container" id="innerheader2" >
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="">
                <h1 class="header-title_new_part_pro header-title_new_part"><span
            class="txt-rotate"
            data-period="2000"
            data-rotate='["Contact"]'></span></h1>
            </div>
              <!-- <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Contact</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>Contact</li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
      </section>
      <!-- /#innerheader2-->
      <section id="contact" class="contact-form section">
        <div class="container">
          <div class="row">
            <div class="col-md-12 contact-header sec-head four">
              <h2>Get in touch</h2>
              <p class="wow fadeInUp">If you face any trouble, you can always let our dedicated support team help you. They are ready for you 24/7.</p>

            </div>
            <div class="col-md-3  col-sm-6 contact-desc contact-desc_1" >
              <h4>INDIA OFFICE</h4>
              <div data-wow-delay="0.1s" class="call-us contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-telephone"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Call us</h5><span>+91 44 24629069</span><span>+91 44 45010069</span>
                </div>
              </div>
              <div data-wow-delay="0.2s" class="address contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-location"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Address</h5><span>2/2 Venkatesa Agraharam Street,<br/>Mylapore,Chennai-600004,India</span>
                </div>
              </div>
              <div data-wow-delay="0.3s" class="email contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-at"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Email us</h5>
                   @include('include.mailto')
                </div>
              </div>
                <h4>CANADA OFFICE</h4>
              <div data-wow-delay="0.1s" class="call-us contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-telephone"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Call us</h5><span>+1 646 434 6519</span>
                </div>
              </div>
              <div data-wow-delay="0.2s" class="address contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-location"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Address</h5><span>1235, McCowan Road,
                     <br/>P.O. Box.4051,McCowan sq.,
                   <br/> Toronto, Ontario M1H 0A4</span>
                </div>
              </div>
              <div data-wow-delay="0.3s" class="email contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-at"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Email us</h5>
                   @include('include.mailto')
                </div>
              </div>
            </div>

            <div class="col-md-3  col-sm-6 contact-desc contact-desc_2"  >
              <h4>SRI LANKA OFFICE</h4>
              <div data-wow-delay="0.1s" class="call-us contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-telephone"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Call us</h5><span>+94 <?php echo config('services.srilanka_number');?></span>
                </div>
              </div>

              <div data-wow-delay="0.2s" class="address contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-location"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Address</h5><span>No. 194, Sri Saranankara Road,<br/>Dehiwela-10350</span>
                </div>
              </div>
              <div data-wow-delay="0.3s" class="email contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-at"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Email us</h5>
                   @include('include.mailto')
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="contact-fields">
               <!--  <form id="contactForm" action="php/contact.php" method="post" name="contactform">
                  <div data-wow-delay="0.1s" class="form-group wow fadeInUp">
                    <input id="name" type="text" placeholder="Your Name" name="name" required="required" class="form-control"/>
                  </div>

                  <div data-wow-delay="0.2s" class="form-group wow fadeInUp">
                    <input id="email" type="email" placeholder="Your Email" name="email" required="required" class="form-control"/>
                  </div>

                  <div data-wow-delay="0.3s" class="form-group wow fadeInUp">
                    <textarea rows="6" placeholder="Message" name="message" class="form-control"></textarea>
                  </div>

                  <div id="contactFormResponse"></div>
                  <button id="cfsubmit" type="submit" data-wow-delay="0.2s" class="btn btn-mountain btn-block wow fadeInUp">Submit</button>
                </form> -->
                <form action="contact_post" method="post" onsubmit="return validateContact()">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
         <!--  @if ($errors->has())
          <div class="col-sm-12 ">
            <div class="alert alert-danger" role="alert">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <strong>Error! &#128546;</strong>
              @foreach ($errors->all() as $error)
              <span class="err_line">

                <span class="sr-only">Errors</span>
                <p class="error_message">{{ $error }} <span class="glyphicon glyphicon-exclamation-sign" style="padding-left:10px;" aria-hidden="true"></span></p>
              </span>
              @endforeach
            </div>
          </div>
          @endif   -->
          @if ($message = Session::get('success'))
          <div class="col-sm-12 ">
            <div class="alert alert-success alert-block">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Success  &#128522; </h4>
              {!! $message !!}
            </div>
          </div>
          @endif
          @if ($error = Session::get('alert'))
          <div class="col-sm-12 ">
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Error  &#128546; </h4>
              {!! $error !!}
            </div>
          </div>
          @endif
          <div class="col-sm-12 highlight" style="display:none">
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Error  &#128546; </h4>
             <span class="highlightmsg"></span>
            </div>
          </div>
          <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6">
              <label>Your Name:</label>
              <input type="text" placeholder="Enter Your name minimum 3 char" name="name" class="textbox_align form-control validate{{ $errors->has('name') ? ' has-error' : '' }}" id="name" value="{{ old('name') }}" required="" pattern="^([a-zA-Z][a-z]{2,20})$">
               @if ($errors->has('name'))
                  <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6">
              <label>Your Email:</label>
              <input type="text" placeholder="Enter Your Valid Email" name="email" class="textbox_align form-control validate{{ $errors->has('email') ? ' has-error' : '' }}" id="email" value="{{ old('email') }}" required="" pattern="^([a-zA-Z0-9]((\.|\+)?[a-zA-Z0-9-_]+)*@([a-zA-Z0-9](-?[a-zA-Z0-9]+)*(\.))+[a-zA-Z]{2,64})$">
              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span>
              @endif
            </div>


          </div>

          <label>Subject:</label>
          <input type="text" placeholder="Enter message subject" name="subject" class="textbox_align form-control validate {{ $errors->has('subject') ? ' has-error' : '' }}" id="subject" value="{{ old('subject') }}" required="" pattern="^([a-zA-Z][a-z]{2,20})$">
            @if ($errors->has('subject'))
                  <span class="help-block">
                      <strong>{{ $errors->first('subject') }}</strong>
                  </span>
              @endif


          <label class="file_browse">Message: </label> <br>
          <textarea name="message" class="textbox_align form-control validate {{ $errors->has('message') ? ' has-error' : '' }}" id="message" required pattern="^([a-zA-Z][a-z]{2,20})$" minlength="10" maxlength="200">{{ old('message') }}</textarea>

          @if ($errors->has('message'))
                  <span class="help-block">
                      <strong>{{ $errors->first('message') }}</strong>
                  </span>
              @endif


          <div class="row">
            <!-- captacha -->
            <div class="col-lg-12 col-md-12 col-sm-12">
              <div class="form-group" >
                <label for="captcha">Security code</label>
                <input name="captcha" type="text" class="form-control validate {{ $errors->has('message') ? ' has-error' : '' }}" placeholder="captcha" autocomplete="off" id="captcha">
                 @if ($errors->has('captcha'))
                  <span class="help-block">
                      <strong>{{ $errors->first('captcha') }}</strong>
                  </span>
              @endif
              </div>
              <div class="row">
                <div class="col-xs-2">
                  <center><div class="form-group" id="captchadiv">
                    <img src="{{ url('captcha') }}" id="captchaimg" class="img-rounded" alt="captcha_img" />
                  </div></center>
                </div>
                <div class="col-xs-10 refresh_div text-left">
                  <a href="#" class="refreshCaptcha text-success bg-success">
                    <i class="fa fa-refresh"></i></a>
                  </div>
                </div>
                 <button id="cfsubmit" type="submit" data-wow-delay="0.2s" class="btn btn-mountain btn-block wow fadeInUp">Submit</button>
                <!-- captacha -->
               <!--  <div class="col-lg-6 col-md-6 col-sm-12 captcha_block">
                  <button type="submit" class="btn btn-primary btn-md">Send Message</button>
                  <button type="reset" class="btn btn-warning">Reset</button>
                </div> -->
              </div>
            </div>
          </form>
              </div>
            </div>

          </div>
        </div>
      </section>
      <section id="contact-map" class="contact-map">
        <div id="map"></div>
      </section>
      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
       <script src="/websiteV2/assets/lib/jquery/dist/app13.js"></script>
     @stop
