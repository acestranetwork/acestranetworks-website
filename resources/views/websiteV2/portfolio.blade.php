@extends('layouts/appV2') @section('content')
<section class="padding-top-80">
	<!-- <div id="particles-js3"> -->
	<div id="particles-js3" style="background-image: linear-gradient(rgba(22, 22, 22, 0.48), rgba(22,22,22,0.5)), url(/websiteV2/img/header/portfolio.jpg);">
		<div class="container" id="innerheader2" class="innerheader innerheader2">
			<div class=" pad-small">
				<div class=" text-center">
					<div class="col-md-12">
						<!-- <h3  class="header-title_new_part_add header-title_new_part ">Products</h3>
 -->
						<h1 class="header-title_new_part_pro header-title_new_part"><span
     class="txt-rotate"
     data-period="2000"
     data-rotate='[ "Portfolio", "Made by Acestra"]'></span></h1>
              <!--   <p class=" inner-title hidden-xs">Acestra Network provides innovative solutions and top quality services for a great variety of business organizations. We can help you by developing and maintaining software solutions, upgrade technology and migrate services to cloud.</p> --> </h1>
						<!--              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
<ol class="pro_bread breadcrumb ">
<li><a href="/">Home</a></li>
<li>Products</li>
</ol>
</div> -->
</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<!-- ////// -->
<section id="portfolio-grid-sortable-gutter" class="portfolio-grid-sortable-gutter section">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<div class="title-subtitle">
					<h2>Our Portfolio</h2>
					<p>We know the business secret for your success</p>
				</div>
				<div class="portfolio-btn-group-wraper">
					<div class="sortable-portfolio-button button-group filter-button-group">
						<button data-filter="*">Show All</button>
						<!-- <button data-filter=".products">Products</button> -->
						<button data-filter=".apps">Apps</button>
						<button data-filter=".web-design">Web Design</button>
						<button data-filter=".logos">Logos</button>
						<!-- <button data-filter=".branding">Logo</button> -->
						<!-- <button data-filter=".banner-design">banner design</button> -->
					</div>
				</div>
			</div>
		</div>
		<div class="portfolio-grid-sortable" id="gr">
			<div class="grid-sizer"></div>
			<div class="portfolio-grid-item banner-design web-design">
				<!-- "portfolio-grid-item grid-item-2x products web-design"> -->
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro1_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/mylporetoday.jpg" class="img-responsive" alt="img05"> <a href="#01" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="col-md-4"><img src="/websiteV2/img/logos/mylporetoday.jpg" class="img-responsive"/></div>
  <div class="col-md-8"><img src="/websiteV2/img/logos/mylporetoday.jpg" class="img-responsive"/></div> -->
				<!-- <div class="portfolio-hover">
<div class="vcenter"><a href="#01" class="inline-popup">
<div class="portfolio-description">
<h5>MylaporeToday</h5>
</div></a></div>
</div> --></div>
			<div class="portfolio-grid-item apps banner-design">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<div class="ribbon"><span>App</span></div>
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro2_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/aceconnect.jpg" class="img-responsive" alt="img05"> <a href="#02" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/aceconnect.jpg" class="img-responsive"/>
     <div class="ribbon"><span>App</span></div>
<div class="portfolio-hover">
<div class="vcenter"><a href="#02" class="inline-popup">
<div class="portfolio-description">
  <h5>aceConnect</h5>
</div></a></div>
</div>
</div> --></div>
			<div class="portfolio-grid-item apps banner-design">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<div class="ribbon"><span>App</span></div>
							<figure>
								<div><img src="/websiteV2/img/products/mylapore-taday-app.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/mylporetoday.jpg" class="img-responsive" alt="img05"> <a href="#12" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/mylporetoday.jpg" class="img-responsive"/>
     <div class="ribbon"><span>App</span></div>
<div class="portfolio-hover">
<div class="vcenter"><a href="#12" class="inline-popup">
<div class="portfolio-description">
  <h5>MylaporeToday</h5>
</div></a></div>
</div>
</div> --></div>
			<div class="portfolio-grid-item  web-design">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro3_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/kollywoodtalkies.jpg" class="img-responsive" alt="img05"> <a href="#03" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/kollywoodtalkies.jpg" class="img-responsive"/>
<div class="portfolio-hover">
<div class="vcenter"><a href="#03" class="inline-popup">
  <div class="portfolio-description">
   <h5>KollywoodTalkies</h5>
 </div></a></div>
</div>
</div> --></div>
			<!--
<div class="portfolio-grid-item  products banner-design">
  <div  class="no-touch">


    <div class="grid cs-style-4">
            <div class="list">
              <figure>
                <div><img src="/websiteV2/img/products/ace_pro4_thumb.jpg" class="img-responsive height1" alt="img05"></div>
                <figcaption>
                  <h3>Safari</h3>
                  <img src="/websiteV2/img/logos/aceclassroom.jpg" class="img-responsive" alt="img05">

                  <a href="#04" class="inline-popup">Take a look</a>
                </figcaption>
              </figure>
            </div></div></div>
<div class="portfolio-thumb"><img src="/websiteV2/img/logos/aceclassroom.jpg" class="img-responsive"/>
<div class="portfolio-hover">
<div class="vcenter"><a href="#04" class="inline-popup">
  <div class="portfolio-description">
    <h5>aceClassRoom</h5>
  </div></a></div>
</div>
</div>
</div> -->
			<div class="portfolio-grid-item  apps branding">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<div class="ribbon"><span>App</span></div>
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro5_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/kalyanseva.jpg" class="img-responsive" alt="img05"> <a href="#05" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/kalyanseva.jpg" class="img-responsive"/>
     <div class="ribbon"><span>App</span></div>
  <div class="portfolio-hover">
    <div class="vcenter"><a href="#05" class="inline-popup">
      <div class="portfolio-description">
        <h5>Kalyanseva</h5>
      </div></a></div>
    </div>
  </div> --></div>
			<div class="portfolio-grid-item apps branding banner-design">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<div class="ribbon"><span>App</span></div>
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro6_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/schoollocater.jpg" class="img-responsive" alt="img05"> <a href="#06" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/schoollocater.jpg" class="img-responsive"/>
       <div class="ribbon"><span>App</span></div>
    <div class="portfolio-hover">
      <div class="vcenter"><a href="#06" class="inline-popup">
        <div class="portfolio-description">
          <h5>SchoolLocator</h5>
        </div></a></div>
      </div>
    </div> --></div>
			<div class="portfolio-grid-item web-design">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro7_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/kalyanseva.jpg" class="img-responsive" alt="img05"> <a href="#07" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/kalyanseva.jpg" class="img-responsive"/>
      <div class="portfolio-hover">
        <div class="vcenter"><a href="#07" class="inline-popup">
          <div class="portfolio-description">
            <h5>Kalyanseva</h5>
          </div></a></div>
        </div>
      </div> --></div>
			<div class="portfolio-grid-item web-design">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<figure>
								<div><img src="/websiteV2/img/products/lanka-matrimony.png" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/langa.jpg" class="img-responsive" alt="img05"> <a href="#14" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/langa.jpg" class="img-responsive"/>
      <div class="portfolio-hover">
        <div class="vcenter"><a href="#14" class="inline-popup">
          <div class="portfolio-description">
            <h5>Kalyanseva</h5>
          </div></a></div>
        </div>
      </div> --></div>
			<div class="portfolio-grid-item apps">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<div class="ribbon"><span>App</span></div>
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro8_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/shoping.jpg" class="img-responsive" alt="img05"> <a href="#08" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/shoping.jpg" class="img-responsive"/>
           <div class="ribbon"><span>App</span></div>
        <div class="portfolio-hover">
          <div class="vcenter"><a href="#08" class="inline-popup">
            <div class="portfolio-description">
              <h5>AceShopping</h5>
            </div></a></div>
          </div>
        </div> --></div>
			<div class="portfolio-grid-item apps branding banner-design">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<div class="ribbon"><span>App</span></div>
							<figure>
								<div><img src="/websiteV2/img/products/fund_app_thumb.jpg" class="img-responsive height1" alt="img06"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/fund_app_logo.jpg" class="img-responsive" alt="img06"> <a href="#15" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/schoollocater.jpg" class="img-responsive"/>
             <div class="ribbon"><span>App</span></div>
          <div class="portfolio-hover">
            <div class="vcenter"><a href="#06" class="inline-popup">
              <div class="portfolio-description">
                <h5>SchoolLocator</h5>
              </div></a></div>
            </div>
          </div> --></div>
			<div class="portfolio-grid-item web-design branding">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<figure>
								<div><img src="/websiteV2/img/products/ace_pro10_thumb.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/kurumpadam.jpg" class="img-responsive" alt="img05"> <a href="#10" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/kurumpadam.jpg" class="img-responsive"/>
            <div class="portfolio-hover">
              <div class="vcenter"><a href="#10"  class="inline-popup">
                <div class="portfolio-description">
                  <h5>KurumpadamTamil</h5>
                </div></a></div>
              </div>
            </div> --></div>
			<!--  <div class="portfolio-grid-item products web-design branding">
            <div class="portfolio-thumb"><img src="/websiteV2/img/products/ace_pro11_thumb.jpg" class="img-responsive"/>
              <div class="portfolio-hover">
                <div class="vcenter"><a href="#11"  class="inline-popup">
                  <div class="portfolio-description">
                    <h5>Surabi spices</h5>
                  </div></a></div>
                </div>
              </div>
            </div> -->
			<div class="portfolio-grid-item products web-design branding">
				<div class="no-touch">
					<div class="grid cs-style-4">
						<div class="list">
							<figure>
								<div><img src="/websiteV2/img/products/acestarealty.jpg" class="img-responsive height1" alt="img05"></div>
								<figcaption>
									<!-- <h3>Safari</h3> --><img src="/websiteV2/img/logos/acerealty.jpg" class="img-responsive" alt="img05"> <a href="#09" class="inline-popup">Take a look</a> </figcaption>
							</figure>
						</div>
					</div>
				</div>
				<!-- <div class="portfolio-thumb"><img src="/websiteV2/img/logos/acerealty.jpg" class="img-responsive"/>
          <div class="portfolio-hover">
            <div class="vcenter"><a href="#09"  class="inline-popup">
              <div class="portfolio-description">
                <h5>acestraRealty</h5>
              </div></a></div>
            </div>
          </div> --></div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo1.png" alt="logo1" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-1" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 1</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo2.png"  alt="logo2" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-2" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 2</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo3.png"  alt="logo3" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-3" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 3</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo4.png"  alt="logo4" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-4" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 4</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo5.png"  alt="logo5" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-5" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 5</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo6.png"  alt="logo6" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-6" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 6</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo7.png"  alt="logo7" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-7" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 7</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo8.png"   alt="logo8" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-8" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 8</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo9.png"  alt="logo9" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-9" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 9</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo10.png"  alt="logo10" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-10" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 10</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo11.png"   alt="logo11" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-11" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 11</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo12.png"  alt="logo12" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-12" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 12</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo13.png"  alt="logo13" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-13" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 13</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo14.png"   alt="logo14" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-14" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 14</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo15.png"  alt="logo15" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-15" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 15</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo16.png"  alt="logo16" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-16" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 16</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo17.png"  alt="logo17" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-17" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 17</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo18.png"  alt="logo18" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-18" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 18</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo19.png"  alt="logo19" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-19" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 19</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo20.png"  alt="logo20" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-20" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 20</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="portfolio-grid-item logos">
				<div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo21.png"  alt="logo21" class="img-responsive height1" />
					<div class="portfolio-hover">
						<div class="vcenter">
							<a href="#logo-21" class="inline-popup">
								<div class="portfolio-description">
									<h5>Logo - 21</h5> </div>
							</a>
						</div>
					</div>
				</div>
			</div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo22.png"  alt="logo22" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-22" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 22</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo23.png"  alt="logo23" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-23" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 23</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo24.png"  alt="logo24"class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-24" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 24</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo25.png"  alt="logo25" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-25" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 25</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo26.png"  alt="logo26" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-26" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 26</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo27.png"  alt="logo27" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-27" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 27</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo28.png"  alt="logo28" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-28" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 28</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo29.png"  alt="logo29" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-29" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 29</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
      <div class="portfolio-grid-item logos">
        <div class="portfolio-thumb"><img src="/websiteV2/img/logos/logo30.png"  alt="logo30" class="img-responsive height1" />
          <div class="portfolio-hover">
            <div class="vcenter">
              <a href="#logo-30" class="inline-popup">
                <div class="portfolio-description">
                  <h5>Logo - 30</h5> </div>
              </a>
            </div>
          </div>
        </div>
      </div>
		</div>
	</div>
</section>
<div class="mfp-content">
	<div id="15" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/products/fund_app.gif"  alt="fund_app" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-1" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo1.png" alt="logo1" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-2" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo2.png" alt="logo2" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-3" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo3.png" alt="logo3" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-4" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo4.png" alt="logo4" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-5" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo5.png" alt="logo5" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-6" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo6.png" alt="logo6" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-7" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo7.png" alt="logo7" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-8" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo8.png" alt="logo8" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-9" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo9.png" alt="logo9" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-10" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo10.png" alt="logo10" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-11" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo11.png" alt="logo11" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-12" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo12.png" alt="logo12" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-13" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo13.png" alt="logo13" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-14" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo14.png" alt="logo14" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-15" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo15.png" alt="logo15" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-16" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo16.png" alt="logo16" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-17" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo17.png" alt="logo17" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-18" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo18.png" alt="logo18" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-19" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo19.png" alt="logo19" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-20" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo20.png" alt="logo20" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-21" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo21.png" alt="logo21" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-22" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo22.png" alt="logo22" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-23" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo23.png" alt="logo23" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-24" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo24.png" alt="logo24" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-25" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo25.png" alt="logo25" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-26" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo26.png" alt="logo26" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-27" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo27.png" alt="logo27" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-28" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo28.png" alt="logo28" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-29" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo29.png" alt="logo29" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="mfp-content">
	<div id="logo-30" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row">
					<div class="col-md-8 col-md-offset-2 col-xs-10 col-xs-offset-1">
						<div class=""><img src="/websiteV2/img/logos/logo30.png" alt="logo30" class="img-responsive"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="01" class="mfp-hide">
	<div class="img-lightbox">
		<div class="container-fluid">
			<div class="row no-gutter">
				<div class="col-md-9">
					<div class="img-wrapper"><img src="/websiteV2/img/products/img-gif.gif"  alt="Mylapore Today" class="img-responsive" /></div>
				</div>
				<div class="col-md-3">
					<div class="img-sidebar">
						<div class="sidebar-header">
							<div class="img-title">
								<h4>MylaporeToday</h4> </div>
							<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
						<div class="sidebar-content">
							<p>Mylapore Today is an exclusive website which provides news ,happenings,civic issues,tourism details and much more information about mylapore.It is regularly updated and thus it becomes very convenient for new visitors to avail and to be aware about Mylapore and its neighborhood.</p>
						</div>
						<div class="img-meta">
							<ul>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Latest news on Mylapore.</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Detailed directory information.</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get your business Classified.</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Civic issues regularly updated.</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get to know in and around Mylapore.</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Top tourist destinations in mylapore.</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Whats special and happening at the moment in mylapore.</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="02" class="mfp-hide">
	<div class="img-lightbox">
		<div class="container-fluid">
			<div class="row no-gutter">
				<div class="col-md-9">
					<div class="img-wrapper"><img src="/websiteV2/img/products/ace_pro2.gif" alt="aceConnect" class="img-responsive" /></div>
				</div>
				<div class="col-md-3">
					<div class="img-sidebar">
						<div class="sidebar-header">
							<div class="img-title">
								<h4>aceConnect</h4> </div>
							<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
						<div class="sidebar-content">
							<p>aceConnect is an app built to facilitate communication between educational institutions and parents.Most parents have demanding careers and have constant worry about their child's activities and progress at school. aceConnect relieves parents from this stress as all information regarding their child reaches them instantly. It seamlessly syncs your data between devices, allowing you to use the app even when you are offline.</p>
						</div>
						<div class="img-meta">
							<ul>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Announcements</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Calendar</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Communication/home work</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Special moments/Albums</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Leave Request</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Attendance</span></li>
								<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Fee particulars</span></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="03" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/kollywoodtalkies.gif" alt="KollywoodTalkies" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>KollywoodTalkies</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>Kollywood Talkies is an 24 x 7 cine bites entertainment web portal which gives the attention and interest of an audience or gives pleasure and delight. It can be an idea or a task, but is more likely to be one of the activities or events that can be viewed on day to day basis over thousands and millions of people across the country and for the purpose of keeping an audience's attention.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Entertainment 24x7</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Cine Media Marketing & Promotions</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Digital Engagement</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Advertisement & Media Analytics</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Cine PR</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Hundreds of Reports</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="04" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/ace-class-room.gif" alt="aceClassRoom" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>aceClassRoom</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>aceClassRoom is a highly interactive web based education management system that provides a complete market leading solution for modern day school administration and management. We recognize the need for a solution that bridges the gap between the School, Parents and Students. It combines the evolution of software and the internet for the purpose of improving education by fostering an environment for any time access, monitoring, communication and secure data exchange</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Easy to use, simple to navigate</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Intuitive dashboard</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Remote monitoring, assistance</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Secure and reliable</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Multi-user system</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Regular backup</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;24 * 7 availability</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Hundreds of Reports</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="05" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/kalyanseva.gif" alt="Kalyanseva" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>Kalyanseva</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>Kalyanseva is one of the most sought after and trustworthy matrimony website in Srilanka.We are always on the brink to help you find the most suitable life partner,by providing a very safe and user-friendly match making venture.</p>
								<p> We at Kalyanseva understand and respect the roots of Indian astrology to the core ,hence we make sure that the couple have horoscope compatiblity for thier well-being and happy long married life. </p>
								<p> In addition to helping you find the best matrimonial profile ,we also strive hard to cater to all the indispensible services which forms a vital part of a Wedding, such as suggestions on Wedding photography,Catering and Hall arrangements.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Creating your credible profile is absolutely free!</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Once you register,feel free to Login and search for profiles that suit your interest.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Ola..we provide horoscope matching on your shortlisted profiles.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get to know who has reviewed your profile and has shown interest in you!</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Constantly get updates about profiles that sync with your fascination.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Sit back ,relax...all your conversations keep happening with utmost privacy.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Got lucky and found your soul-mate ,now we help you with the rest of services.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;If you are a foriegn client,leave your worries to us;right from accomodation to food,and transport ,we cater to the best of our abilities.Beleive in us, we help you find the best life partner.</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="06" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/school-locater.gif" alt="SchoolLocater" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>SchoolLocator</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>SchoolLocater App is a handy app for Parents and Educational researchers or anyone who is looking for information about a good school in and around your location. Latest announcements and news updates are regularly flashed to get up to the min information on schools. Since it also provides necessary information about the chosen school, including reviews and ratings, it helps us to fine tune our search and help select the best among the preferred few.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Search more than 1000 schools in chennai which includes State ,Matric, CBSE, Montessori etc</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get schools listed name-wise, area-wise or pincode-wise.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Map and get directions to school from your current location.</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="07" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/kalyanseva-web.gif" alt="kalyanseva-web" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>Kalyanseva</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>Kalyanseva is one of the most sought after and trustworthy matrimony website in Srilanka.We are always on the brink to help you find the most suitable life partner,by providing a very safe and user-friendly match making venture.</p>
								<p> We at Kalyanseva understand and respect the roots of Indian astrology to the core ,hence we make sure that the couple have horoscope compatiblity for thier well-being and happy long married life. </p>
								<p> In addition to helping you find the best matrimonial profile ,we also strive hard to cater to all the indispensible services which forms a vital part of a Wedding, such as suggestions on Wedding photography,Catering and Hall arrangements.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Creating your credible profile is absolutely free!</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Once you register,feel free to Login and search for profiles that suit your interest.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Ola..we provide horoscope matching on your shortlisted profiles.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get to know who has reviewed your profile and has shown interest in you!</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Constantly get updates about profiles that sync with your fascination.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Payment with Pay tm or through COD.</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="08" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/shopping.gif" alt="AceShopping" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>AceShopping</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>AceShop is a versatile shopping app for purchasing all your home-needs.Now you can experience an easier way to shop with user-friendly features that guide you from selection to delivery. Aceshop has in store a wide array of products to choose from.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Easily browse through categories , select, order and get quick delivery.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get notifications on Deals and Offers.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;We make sure our products are quality assured.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Items are guaranteed to be delivered at scheduled time.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Payment with Pay tm or through COD.</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="09" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/acestarealty.gif" alt="AcetraRealty" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>acestraRealty</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>AcestraRealty is a powerful tool to communicate information about properties and to promote real estate business across the Country. Through Acestra Realty web portal, landlords, builders can post their property to buy and sell to enable builders to build/construct to develop layout projects according to the customers and owners satisfaction.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Social Media Engagement</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Landlord Database</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Call for Joint Ventures</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Contact management</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Sponsored Listings</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Map Enabled Search</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Owner Portal</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Portfolio Management</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Billing & Invoicing</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Residential & Commercial</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="10" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/kurumpadamtamil.gif" alt="kurumpadam" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>KurumpadamTamil</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>KurumpadamTamil is an 24 x 7 cine bites entertainment web portal which gives the attention and interest of an audience or gives pleasure and delight. It can be an idea or a task, but is more likely to be one of the Short films for Tamil.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Entertainment 24x7</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Digital Engagement</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Advertisement & Media Analytics</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Hundreds of Reports</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="11" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/ace_pro11.jpg" alt="Surabi spices" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>Surabi spices</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>Surabi spices is a versatile shopping website for purchasing all your spices-needs.Now you can experience an easier way to shop with user-friendly features that guide you from selection to delivery. Surabi spices has in store a wide array of spices products to choose from.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Easily browse through categories , select, order and get quick delivery.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get notifications on Deals and Offers.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;We make sure our products are quality assured.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Items are guaranteed to be delivered at scheduled time.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Payment with Pay tm or through COD.</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="12" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/mylapore-taday-app.gif" alt="MylaporeTodayApp" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>MylaporeToday</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>Mylapore Today is an exclusive website which provides news ,happenings,civic issues,tourism details and much more information about mylapore.It is regularly updated and thus it becomes very convenient for new visitors to avail and to be aware about Mylapore and its neighborhood.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Latest news on Mylapore.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Detailed directory information.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get your business Classified.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Civic issues regularly updated.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get to know in and around Mylapore.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Top tourist destinations in mylapore.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Whats special and happening at the moment in mylapore.</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="14" class="mfp-hide">
		<div class="img-lightbox">
			<div class="container-fluid">
				<div class="row no-gutter">
					<div class="col-md-9">
						<div class="img-wrapper"><img src="/websiteV2/img/products/lanka-matrimony.gif" alt="lankamatrimony" class="img-responsive" /></div>
					</div>
					<div class="col-md-3">
						<div class="img-sidebar">
							<div class="sidebar-header">
								<div class="img-title">
									<h4>Lanka Matrimony</h4> </div>
								<!-- <div class="author-meta"><span>By</span><span class="author-name"> aceClassRoom</span></div> --></div>
							<div class="sidebar-content">
								<p>Lanka Matrimony is one of the most sought after and trustworthy matrimony website in Srilanka.We are always on the brink to help you find the most suitable life partner,by providing a very safe and user-friendly match making venture.</p>
								<p> We at Kalyanseva understand and respect the roots of Indian astrology to the core ,hence we make sure that the couple have horoscope compatiblity for thier well-being and happy long married life. </p>
								<p> In addition to helping you find the best matrimonial profile ,we also strive hard to cater to all the indispensible services which forms a vital part of a Wedding, such as suggestions on Wedding photography,Catering and Hall arrangements.</p>
							</div>
							<div class="img-meta">
								<ul>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Creating your credible profile is absolutely free!</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Once you register,feel free to Login and search for profiles that suit your interest.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Ola..we provide horoscope matching on your shortlisted profiles.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Get to know who has reviewed your profile and has shown interest in you!</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Constantly get updates about profiles that sync with your fascination.</span></li>
									<li><span><i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Payment with Pay tm or through COD.</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
<script src="/websiteV2/assets/js/app2.js"></script> @stop
