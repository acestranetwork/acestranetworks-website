@extends('layouts/appV2')
@section('content')

<section class="padding-top-80" >
<div id="particles-js4" >

  <div class="container" id="innerheader2" class="innerheader innerheader2">
    <div class="row pad-small">
      <div class="col-sm-12 text-center">
        <div class="inner-title">
          <h1 class="header-title_new_part_pro header-title_new_part"><span
class="txt-rotate"
data-period="2000"
data-rotate='[ "معلومات عنا"]'></span></h1>
        </div>
        <!-- <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
          <ol class="breadcrumb">
            <li><a href="/" >Home</a></li>
            <li>معلومات عنا</li>
          </ol>
        </div> -->
      </div>
    </div>
  </div>
  </div>
</section>

            <!-- /#innerheader2-->
      <section id="video" class="video video1 section">
        <div class="container">
          <div class="">
          <div class="col-md-12 sec-head one">
            <h3 data-wow-delay="0.1s" class="section-title wow fadeInUp">يدا بيد مع Acestra ل  تقدم</h3>
            <p data-wow-delay="0.2s" class="wow fadeInUp"> منذ تأسيسها، وقد تطورت ACESTRA في شركة لتطوير البرمجيات المختصة التي تقدم تنوعا ، قابلة لل حلول وحدات. لقد نمت من مجموعة صغيرة من المبرمجين برامج مخصصة ل منزل تطوير البرمجيات واسعة النطاق في كل من الإنترنت، وتطوير التطبيقات النقالة . .</p>
          </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-sm-7 padding-top-20">
                  <p data-wow-delay="0.3s" class="wow fadeInUp"> نحن مجموعة من المهنيين عاطفي والتفاني و موثوق به في برنامج و الأجهزة الهندسية و المحللين والمبرمجين و المتخصصين في التجارة الإلكترونية ، والمهنيين تصميم مواقع الإنترنت و مطوري تطبيقات الهاتف المتحرك .</p> <p data-wow-delay="0.4s" class="wow fadeInUp">قمنا بدعم الشركات عبر مجموعة واسعة من الصناعات التي تشمل التصنيع والرعاية الصحية ، والخدمات المالية والقانونية و الحكومة ، والعقارات ، وتجارة التجزئة والمنظمات غير الهادفة للربح . مع القوة و الخبرة في إدارة المشاريع ، والتصميم الإبداعي ، والتكنولوجيا، وتطوير البرمجيات و التجارة الإلكترونية لدينا، ACESTRA شبكات بمساعدة عملائها على تحسين الأداء و اكتساب تركيز الأعمال المتقدمة، و أعلى معايير الجودة والأهم من تحقيق زيادة الربحية ..</p>
                </div>
              <div class="col-sm-3 col-sm-push-1">
                  <div class="post-img-wrap">
                     <img src="/websiteV2/img/join.jpg" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
        <!-- /#innerheader1-->
      <section id="cta2" class="cta2">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="cta2-text">
                <h4 class="text-center"><sup><i class="ion-quote"></i></sup>&nbsp;مهمتنا هي أن تفعل الأصلي ، عمل مبتكر مستوحى من قبل عملائنا "العلامة التجارية ، وأهداف العمل و الميزانية و تقديم قيمة مقابل المال .&nbsp;<sup><i class="ion-quote rotate"></i></sup></h4>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="cta4" class="cta4 bgsec">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="title-subtitle text-center">
               <!-- <img src="/websiteV2/img/team.jpg" class="img-responsive">     -->
                <p>هدفنا هو تنفيذ المشاريع الخاصة بك باستخدام تعزيز دولة من البرامج الفنية . لدينا فريق عمل متخصص و مجهز تجهيزا جيدا ويسعى لتحقيق نتائج سريعة ودقيقة .</p>
                 <div style="margin-bottom:25px;" class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
     <script src="/websiteV2/assets/js/app3.js"></script>

@stop
