@extends('layouts/appV2')
@section('content')

 <section class="padding-top-80">
   <div  id="particles-js6" >
        <div class="container" id="innerheader2" >
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="">
                <h1 class="header-title_new_part_pro header-title_new_part"><span
     class="txt-rotate"
     data-period="2000"
     data-rotate='["Privacy policy"]'></span></h1>
   </div>
              <!-- <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Careers</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>Careers</li>
                </ol>
              </div> -->
            </div>
          </div>
        </div></div>
      </section>
<!--
 <section id="innerheader2" style="" class="innerheader innerheader2">
        <div class="container">
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Privacy policy</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/" target="_blank">Home</a></li>
                  <li>Privacy policy</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </section> -->

            <!-- /#innerheader2-->
  <section class="section privacy">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
              <h2 class="text-center marginBottom20">Privacy policy</h2>
              <p>Your privacy is important to us. In order to protect your privacy we provide this notice in explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice searchable we have made it available on all pages in the footer of the web pages.</p>
              <p>We may collect information such as your name and contact details for the purpose of our business. Information collected on the website helps us respond to questions and will serve as an asset to improve our web site, ban misbehaving IP's and hacking attempts. Data thus collected is not shared with third parties except as required by law or to investigate abuse of this Web site.</p>

              <h4>Our Services</h4>
              <ol>
              <li><p>Acestra Networks Pvt ltd is a professional, family friendly business entity. We reserve the right to limit our service to only those individuals, businesses and non-profit organizations whose needs we can realistically meet.</p></li>
              <li><p>Software or web applications developed by Acestra Networks Pvt ltd cannot be modified without permission and should adhere to Acestra Networks Pvt ltd's philosophy.</p></li>
              </ol>

              <h4>Our Website</h4>
              <ol>
              <li><p>Web Site is designed for the purpose of marketing Acestra Networks Pvt ltd's services and is targeted at commercial and non-commercial organizations.</p></li>
              <li><p>Visitors of the Web Site shall not use the search engines, links, or free graphics for: Illegal purposes, Adult-material purposes, Or any other purpose that does not follow our philosophy</p></li>
              <li><p>Constructive comments about the site and the site content are appreciated and should be e-mailed to info[@]acestranetwork.com.</p></li>
              <li><p>External sites may link to our site without permission as long as your site is clean, family-friendly and follows our ethical philosophy.</p></li>
            </ol>

              <h4>Alert Us</h4>
               <p>If you suspect that our Web Site is being used inappropriately by anyone, or is conveying inappropriate information, or that it contains links, graphics, or text that were reproduced inappropriately, please e-mail contact[@]acestranetwork.com immediately.</p>
               <p>Thank you.</p>
            </div>
          </div>
        </div>
      </section>
      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
       <script src="/websiteV2/assets/lib/jquery/dist/app12.js"></script>
@stop
