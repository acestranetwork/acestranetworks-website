@extends('layouts/appV2')
@section('content')

 <section class="padding-top-80">
   <div  id="particles-js6" >
        <div class="container" id="innerheader2" >
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="">
                <h1 class="header-title_new_part_pro header-title_new_part"><span
     class="txt-rotate"
     data-period="2000"
     data-rotate='["Careers"]'></span></h1>
   </div>
              <!-- <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Careers</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>Careers</li>
                </ol>
              </div> -->
            </div>
          </div>
        </div></div>
      </section>
       <!-- /#innerheader2-->
      <section id="blog-left-sidebar1" class="blog-left-sidebar1 blog section">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="header-classic text-center">
                <h2 class="section-title">Techstars wanted! </h2>
                <p class="career_p">Freshers welcome <a href="/career/fresher/job_apply" class="btn btn-mountain-o btn-round btn-sm job_aply">APPLY</a></p>
                <p class="section-subtitle">Our employees are important to us and while we offer market rate salaries, we also offer a supportive and fun work environment. Candidates can expect us to present them with opportunities that best match their talents and desires. We look forward to meeting you and thank you for your interest!</p>
              </div>
            </div>
            <div class="col-md-8 col-sm-12">
                   <!-- Accordions start-->
              <div id="accordion" class="panel-group">
                <!-- Accordion item start-->
                <?php
                 if(isset($jobs) && count($jobs) > 0){
                  $i = 1;
                  foreach ($jobs as $key => $value) {
                    ?>



                <div class="panel panel-default">

                  <div class="panel-heading">
                    <h4 class="panel-title font-alt">
                        <a data-toggle="collapse" data-parent="#accordion" href="#support<?php echo $value->id;?>"><?php echo $value->title;?>
                        <span class="live">
                        <?php
                          if($value->status == 1){
                            ?>
                               Posted on <?php echo $value->opening;?> &nbsp;
                              <img src="/websiteV2/img/lives.png" width="50"> &nbsp;
                            <?php
                          }else{
                            ?>
                             closed on <?php echo $value->closing;?> &nbsp;
                             <img src="/websiteV2/img/closed.png" width="50"> &nbsp;
                            <?php
                          }
                        ?>
                        </span>
                        </a>
                     </h4>
                  </div>
                  <div id="support<?php echo $value->id;?>" class="panel-collapse collapse <?php if($i == 1 ){ echo "in";}?>">
                    <div class="panel-body">
                      <p><b>Job ID: <?php echo $value->job_id;?> - Location: <?php echo $value->location;?></b></p>
                      <p> <?php echo $value->description;?></p>
                       <p><b>Training Period</b></p>
                       <p><?php echo $value->traning;?></p>
                      <p><b>Job Requirements</b></p>
                      <ul class="mylist">
                      <?php
                      $word = $value->requirements;
                       $ex = explode('*', $word);
                      if (isset($ex) && count($ex) >1) {
                        $j = 1;
                        foreach ($ex as $keys => $values) {
                          if($j !=1){
                          ?>
                            <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;<?php echo $values; ?> </li>
                          <?php
                          }
                          $j++;
                        }
                      }

                      ?>


                      </ul>
                      <p> <a href="/career/<?php echo $value->job_id;?>/job_apply" class="btn btn-primary" alt="Wanted web software fresher">Apply Now</a></p>
                    </div>
                  </div>
                </div>

               <?php
                   $i++;
                  }
                 }
                ?>

              <!--   <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support1">Web Software Developer <span class="live">Posted on 27 Jan 2017 &nbsp;Closed now &nbsp;</span></a> </h4>
                  </div>
                  <div id="support1" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p><b>Job ID: P1405 - Location: Chennai</b></p>
                      <p>We are seeking an Web Software Developer who will be responsible for designing and developing new systems, supporting and enhancing existing systems, and documenting technical specifications.</p>
                       <p><b>Training Period</b></p>
                      <p>The selected candidate shall undergo a mandatory training period of 3 months.The candidate who is willing to learn and proves his/her potential shall be absorbed as a full time employee after the successful completion of the training period. Kindly note that remuneration will be in the form of a stipend during the training period.</p>
                      <p><b>Job Requirements</b></p>
                      <ul class="mylist">
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Education: Graduate degree/diploma in computer information technology or equivalent experience </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Basic knlowedge with: C#, ASP.NET, SQL Server, UML, Java, PHP. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Technical knowledge in designing, building and implementing large scale, complex databases using SQL Server 2008/2012. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Be proficient in HTML,CSS,JS,AngularJS . </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Functions at a high level of autonomy in setting objectives, expectations and tracking performance. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Excellent verbal and written communication skills. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Any Batch fresher can apply. And total vacancies 4. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Provides work estimates for their own development tasks and verifies works estimates of development tasks on project plans. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Have basic skills: Git/SVN, Objective C++, Ionic, PhoneGAP. <br><br>  <a href="/career/P1405/job_apply" class="btn btn-primary" alt="Wanted web software fresher">Apply Now</a> </li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support2" class="collapsed"> Analyst Programmer</a></h4>
                  </div>
                  <div id="support2" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p><b>Job ID: P1401 - Location: Chennai - Department: Technology</b></p>
                      <p>We are seeking an Web software Programmer who will be responsible for designing and developing new systems, supporting and enhancing existing systems, and documenting technical specifications.</p>
                      <p><b>Job Requirements</b></p>
                      <ul class="mylist">
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Education: Graduate degree/diploma in computer information technology or equivalent experience </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Basic knlowedge with: C#, ASP.NET, SQL Server, UML, Java, PHP. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Technical knowledge in designing, building and implementing large scale, complex databases using SQL Server 2008/2012. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Be proficient in HTML,CSS,JS,AngularJS . </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Functions at a high level of autonomy in setting objectives, expectations and tracking performance. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Excellent verbal and written communication skills. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Any Batch fresher can apply. And total vacancies 4. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Provides work estimates for their own development tasks and verifies works estimates of development tasks on project plans. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Have basic skills: Git/SVN, Objective C++, Ionic, PhoneGAP. <br><br>  <a href="/career/P1405/job_apply" class="btn btn-primary" alt="Wanted web software fresher">Apply Now</a> </li>
                      </ul>
                    </div>
                  </div>
                </div>

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support3" class="collapsed"> Web Application Developer</a></h4>
                  </div>
                  <div id="support3" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p><b>Job ID: P1402 - Location: Chennai - Department: Technology</b></p>
                      <p>We are looking for a motivated Web Developer who enjoys bringing out client's ideas to life by developing websites that stand out from the rest!</p>
                      <ul class="mylist">
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Education: Graduate degree/diploma in computer information technology or equivalent experience </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Competent with HTML5, CSS3, JavaScript, Jquery, PHP, MySQL, WordPress.</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Have an eye for design and must be detail-oriented.</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Thorough knowledge of web development techniques and procedures. </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Design for mobile devices, responsive design and cross-platform development.</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Able to prioritize multiple project deadlines.</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Ability to work independently with minimal supervision in a fast-paced team environment.</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Great communication and customer service skills.</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Nice to have skills: Git/SVN, Angular, Node, MongoDB <br><br>  <a href="/career/P1402/job_apply" class="btn btn-primary">Apply Now</a> </li>
                      </ul>
                    </div>
                </div>
                 </div>

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support4" class="collapsed"> Project Co-ordinator</a></h4>
                  </div>
                  <div id="support4" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p><b>Job ID: P1403 - Location: Chennai - Department: Management</b></p>
                      <p>The Project Co-ordinator role has direct responsibility for complete life cycle management and accountability of project initiatives within Acestra Network, ensuring compliance to project management policy, standards and procedures. The Project Co-ordinator must also have a genuine interest to make a valuable contribution to increase capability and maturity of a rapidly growing company.</p>
                      <ul class="mylist">
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Education: Qualified MA/BA with computing skills </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Project management experience a plus</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Fluent in English, oral and written</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Strong conceptual thinking and presentation skills </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp; Ability to take direction and critique</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Ability to collaborate with a team and also work autonomously</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Proven problem-solving skills and adaptability to change <br><br>  <a href="/career/P1403/job_apply" class="btn btn-primary">Apply Now</a> </li>
                      </ul>
                    </div>
                </div>
                 </div>

                <div class="panel panel-default">
                  <div class="panel-heading">
                    <h4 class="panel-title font-alt"><a data-toggle="collapse" data-parent="#accordion" href="#support5" class="collapsed">  Copy Writer</a></h4>
                  </div>
                  <div id="support5" class="panel-collapse collapse">
                    <div class="panel-body">
                      <p><b>Job ID: P1404 - Location: Chennai - Department: Marketing</b></p>
                      <p>Do you have the ability to write compelling creative copy? Do you know how to fashion ideas to achieve a communications strategy, incorporating marketing techniques into finely crafted prose that drives people to action?</p>
                      <ul class="mylist">
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Education: Bachelor's Degree (English or Journalism preferred) </li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Strong writing skills with 3+ years' web journalism or copywriting experience</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Proficiency in Microsoft Word, Excel</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Proven ability to meet deadlines, handle multiple jobs simultaneously, and re-prioritize at a moment's notice</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Ability to work both independently and as part of a team</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Fast, flexible, detail-oriented, and cooperative work style</li>
                        <li>&nbsp;<i class="ion-ios-checkmark-empty"></i>&nbsp;&nbsp;&nbsp;Project management experience a plus, but not required <br><br>  <a href="/career/P1404/job_apply" class="btn btn-primary">Apply Now</a> </li>
                      </ul>
                    </div>
                  </div>
                </div> -->

              </div>


            </div>
            <div class="col-md-4 col-sm-12">
              <div id="sidebar-1" class="sidebar-1 blog-sidebar">
                 <div id="textWidget2" class="textWidget2 mountainWidget price-features">
                  <h4>How to Apply?</h4>
                  <ul>
                    <li>You must Apply Online</li>
                    <li>Cover letter, resume within five (5) pages.</li>
                    <li>Add Job ID in the subject line (if indicated).</li>
                    <li>Compensation based on experience.</li>
                    <li> Indicate your availability for the interview.</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

        </div>
      </section>
      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
       <script src="/websiteV2/assets/lib/jquery/dist/app12.js"></script>
     @stop
