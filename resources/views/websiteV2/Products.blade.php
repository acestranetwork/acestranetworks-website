@extends('layouts/appV2')
@section('content')

<section class="padding-top-80">
  <!-- <div id="particles-js3"> -->
  <div id="particles-js3">

<div class="container" id="innerheader2"  class="innerheader innerheader2">
<div class=" pad-small">
<div class=" text-center">
<div class="col-md-12">
<!-- <h3  class="header-title_new_part_add header-title_new_part ">Products</h3>
 -->
    <h1 class="header-title_new_part_pro header-title_new_part"><span
     class="txt-rotate"
     data-period="2000"
     data-rotate='[ "Products", "Made by Acestra"]'></span></h1>
              <!--   <p class=" inner-title hidden-xs">Acestra Network provides innovative solutions and top quality services for a great variety of business organizations. We can help you by developing and maintaining software solutions, upgrade technology and migrate services to cloud.</p> --> </h1>
<!--              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
<ol class="pro_bread breadcrumb ">
<li><a href="/">Home</a></li>
<li>Products</li>
</ol>
</div> -->
             </div>
 </div>

</div>
</div>
</div>
</div>
</section>
<section class="section-padding portfolio-grid-1 " id="portfolio-grid-1" >

<div class="container "><div class="portfolio-grid">
 <div class="grid-sizer"></div>
      <div class="row text-center">

        <div class="portfolio-grid-item">
          <div class="thumbnail wow fadeInUp " data-wow-delay="0.2s">
            <img src="/websiteV2/img/products/ace_pro1.jpg" alt="">
              <div class="caption">
                <!-- <h4>MYLAPORE TODAY</h4> -->
                <!-- <p class="text-justify padding-top-20">Mylapore Today is an exclusive website which provides news ,happenings,civic issues,tourism details and much more information about mylapore.It is regularly updated and thus it becomes very convenient for new visitors to avail and to be aware about Mylapore and its neighborhood.</p> -->

                                        <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <h4 class="panel-title">
                                                        <a role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
                                                            MYLAPORE TODAY
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
                                                    <div class="panel-body">
                                                      <p class="text-justify padding-top-20">Mylapore Today is an exclusive website which provides news ,happenings,civic issues,tourism details and much more information about mylapore.It is regularly updated and thus it becomes very convenient for new visitors to avail and to be aware about Mylapore and its neighborhood.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                    <h4 class="panel-title">
                                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion1" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                              FEATURES
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                                                    <div class="panel-body">
                                                      <ul>
                                            <li>  Latest news on Mylapore.</li>
                                            <li> Detailed directory information.</li>
                                            <li>   Get your business Classified.</li>
                                            <li>   Civic issues regularly updated.</li>
                                            <li>     Get to know in and around Mylapore.</li>
                                            <li>   Top tourist destinations in mylapore.</li>
                                            <li>
                                               Whats special and happening at the moment in mylapore.</li>
                                        </ul>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="padding-top-20">
                                        <a href="http://mylaporetoday.com/"  target="_blank" class="btn btn-info btn-lg btn-block " role="button">Visit Now</a>
                                        </div>

            </div>
          </div>
        </div>

        <div class="portfolio-grid-item ">
          <div class="thumbnail wow fadeInUp " data-wow-delay="0.4s">
            <img src="/websiteV2/img/products/ace_pro3.jpg" alt="">
              <div class="caption">
                <!-- <h4>KOLLYWOOD TALKIES</h4> -->
                <!-- <p class="text-justify padding-top-20">Kollywood Talkies is an 24 x 7 cine bites entertainment web portal which gives the attention and interest of an audience or gives pleasure and delight. It can be an idea or a task, but is more likely to be one of the activities or events that can be viewed on day to day basis over thousands and millions of people across the country and for the purpose of keeping an audience's attention.</p> -->
                <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree" class="">
                                    KOLLYWOOD TALKIES
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree" aria-expanded="true" style="">
                            <div class="panel-body">
                              <p class="text-justify padding-top-20">Kollywood Talkies is an 24 x 7 cine bites entertainment web portal which gives the attention and interest of an audience or gives pleasure and delight. It can be an idea or a task, but is more likely to be one of the activities or events that can be viewed on day to day basis over thousands and millions of people across the country and for the purpose of keeping an audience's attention.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                      FEATURES
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                              <ul>
                    <li>  Entertainment 24x7</li>
                    <li>    Cine Media Marketing & Promotions</li>
                    <li>    Digital Engagement</li>
                    <li>      Advertisement & Media Analytics</li>
                    <li>        Cine PR</li>
                    <li>     Hundreds of Reports</li>
                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding-top-20">
                <a href="http://kollywoodtalkies.com/"  target="_blank" class="btn btn-info btn-lg btn-block " role="button">Visit Now</a>
                </div>
            </div>
          </div>
        </div>

        <div class="portfolio-grid-item">
          <div class="thumbnail wow fadeInUp " data-wow-delay="0.6s">
            <img src="/websiteV2/img/products/ace_pro4.jpg" alt="">
              <div class="caption">
                <!-- <h4>ACE CLASSROOM</h4> -->
                <!-- <p class="text-justify padding-top-20">aceClassRoom is a highly interactive web based education management system that provides a complete market leading solution for modern day school administration and management. We recognize the need for a solution that bridges the gap between the School, Parents and Students. It combines the evolution of software and the internet for the purpose of improving education by fostering an environment for any time access, monitoring, communication and secure data exchange</p> -->
                <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingFive">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive" class="">
                                    ACE CLASSROOM
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFive" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingFive" aria-expanded="true" style="">
                            <div class="panel-body">
                              <p class="text-justify padding-top-20">aceClassRoom is a highly interactive web based education management system that provides a complete market leading solution for modern day school administration and management. We recognize the need for a solution that bridges the gap between the School, Parents and Students. It combines the evolution of software and the internet for the purpose of improving education by fostering an environment for any time access, monitoring, communication and secure data exchange</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                      FEATURES
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                              <ul>
                    <li>    Easy to use, simple to navigate</li>
                    <li> Intuitive dashboard</li>
                    <li>    Remote monitoring, assistance</li>
                    <li>    Secure and reliable</li>
                    <li>       Multi-user system</li>
                    <li>      Regular backup</li>
                    <li>24 * 7 availability</li>
                      <li>   Hundreds of Reports</li>
                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding-top-20">
                <a href="http://aceclassroom.com/"  target="_blank" class="btn btn-info btn-lg btn-block " role="button">Visit Now</a>
                </div>
            </div>
          </div>
        </div>

        <div class="portfolio-grid-item">
          <div class="thumbnail wow fadeInUp " data-wow-delay="0.8s">
            <img src="/websiteV2/img/products/ace_pro7.jpg" alt="">
              <div class="caption">
                <!-- <h4>KALYAN SEVA</h4>
                <p class="text-justify padding-top-20">

Kalyanseva is one of the most sought after and trustworthy matrimony website in Srilanka.We are always on the brink to help you find the most suitable life partner,by providing a very safe and user-friendly match making venture.

We at Kalyanseva understand and respect the roots of Indian astrology to the core ,hence we make sure that the couple have horoscope compatiblity for thier well-being and happy long married life.

In addition to helping you find the best matrimonial profile ,we also strive hard to cater to all the indispensible services which forms a vital part of a Wedding, such as suggestions on Wedding photography,Catering and Hall arrangements.
</p> -->
<div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingSeven">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven" class="">
                    KALYAN SEVA
                </a>
            </h4>
        </div>
        <div id="collapseSeven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingSeven" aria-expanded="true" style="">
            <div class="panel-body">
              <p class="text-justify padding-top-20">Kalyanseva is one of the most sought after and trustworthy matrimony website in Srilanka.We are always on the brink to help you find the most suitable life partner,by providing a very safe and user-friendly match making venture.</p>
              <p class="text-justify padding-top-20">We at Kalyanseva understand and respect the roots of Indian astrology to the core ,hence we make sure that the couple have horoscope compatiblity for thier well-being and happy long married life. </p>
              <p class="text-justify padding-top-20">In addition to helping you find the best matrimonial profile ,we also strive hard to cater to all the indispensible services which forms a vital part of a Wedding, such as suggestions on Wedding photography,Catering and Hall arrangements.</p>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingEight">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                      FEATURES
                </a>
            </h4>
        </div>
        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
              <ul>
    <li>           Creating your credible profile is absolutely free!</li>
    <li>  Once you register,feel free to Login and search for profiles that suit your interest.</li>
    <li>     Ola..we provide horoscope matching on your shortlisted profiles.</li>
    <li>      Get to know who has reviewed your profile and has shown interest in you!</li>
    <li>        Constantly get updates about profiles that sync with your fascination.</li>
    <li>        Payment with Pay tm or through COD.</li>
</ul>

            </div>
        </div>
    </div>
</div>
<div class="padding-top-20">
<a href="http://www.kalyanseva.com/"  target="_blank" class="btn btn-info btn-lg btn-block" role="button">Visit Now</a>
</div>
            </div>
          </div>
        </div>
        <div class="portfolio-grid-item">
          <div class="thumbnail wow fadeInUp " data-wow-delay="0.4s">
            <img src="/websiteV2/img/products/acestarealty.jpg" alt="">
              <div class="caption">
                <!-- <h4>ACE REALTITY</h4> -->
                <!-- <p class="text-justify padding-top-20">AcestraRealty is a powerful tool to communicate information about properties and to promote real estate business across the Country. Through Acestra Realty web portal, landlords, builders can post their property to buy and sell to enable builders to build/construct to develop layout projects according to the customers and owners satisfaction.</p> -->
                <div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingNine">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseNine" aria-expanded="true" aria-controls="collapseNine" class="">
                                    ACESTRA REALTITY
                                </a>
                            </h4>
                        </div>
                        <div id="collapseNine" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingNine" aria-expanded="true" style="">
                            <div class="panel-body">
                              <p class="text-justify padding-top-20">AcestraRealty is a powerful tool to communicate information about properties and to promote real estate business across the Country. Through Acestra Realty web portal, landlords, builders can post their property to buy and sell to enable builders to build/construct to develop layout projects according to the customers and owners satisfaction.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
                                      FEATURES
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                              <ul>

                    <li>   Social Media Engagement</li>
                    <li> Landlord Database</li>
                    <li>     Call for Joint Ventures</li>
                    <li>      Contact management</li>
                    <li>         Sponsored Listings</li>
                    <li>        Map Enabled Search</li>
                    <li>  Owner Portal</li>
                    <li>     Portfolio Management</li>
                    <li>       Billing & Invoicing</li>
                      <li>    Residential & Commercial</li>
                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding-top-20">
                <a href="http://www.acestrarealty.com/  "   target="_blank" class="btn btn-info btn-lg btn-block" role="button">Visit Now</a>
                </div>
            </div>
          </div>
        </div>
        <div class="portfolio-grid-item ">
          <div class="thumbnail wow fadeInUp " data-wow-delay="0.6s">
            <img src="/websiteV2/img/products/lanka-matrimony.png" alt="">
              <div class="caption">
                <!-- <h4>LANKA MATRIMONY</h4>
                <p class="text-justify padding-top-20">

Lanka Matrimony is one of the most sought after and trustworthy matrimony website in Srilanka.We are always on the brink to help you find the most suitable life partner,by providing a very safe and user-friendly match making venture.

We at Kalyanseva understand and respect the roots of Indian astrology to the core ,hence we make sure that the couple have horoscope compatiblity for thier well-being and happy long married life.

In addition to helping you find the best matrimonial profile ,we also strive hard to cater to all the indispensible services which forms a vital part of a Wedding, such as suggestions on Wedding photography,Catering and Hall arrangements.
</p> -->
<div class="panel-group" id="accordion6" role="tablist" aria-multiselectable="true">
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingEleven">
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion6" href="#collapseEleven" aria-expanded="true" aria-controls="collapseEleven" class="">
                    LANKA MATRIMONY
                </a>
            </h4>
        </div>
        <div id="collapseEleven" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingEleven" aria-expanded="true" style="">
            <div class="panel-body">
              <p class="text-justify padding-top-20">Lanka Matrimony is one of the most sought after and trustworthy matrimony website in Srilanka.We are always on the brink to help you find the most suitable life partner,by providing a very safe and user-friendly match making venture.</p>

              <p class="text-justify padding-top-20">We at Lanka Matrimony understand and respect the roots of Indian astrology to the core ,hence we make sure that the couple have horoscope compatiblity for thier well-being and happy long married life. </p>

              <p class="text-justify padding-top-20">In addition to helping you find the best matrimonial profile ,we also strive hard to cater to all the indispensible services which forms a vital part of a Wedding, such as suggestions on Wedding photography,Catering and Hall arrangements.</p>

            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwelve">
            <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion6" href="#collapseTwelve" aria-expanded="false" aria-controls="collapseTwelve">
                      FEATURES
                </a>
            </h4>
        </div>
        <div id="collapseTwelve" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwelve" aria-expanded="false" style="height: 0px;">
            <div class="panel-body">
              <ul>
                <li>           Creating your credible profile is absolutely free!</li>
                <li>  Once you register,feel free to Login and search for profiles that suit your interest.</li>
                <li>     Ola..we provide horoscope matching on your shortlisted profiles.</li>
                <li>      Get to know who has reviewed your profile and has shown interest in you!</li>
                <li>        Constantly get updates about profiles that sync with your fascination.</li>
                <li>        Payment with Pay tm or through COD.</li>
</ul>

            </div>
        </div>
    </div>
</div>
<div class="padding-top-20">
<a href="http://lankamatrimony.lk/"  target="_blank" class="btn btn-info btn-lg btn-block " role="button">Visit Now</a>
</div>
            </div>
          </div>
        </div>
        <div class="portfolio-grid-item">
          <div class="thumbnail wow fadeInUp " data-wow-delay="0.8s">
            <img src="/websiteV2/img/products/ace_pro10.jpg" alt="">
              <div class="caption">
                <!-- <h4>KURUM PADAM</h4> -->
                <!-- <p class="text-justify padding-top-20">KurumpadamTamil is an 24 x 7 cine bites entertainment web portal which gives the attention and interest of an audience or gives pleasure and delight. It can be an idea or a task, but is more likely to be one of the Short films for Tamil.</p> -->
                <div class="panel-group" id="accordion7" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThirteen">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion7" href="#collapseThirteen" aria-expanded="true" aria-controls="collapseThirteen" class="">
                                    KURUM PADAM
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThirteen" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThirteen" aria-expanded="true" style="">
                            <div class="panel-body">
                              <p class="text-justify padding-top-20">KurumpadamTamil is an 24 x 7 cine bites entertainment web portal which gives the attention and interest of an audience or gives pleasure and delight. It can be an idea or a task, but is more likely to be one of the Short films for Tamil.</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingForteen">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion7" href="#collapseForteen" aria-expanded="false" aria-controls="collapseForteen">
                                      FEATURES
                                </a>
                            </h4>
                        </div>
                        <div id="collapseForteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingForteen" aria-expanded="false" style="height: 0px;">
                            <div class="panel-body">
                              <ul>

                    <li>      Entertainment 24x7</li>
                    <li> Digital Engagement</li>
                    <li>      Advertisement & Media Analytics</li>
                    <li>      Hundreds of Reports</li>
                </ul>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding-top-20">
                <a href="http://kurumpadamtamil.com/"  target="_blank" class="btn btn-info btn-lg btn-block " role="button">Visit Now</a>
                </div>
            </div>
          </div>
        </div>

      </div><!-- End row -->
</div>
    </div><!-- End container -->
  </section>


  <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
  <script src="/websiteV2/assets/js/app2.js"></script>

@stop
