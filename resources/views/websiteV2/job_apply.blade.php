@extends('layouts/appV2')
@section('content')
<!-- <section id="innerheader2" style="background-image: url(/websiteV2/img/header/contact.jpg)" class="innerheader innerheader2">
        <div class="container">
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="inner-title">
              	  <?php
                          if($position_name == 'fresher'){
                            ?>
                            <h3 data-wow-delay="0.1s" class="wow fadeInUp">Job Apply</h3>
                             <?php
                          }else{
                            ?>
                               <h3 data-wow-delay="0.1s" class="wow fadeInUp">Job Apply for {!!$position_name!!}</h3>
                            <?php
                          }
                        ?>

              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>Contact</li>
                  <li>Career</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </section> -->


        <section class="padding-top-80" >
          <div  id="particles-js7">
              <div class="container" id="innerheader2" >
                <div class="row pad-small">
                  <div class="col-sm-12 text-center">
                    <div class="">
                      <?php
                              if($position_name == 'fresher'){
                                ?>
                      <h1 class="header-title_new_part_pro header-title_new_part"><span
                  class="txt-rotate"
                  data-period="2000"
                  data-rotate='["Job Apply"]'></span></h1>

                  <?php
               }else{
                 ?>
                 <h1 class="header-title_new_part_pro header-title_new_part"><span
             class="txt-rotate"
             data-period="2000"
             data-rotate='["Job Apply for {!!$position_name!!}"]'></span></h1>

              <?php
            }
          ?>
                  </div>
                    <!-- <div class="inner-title">
                      <h3 data-wow-delay="0.1s" class="wow fadeInUp">Contact</h3>
                    </div>
                    <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                      <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li>Contact</li>
                      </ol>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            </section>
      <!-- /#innerheader2-->
      <section id="contact" class="contact-form section">
        <div class="container">
          <div class="row">
            <div class="col-md-12 contact-header  sec-head four">
              <h2>Techstars wanted!</h2>
              <!-- <p class="section-subtitle">Our employees are important to us and while we offer market rate salaries, we also offer a supportive and fun work environment. Candidates can expect us to present them with opportunities that best match their talents and desires. We look forward to meeting you and thank you for your interest!</p> -->

              <p class="wow fadeInUp">If you face any trouble, you can always let our dedicated support team help you. They are ready for you 24/7.</p>

            </div>
            <div class="col-md-6 contact-desc">
              <div data-wow-delay="0.1s" class="call-us contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-telephone"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Call us</h5><span>+91 44 24629069</span>
                </div>
              </div>
              <div data-wow-delay="0.2s" class="address contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-ios-location"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Address</h5><span>2/2 Venkatesa Agraharam Street,<br/>Mylapore,Chennai-600004,India</span>
                </div>
              </div>
              <div data-wow-delay="0.3s" class="email contact-icon-block clearfix wow fadeInUp"><span class="contact-form-icons text-center"><i class="ion-at"></i></span>
                <div class="contact-small-text">
                  <h5 class="contact-small-header">Email us</h5>@include('include.mailto')
                </div>
              </div>
            </div>
            <div class="col-md-6">
             @if ($errors->has('success_msg'))
                        <div class="alert alert-success alert-block">
                            <strong class="text-success">{{ $errors->first('success_msg') }}</strong>
                        </div>
                    @endif
              <div class="contact-fields">
                <!-- <form id="contactForm" action="php/contact.php" method="post" name="contactform">
                  <div data-wow-delay="0.1s" class="form-group wow fadeInUp">
                    <input id="name" type="text" placeholder="Your Name" name="name" required="required" class="form-control"/>
                  </div>

                  <div data-wow-delay="0.2s" class="form-group wow fadeInUp">
                    <input id="email" type="email" placeholder="Your Email" name="email" required="required" class="form-control"/>
                  </div>

                  <div data-wow-delay="0.3s" class="form-group wow fadeInUp">
                    <textarea rows="6" placeholder="Message" name="message" class="form-control"></textarea>
                  </div>

                  <div id="contactFormResponse"></div>
                  <button id="cfsubmit" type="submit" data-wow-delay="0.2s" class="btn btn-mountain btn-block wow fadeInUp">Submit</button>
                </form> -->
                <form action="/career/P1405/job_apply_insert" method="post" enctype="multipart/form-data"  >
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <!--  @if ($errors->has())

						<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Error! &#128546;</strong>
							@foreach ($errors->all() as $error)
							<span class="err_line">

								<span class="sr-only">Errors</span>
								<p class="error_message">{{ $error }} <span class="glyphicon glyphicon-exclamation-sign" style="padding-left:10px;" aria-hidden="true"></span></p>
							</span>
							@endforeach
						</div>

					@endif   -->


				@if (Session::has('wrong'))
					<div class="col-sm-12 ">
						<div class="alert alert-danger alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
						<p class="alert-danger alert-info">{{ Session::get('wrong') }}</p>
						</div>
					</div>
					@endif
					@if ($message = Session::get('success'))
					<div class="col-sm-12 ">
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>Success  &#128522; </h4>
							{!! $message !!}
						</div>
					</div>
					@endif
					@if ($error = Session::get('alert'))
					<div class="col-sm-12 ">
						<div class="alert alert-danger alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>Error  &#128546; </h4>
							{!! $error !!}
						</div>
					</div>
					@endif

					 <!--  <div data-wow-delay="0.1s" class="form-group wow fadeInUp">
	                    <input id="name" type="text" placeholder="Your Name" name="name" required="required" class="form-control"/>
	                  </div> -->

                   <div class="col-sm-12 highlight" style="display:none">
			            <div class="alert alert-danger alert-block">
			              <button type="button" class="close" data-dismiss="alert">&times;</button>
			              <h4>Error  &#128546; </h4>
			             <span class="highlightmsg"></span>
			            </div>
			          </div>
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6 form-group wow fadeInUp">
							<label>Your Name:</label>
							<input type="text" placeholder="Your name" name="name" class=" form-control validate" id="name" value="{{ old('name') }}">
							 @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<label>Your Email:</label>
							<input type="text" placeholder="Your Email" name="email" class="textbox_align form-control validate" id="email" value="{{ old('email') }}">
							@if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
						</div>
					</div>
					<?php
					    $result = str_replace('_', ' ', $position_name);
					    $post_name=ucwords($result);
					?>
					<input type="hidden" name="post" value="<?php echo $post_name;?>">
					<label>Message Subject:</label>

					<select class="textbox_align form-control validate" name="subject" id="subject">

							<?php

                          if($position_name == 'fresher'){
                 if(isset($jobs) && count($jobs) > 0){

                    ?>

						<option value="">choose job to apply</option>
					  <?php

                  foreach ($jobs as $key => $value) {

                    ?>
						<option value="Application for the post of <?php echo $value->job_id;?>	" >Application for the post of <?php echo $value->job_id;?></option>
					  <?php
                    }
                  }
                 }
                 else
                 {
                   ?>
                   <option value="Application for the post of {!!$post_name!!}" >Application for the post of {!!$post_name!!}</option>

					  <?php

                 }
                   ?>
					</select>

					@if ($errors->has('subject'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('subject') }}</strong>
                        </span>
                    @endif

		            <label>Contact Number:</label>
		            <input type="tel" placeholder="Your number" name="phone" class="textbox_align form-control validate" id="phone" value="{{ old('phone') }}">
		            <label class="file_browse">Upload your resume (.doc, .docx or .pdf and size less than 5MB)</label>
		            <input type="file" size="chars" name="cv_name" class="validate" id="cv_name">
		            @if ($errors->has('cv_name'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('cv_name') }}</strong>
                        </span>
                    @endif

					<label class="file_browse">Cover letter: </label>
					<br>
					<textarea name="cover_letter" class="textbox_align form-control validate" id="letter">{{ old('name') }}</textarea>
					@if ($errors->has('cover_letter'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('cover_letter') }}</strong>
                        </span>
                    @endif

					<div class="row">
						<!-- captacha -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group" >
								<label for="captcha">Security code</label>
								<input name="captcha" type="text" class="form-control validate" id="captcha" placeholder="captcha" autocomplete="off">
								@if ($errors->has('captcha'))
			                        <span class="help-block">
			                            <strong class="text-danger">{{ $errors->first('captcha') }}</strong>
			                        </span>
			                    @endif
							</div>
							<div class="row">
								<div class="col-xs-2">
									<center><div class="form-group" id="captchadiv">
										<img src="{{ url('captcha') }}" id="captchaimg" class="img-rounded" alt="captcha_img" />
									</div></center>
								</div>
								<div class="col-xs-10 refresh_div text-left">
									<a href="#" class="refreshCaptcha text-success bg-success">
										<i class="fa fa-refresh"></i></a>
									</div>
								</div>
								<!-- captacha -->
								<!-- <button id="cfsubmit" type="submit" data-wow-delay="0.2s" class="btn btn-mountain btn-block wow fadeInUp">Submit</button> -->
								<!-- <div class="col-lg-6 col-md-6 col-sm-12 captcha_block"> -->
									<button type="submit" class="btn btn-primary btn-md">Send Message</button>
									<!-- <button id="cfsubmit" type="submit" data-wow-delay="0.2s" class="btn btn-mountain btn-block wow fadeInUp">Submit</button> -->
									<!--  <button type="reset" class="btn btn-warning">Reset</button>-->

								<!-- </div> -->
							</div>
						</div>
					</form>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- <section id="contact-map" class="contact-map">
        <div id="map"></div>
      </section> -->


      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
       <script src="/websiteV2/assets/lib/jquery/dist/app13.js"></script>
	@stop
