@extends('layouts/appV2')
@section('content')

<section ng-controller="home">
      <!-- Header Section-->
     <header>

<!-- scripts -->


        <!-- <div id="owl-hs-carousel" class="owl-carousel carousel-items-wraper"> -->
          <div class="item">
      <div id="particles-js">

            <div  class="header-bg">
              <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="intro-box">
                      <div class="intro text-center">
                        <!-- <h1 >Global Software services</h1> -->
                        <h1 class="header-title_new_part wow slideInUp" data-wow-delay="0.2s">
  <span
     class="txt-rotate"
     data-period="2000"
     data-rotate='[ "Global Software Services", "Digital Transformation"]'></span>
</h1>
                        <div class="row">
                          <div class="col-md-8 col-md-offset-2">
                 <!--            <div class="header-subtitle_new_part element " id="typed2-strings">Evolve your business with the help of Custom software business solutions at ACESTRA</div> -->
                            <span id="typed2-strings"></span>
                          </div>

                        </div>

                      </div>
                    </div>
                  </div>
                  </div>
                </div>

              </div>
              <a href="#ourExpertise1" class="down-arrow">
          <!--span.ion-ios-arrow-down.infinite.animated.fadeInDown--><span data-wow-iteration="infinite" data-wow-duration="1200ms" class="ion-ios-arrow-down wow fadeInDown animated" ></span></a>
            </div>

          </div>
      </header>

          <!-- <div class="item">
            <div style="background-image:url('/websiteV2/img/slider/slider2.jpg')" class="header-bg">
              <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="intro-box">
                      <div class="intro text-center">
                        <h1 class="header-title">QUALITY SOFTWARE SOLUTIONS</h1>
                        <div class="row">
                          <div class="col-md-8 col-md-offset-2">
                            <div class="header-subtitle">Your search for a quality software solutions provider for all your back office needs ends here</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="item">
            <div style="background-image:url('/websiteV2/img/slider/slider3.jpg')" class="header-bg">
              <div class="container">
                <div class="row">
                  <div class="col-sm-12">
                    <div class="intro-box">
                      <div class="intro text-center">
                        <h1 class="header-title">DIGITAL TRANSFORMATION</h1>
                        <div class="row">
                          <div class="col-md-8 col-md-offset-2">
                            <div class="header-subtitle mobileview_bottom">Designing,Developing and Deployment!</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
        <!-- </div> -->
        <section id="ourExpertise1" class="ourExpertise1 back_sec_col section down-arrow-dest">
              <div class="container">
                <div class="row">
                  <div class="col-sm-12 head_focus sec-head one   wow fadeInUp expertise-header" data-wow-delay="0.2s">
                    <div>
                      <h2>Our Proficiency</h2>

                    </div>
                  </div>
                  <div class="col-sm-12 col-md-4 sm-push-bottom-120   wow fadeInUp " data-wow-delay="0.4s">
                    <p>
                      Create a website that you are gonna be proud of. Be it Business Portfolio, Agency,
                      Photography, eCommer developmen, startup and much more.
                    </p><a href="/services" data-wow-delay="0.3s" class="btn2 btn-2 btn-2e wow fadeInUp " data-wow-delay="0.6s">Learn More</a>
                  </div>
                  <div class="col-sm-12 col-md-8">
                    <div id="expertise-circle-wraper" class="circle-wrapper clearfix">
                      <div id="expertise-circle" data-color="rgb(255,87,51)" data-bg-color="rgba(255,87,51,0.4)" data-value="75" class="expertise-circle wow fadeInUp " data-wow-delay="0.4s">
                        <div class="expertise"></div>
                        <h4 class="text-center">html5</h4>
                      </div>
                      <div id="expertise-circle-2" data-color="rgb(0,160,234)" data-bg-color="rgba(0,160,234,0.4)" data-value="90" class="expertise-circle wow fadeInUp " data-wow-delay="0.6s">
                        <div class="expertise"></div>
                        <h4 class="text-center">css3</h4>
                      </div>
                      <div id="expertise-circle-3" data-color="rgb(247,227,79)" data-bg-color="rgba(247,227,79,0.4)" data-value="65" class="expertise-circle wow fadeInUp " data-wow-delay="0.8s">
                        <div class="expertise"></div>
                        <h4 class="text-center">javascript</h4>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>



      <!-- About us Section-->
      <section id="about" class="about-us-features  section about-us-2">
        <div class="container">
          <div class="row">
            <div class="col-md-12 head_focus sec-head two  wow fadeInUp " data-wow-delay="0.2s">
          <h2  class="section-title wow fadeInUp" data-wow-delay="0.4s">Who We Are</h2>
          <p data-wow-delay="0.6s" class="wow fadeInUp">Acestra Network provides
            innovative
            software and mobile solutions.</p>
          </div>
          <div class="col-md-4 col-md-offset-2 col-sm-5 wow fadeInUp " data-wow-delay="0.8s" ><img src="/websiteV2/img/1.jpg" alt="about" data-wow-delay="0.1s" class="img-responsive wow fadeInUp "/></div>

            <div class="col-md-6 col-sm-7">

              <div class="row">
                <div class="col-sm-12 text-justify padding-top-20">
                  <div data-wow-delay="0.3s" class="wow fadeInUp " data-wow-delay="1.0s">
                    <p>We are leading Web Design & Website Application Development company having offices in Chennai, India and Toronto,Canada. We also develop Mobile Applications, e­Commerce Websites, Social Media Websites, CM based Websites, Design Logo's and Design Brochures and provide Domian & Hosting Solutions. Our services and solutions result in improved quality and increased efficiency, coupled with data analytics that provide vital insight for you to enhance your business and support effective decision making. We use our services, products and expertise to find a perfect fit for your business needs and transform our promise into reality. We Deliver solutions with a platform of your choice that is built to your needs for enhanced user experience with custom features and functionality that drives conversions.</p>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </section>
            <!-- /#innerheader2-->
      <section id="cta2" class="cta2">
        <div class="container">
          <div class="row">
            <div class="col-md-7 col-xs-12 col-sm-12 ">
              <div class="cta2-text wow fadeInLeft " data-wow-delay="0.2s">
                <h4 class="text-center">We provide the latest technologies to help you succeed in your business</h4><span class="small-sub text-center">Talk to our experts on the technologies that we use.  </span>
              </div>
            </div>
            <div class="col-md-4 col-md-offset-1 col-xs-12 col-sm-12  text-center">
                <div style="margin-bottom:25px;" class="clearfix"></div>
                 <a href="/quote" class=" btn1 btn-1 btn-1e wow fadeInRight " data-wow-delay="0.2s">Request Quote!</a>
            </div>
          </div>
        </div>
      </section>
      <section id="why-choose-us" class="why-choose-us section">
             <div class="container">
               <div class="row">
                 <div class="col-md-12">
                   <div class="choose-us-header head_focus sec-head three  wow fadeInUp " data-wow-delay="0.2s">
                     <h1>Sectors</h1>
                   </div>
                 </div>
                 <div class="col-md-12   wow fadeInUp " data-wow-delay="0.4s">
                   <ul role="tablist" class="nav nav-tabs">
                     <li role="presentation" class="active"><a href="#Retail_and_e-commerce" aria-controls="Retail_and_e-commerce" role="tab" data-toggle="tab"><span class="hidden-xs">Retail & E-commerce</span>
                    <span class="visible-xs">   <i class=" ion-ios-cart"></i></span>
  </a></li>
                     <li role="presentation"><a href="#education" aria-controls="education" role="tab" data-toggle="tab"><span class="hidden-xs">Education</span>  <span class="visible-xs">   <i class=" ion-ios-book"></i></span></a></li>
                     <li role="presentation"><a href="#media_and_entertainment" aria-controls="media_and_entertainment" role="tab" data-toggle="tab"><span class="hidden-xs">Media & Entertainment</span> <span class="visible-xs">   <i class="ion-camera"></i></span></a></li>
                     <li role="presentation"><a href="#tourism_and_hospitality" aria-controls="tourism_and_hospitality" role="tab" data-toggle="tab"><span class="hidden-xs">Tourism & Hospitality</span> <span class="visible-xs">   <i class=" ion-location"></i></span></a></li>
                   </ul>
                   <div class="tab-content">
                     <div id="Retail_and_e-commerce" role="tabpanel" class="tab-pane in active">
                       <div class="row">
                         <div class="col-md-6">
                           <div class="tab-image-wrap wow fadeInUp " data-wow-delay="0.6s"><img src="websiteV2/img/Homepage/retail.jpg" class="img-responsive image-custome-style width-100"/></div>
                         </div>
                         <div class="col-md-6">
                           <div class="tab-content-wrap wow fadeInUp " data-wow-delay="0.8s">

                             <h2 class="tab-heading "  style="color: #72BC12;">Retail & Ecommerce</h2>
      						<p class="para">Amazon was not built in a day. Online retailers and shoppers are active on the internet and are very demanding. Setting up a storefront for your identified niche can be done by a systematized process. With the coming of the smartphones and access to high speed internet, the industry is set to grow by 200% over the next five years.

</p>

<h4 class="list-head padding-top"  style="color: #72BC12;">What We Offer</h4>
                <div class="bullet-list1 bullet-list padding-left-30 overflow-scroll-y scroll margin-bottom">
                  <ul>
                    <li>Online store front</li>
                    <li>Manufacturer, Distributor connect</li>
                    <li>B2B, B2C commerce</li>
                    <li>Mobile commerce</li>
                    <li>Workflow management</li>
                    <li>Coupons and promotions</li>
                    <li>Scheduling</li>
                    <li>Integration with multi-payment gateways</li>
                    <li>Conversion tracking</li>
                    <li>Real-time product availability, pricing updates</li>
                  </ul>
                </div>
                           </div>
                         </div>
                       </div>
                     </div>
                     <div id="education" role="tabpanel" class="tab-pane">
                       <div class="row">
                         <div class="col-md-6">
                           <div class="tab-image-wrap"><img src="websiteV2/img/Homepage/education.jpg" class="img-responsive image-custome-style width-100"/></div>
                         </div>
                         <div class="col-md-6">
                           <div class="tab-content-wrap">
                             <h2 class="tab-heading" style="color: #0F719E;">Education</h2>
                   <p class="para">Education industry is booming across the world be it online, private or in-school. This industry consists of schools, colleges, universities and private institutions. Traditional colleges and universities are increasingly beginning their digital transformation journey. From cybersecurity to social media profile screening, the industry has interesting problems to be solved. </p>
                   <h4 class="list-head padding-top" style="color: #0F719E;">What We Offer</h4>
                                   <div class="bullet-list3 bullet-list padding-left-30 margin-bottom ">
                                     <ul>
                                       <li>Pre-Admission / Admission</li>
                                       <li>Attendance / Non-Attendance</li>
                                       <li>Timetable Management</li>
                                       <li>Community Web Portal</li>
                                     </ul>
                                   </div>
                           </div>
                         </div>
                       </div>
                     </div>
                     <div id="media_and_entertainment" role="tabpanel" class="tab-pane">
                       <div class="row">
                         <div class="col-md-6">
                           <div class="tab-image-wrap"><img src="websiteV2/img/Homepage/media.jpg" class="img-responsive image-custome-style width-100"/></div>
                         </div>
                         <div class="col-md-6">
                           <div class="tab-content-wrap">
                             <h2 class="tab-heading " style="color: #4A5EE1;">Media & Entertainment</h2>
                   <p class="para">Media industry is undergoing a digital revolution. Transformation has been the key for strengthening their brand and the readership. Big data analytics helps companies get a 360 degree of their audience in addition to understanding the trend setters and key media influencers. Strategy to capture and convert their interests online will help companies reshape their core areas of advertising and marketing efficiency in the coming years.</p>

                   <h4 class="list-head padding-top" style="color: #4A5EE1;">What We Offer</h4>
                                     <div class="bullet-list5 bullet-list padding-left-30 overflow-scroll-y scroll margin-bottom">
                                       <ul>
                                         <li>Latest News</li>
                                         <li>Cross channel promotion</li>
                                         <li>Advertisement sales and tracking</li>
                                         <li>Operations and functional excellence</li>
                                         <li>Consumer surveys and analytics</li>
                                         <li>Touch point analysis</li>
                                       </ul>
                                     </div>
                           </div>
                         </div>
                       </div>
                     </div>
                     <div id="tourism_and_hospitality" role="tabpanel" class="tab-pane">
                       <div class="row">
                         <div class="col-md-6">
                           <div class="tab-image-wrap"><img src="websiteV2/img/Homepage/travel.jpg" class="img-responsive image-custome-style width-100"/></div>
                         </div>
                         <div class="col-md-6">
                           <div class="tab-content-wrap">
                             <h2 class="tab-heading " style="color: #CDC70F;">Tourism & Hospitality</h2>
                   <p class="para">Tourism & Hospitality has become a multi-billion dollar industry. With the technological shift, online travel bookings grew exponentially and its impact on the global tourism industry is impressive and far reaching. Technology plays a vital role in connecting the various wings from reserving airline tickets, renting cars to booking tours all at the same time. </p>
                   <h4 class="list-head padding-top" style="color: #CDC70F;">What We Offer</h4>
                                    <div class="bullet-list4 bullet-list padding-left-30 overflow-scroll-y scroll margin-bottom">
                                      <ul>
                                        <li>Booking and payments integrated on the website</li>
                                        <li>Conversion and tracking packages, sales</li>
                                        <li>Online support system</li>
                                        <li>Sell via world's top online travel agents including Booking, Expedia and Viator</li>
                                        <li>Sync your availability on all channels with just one update</li>
                                        <li>Custom CMS with reports, notifications and dashboard </li>
                                        <li>Revenue management</li>
                                        <li>Payment processing </li>
                                        <li>Social media promotion</li>
                                        <li>Booking notes</li>
                                      </ul>
                                    </div>

                           </div>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </section>



        <section class=" industries-section ">



      <div class="back_sec_col section">
        <section id="icon-3-col" class=" icon-3-col">
          <div class="container">
            <div class="row">
             <div data-wow-delay="0.3s" class="col-md-12 text-center section-heading-wraper wow fadeInUp">
                   <div class="">
                      <h2>Features</h2>
                   </div>
              </div>
              <div data-wow-delay="0.2s" class="col-md-4 col-sm-6 wow fadeInUp">
                <div class="item">
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="icon">
                        <div class="line-top"></div><span class="ion-android-checkmark-circle"></span>
                        <div class="line-bottom"></div>
                      </div>
                    </div>
                    <div class="col-xs-9">
                      <h6 class="sub">Reliable</h6>
                     <h4>Quality and Adaptability</h4>
                      <p class="desc">Our custom solutions ensure the highest level of accuracy, quality,adaptability and the manuverability to innovate and adds business value.</p>
                    </div>
                  </div>
                </div>
                <!-- /.item-->
              </div>
              <div data-wow-delay="0.4s" class="col-md-4 col-sm-6 wow fadeInUp">
                <div class="item">
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="icon">
                        <div class="line-top"></div><span class="ion-arrow-shrink"></span>
                        <div class="line-bottom"></div>
                      </div>
                    </div>
                    <div class="col-xs-9">
                      <h6 class="sub">No compromise</h6>
                      <h4>Reduce Development costs</h4>
                      <p class="desc">Automated solutions for all day to day activities to speed up your operations at competitive prices.</p>
                    </div>
                  </div>
                </div>
                <!-- /.item-->
              </div>
              <div class="clearfix visible-sm"></div>
              <div data-wow-delay="0.6s" class="col-md-4 col-sm-6 wow fadeInUp">
                <div class="item">
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="icon">
                        <div class="line-top"></div><span class="ion-android-time"></span>
                        <div class="line-bottom"></div>
                      </div>
                    </div>
                    <div class="col-xs-9">
                      <h6 class="sub">Project Completion</h6>
                      <h4>On time, every time.</h4>
                      <p class="desc">Out­of­box custom software solutions with additional security features to stay safe and achieve desired results within the specified time frame.</p>
                    </div>
                  </div>
                </div>
                <!-- /.item-->
              </div>
              <div class="clearfix hidden-sm"></div>

            <!-- </div> -->
            <!-- /.row-->
            <!-- <div class="row"> -->
              <div data-wow-delay="0.8s" class="col-md-4 col-sm-6 wow fadeInUp">
                <div class="item">
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="icon">
                        <div class="line-top"></div><span class="ion-android-laptop"></span>
                        <div class="line-bottom"></div>
                      </div>
                    </div>
                    <div class="col-xs-9">
                      <h6 class="sub">Reacting Quickly</h6>
                      <h4>Improved responsiveness</h4>
                      <p class="desc">Improve your overall operations with streamlined business methods with the help of customized application software; that paves way for true business transformation and happy customers.</p>
                    </div>
                  </div>
                </div>
                <!-- /.item-->
              </div>
              <div data-wow-delay="1.0s" class="col-md-4 col-sm-6 wow fadeInUp">
                <div class="item">
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="icon">
                        <div class="line-top"></div><span class="ion-android-people"></span>
                        <div class="line-bottom"></div>
                      </div>
                    </div>
                    <div class="col-xs-9">
                      <h6 class="sub">Highly Trained</h6>
                      <h4>Technical staff</h4>
                      <p class="desc">At Acestra, our people are passionate, highly trained and experienced and we understand you and your business. We can work together and find the  technology solution that best suits your needs.</p>
                    </div>
                  </div>
                </div>
                <!-- /.item-->
              </div>
              <div data-wow-delay="1.2s" class="col-md-4 col-sm-6 wow fadeInUp">
                <div class="item">
                  <div class="row">
                    <div class="col-xs-3">
                      <div class="icon">
                        <div class="line-top"></div><span class="ion-help-buoy"></span>
                        <div class="line-bottom"></div>
                      </div>
                    </div>
                    <div class="col-xs-9">
                      <h6 class="sub">Need help?</h6>
                      <h4>24/7 Tech support</h4>
                      <p class="desc">Our technical resource team at Acestra provide 24/7 support . We  proactively monitor and prevent failure from happening. Routine  tests are done to ensure all your data, software and all other technicalities are not compromised .</p>
                    </div>
                  </div>
                </div>
                <!-- /.item-->
              </div>
            </div>
            <!-- /.row-->
          </div>
          <!-- /.container-->
          <!-- /.icon-3-col-->
        </section>
      </div>
              </section>
      <!-- Fun Facts-->
      <!-- <section id="funfacts" style="background-image:url('/websiteV2/img/home_fix.jpg')" class="funfacts section">
        <div class="container">
          <div class="section-content">
            <div class="row">
              <div class="col-sm-3 single-counter">
                <div class="counter-box text-center"><i class="ion-android-globe"></i>
                  <div class="count number">89</div>
                  <div class="count-description">Web Development</div>
                </div>
              </div>
              <div class="col-sm-3 single-counter">
                <div class="counter-box text-center"><i class="ion-android-desktop"></i>
                  <div class="count number">91</div>
                  <div class="count-description">Software Development</div>
                </div>
              </div>
              <div class="col-sm-3 single-counter">
                <div class="counter-box text-center"><i class="ion-android-phone-portrait"></i>
                  <div class="count number">85</div>

                  <div class="count-description">Mobile App Development</div>
                </div>
              </div>
              <div class="col-sm-3 single-counter">
                <div class="counter-box text-center"><i class="ion-android-color-palette"></i>
                  <div class="count number">95</div>
                  <div class="count-description">UI Design</div>
                </div>
              </div>
            </div>

          </div>

        </div>

      </section> -->
      <section class="section-padding bak_col_new">

<div class="container-fluid our_pro_det">
  <div class="row">
    <div class="col-md-12 text-center">
      <h2 class="wow fadeInUp " data-wow-delay="0.2s">Our Products</h2>
<div class="owl-carousel padding-bottom ">
    <div class="item wow slideInUp " data-wow-delay="0.6s"><h4><img src="/websiteV2/img/logos/mylporetoday1.jpg" alt="mylporetoday"></h4></div>
    <div class="item wow slideInUp " data-wow-delay="0.8s"><h4><img src="/websiteV2/img/logos/kollywoodtalkies1.jpg" alt="kollywoodtalkies"></h4></div>
    <div class="item wow slideInUp " data-wow-delay="1.0s"><h4><img src="/websiteV2/img/logos/kalyanseva1.jpg" alt="kalyanseva"></h4></div>
    <div class="item wow slideInUp " data-wow-delay="0.2s"><h4><img src="/websiteV2/img/logos/aceconnect1.jpg" alt="aceconnect"></h4></div>
    <div class="item wow slideInUp " data-wow-delay="1.2s"><h4><img src="/websiteV2/img/logos/acerealty1.jpg" alt="acerealty"></h4></div>
    <div class="item wow slideInUp " data-wow-delay="0.4s"><h4><img src="/websiteV2/img/logos/schoollocater1.jpg" alt="schoollocater"></h4></div>

</div>
<a href="/products" class=" btn1 btn-1 btn-1e  wow fadeInUp " data-wow-delay="1.4s">View more products</a>
<!-- <button class="btn button-default"></button> -->


</div>
<!-- </div> -->


      </section>

      <section id="funfacts" class="funfact-3 text-center section">

    <div class="container">
           <div class="row">

             <div class="col-sm-3 single-counter fun-1 wow fadeInUp " data-wow-delay="0.4s">
               <div class="counter-box"><i class="ion-ios-monitor"></i>
                 <div class="count number">89</div>
                 <div class="count-description">Web Development</div>
               </div>
             </div>
             <div class="col-sm-3 single-counter fun-2 wow fadeInUp " data-wow-delay="0.6s">
               <div class="counter-box"><i class="ion-code"></i>
                 <div class="count number">15</div>
                 <div class="count-description">Software Development</div>
               </div>
             </div>
             <div class="col-sm-3 single-counter fun-3 wow fadeInUp " data-wow-delay="0.8s">
               <div class="counter-box"><i class="ion-iphone"></i>
                 <div class="count number">25</div>
                 <div class="count-description">Mobile App Development</div>
               </div>
             </div>
             <div class="col-sm-3 single-counter fun-4 wow fadeInUp " data-wow-delay="1.0s">
               <div class="counter-box"><i class="ion-android-color-palette"></i>
                 <div class="count number">95</div>
                 <div class="count-description">UI Design</div>
               </div>
             </div>
           </div>
         </div>
       </section>
       <section id="why-choose-us" class="why-choose-us section">
            <div class="container">
              <div class="row">
                <div class="col-md-12">
                  <div class="choose-us-header  head_focus sec-head four   wow fadeInUp " data-wow-delay="0.2s">
                    <h1>Why Choose Us</h1>
                  </div>
                </div>
                <div class="col-md-12   wow fadeInUp " data-wow-delay="0.4s">

                  <ul role="tablist" class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="#technologies" aria-controls="technologies" role="tab" data-toggle="tab"><span class="hidden-xs">Technologies</span> <span class="visible-xs">   <i class="ion-ios-lightbulb-outline"></i></span></a></li>
                    <li role="presentation"><a href="#news" aria-controls="news" role="tab" data-toggle="tab"><span class="hidden-xs">News</span> <span class="visible-xs">   <i class="ion-earth"></i></span></a></li>

                  </ul>
                  <div class="tab-content">
                    <div id="technologies" role="tabpanel" class="tab-pane in active">
                      <div class="row">
                						<div class="col-md-6 col-sm-6 sm-mrb40   wow fadeInUp " data-wow-delay="0.6s">
                              <div class="logo-borderd-grid">
                                <div class="">
                                  <div class="row no-gutter first-row">
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="Html 5"><i class="ion-social-html5-outline" style="color:#FF5733;"></i></div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="Css 3"><i class="ion-social-css3-outline" style="color:#00A0EA;"></i></div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="Advanced Javascript"><i class="ion-social-javascript-outline"  style="color:#F0DB4F;" ></i></div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="Angular"><i class="ion-social-angular-outline" style="color:#DD0031;" ></i></div>
                                    </div>

                                  </div>
                                  <div class="row no-gutter second-row">
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="node JS"><i class="ion-social-nodejs" style="color:#90C53F;" ></i> </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="sass"><i class="ion-social-sass" style="color:#CF649A;"></i></div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="IOS App"><i class="ion-social-apple-outline" style="color:#A6B0BC;"></i> </div>
                                    </div>
                                    <div class="col-xs-6 col-sm-3 col-md-3">
                                      <div class="logo-wrap p-logo funfacts" title="Android App"><i class="ion-social-android-outline" style="color:#8BC34A;"></i></div>
                                    </div>

                                  </div>
                                   <div class="row no-gutter third-row">
                                     <div class="col-xs-6 col-sm-3 col-md-3">
                                       <div class="logo-wrap p-logo funfacts" title="laravel"><img src="/websiteV2/img/icons/laravel.png" alt="laravel" class="img-responsive"></div>
                                     </div>

                                     <div class="col-xs-6 col-sm-3 col-md-3">
                                       <div class="logo-wrap p-logo funfacts" title="WordPress"><i class="ion-social-wordpress-outline" style="color:#21759B;" ></i></div>
                                     </div>
                                     <div class="col-xs-6 col-sm-3 col-md-3">
                                       <div class="logo-wrap p-logo funfacts" title="php"><img src="/websiteV2/img/icons/PHP-logo.png" alt="PHP-logo" style="margin-top: 15px;" class="img-responsive"></div>
                                     </div>
                                     <div class="col-xs-6 col-sm-3 col-md-3">
                                       <div class="logo-wrap p-logo funfacts" title=".net"><img src="/websiteV2/img/icons/net.png" alt="net" class="img-responsive" style="margin-top: 15px;"></div>
                                     </div>

                                  </div>
                                               <div class="row no-gutter third-row">
                                  <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="mongo DB"><img src="/websiteV2/img/icons/mongodb.png" alt="mongodb" class="img-responsive"  style="width: 75%; margin-top: 15px;"> </div>
                                  </div>
                                  <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="postgre SQL"><img src="/websiteV2/img/icons/postgreesql.png" alt="postgreesql" class="img-responsive" style="width: 75%;margin-top: 15px;"></div>
                                  </div>
                                  <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="Python"><img src="/websiteV2/img/icons/python200x200.png" alt="python200x200" class="img-responsive" style="width: 75%;margin-top: 15px;"></div>
                                  </div>

                                  <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="Redis"><img src="/websiteV2/img/icons/rediss.png" alt="redis" class="img-responsive" style="width: 75%;margin-top: 15px;"></div>
                                  </div></div>
                                               <div class="row no-gutter third-row">
                                  <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="Share Point"><img src="/websiteV2/img/icons/sharepnt.png" alt="sharepnt" class="img-responsive" style="width: 75%;margin-top: 15px;"></div>
                                  </div>
                                  <div class="col-xs-6 col-sm-4 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="Bootstrap"><img src="/websiteV2/img/icons/bootstrap.png" alt="bootstrap" class="img-responsive" style="width: 75%;margin-top: 15px;"></div>
                                  </div>
                                  <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="C#"><img src="/websiteV2/img/icons/csharp.png" alt="csharp" class="img-responsive" style="width: 75%;margin-top: 15px;"></div>
                                  </div>

                                  <div class="col-xs-6 col-sm-3 col-md-3">
                                    <div class="logo-wrap p-logo funfacts" title="Ionic"><img src="/websiteV2/img/icons/ionic.png" alt="ionic" class="img-responsive" style="width: 75%;margin-top: 15px;"></div>
                                  </div>

                                </div>
                                </div>
                          </div>
                        </div>
                        <div class="col-md-6   wow fadeInUp " data-wow-delay="0.8s">
                          <div class="tab-content-wrap">
                            <h2 style="color: #72BC12;">Build a beautiful user interface with mountain</h2>
                            <p>
                              Mountain is a clean and unique multi-purpose template for all kinds of sites &amp; business. Mountain was designed especially for Corporate Site, Ecommerce site, Photo Studio, Personal Blog, Shop, business companies, corporate agencies, organizations, creative designers and much more. This template can easily satisfy all of your needs.
                              The template files are fully layered and customizable and all elements are in groups and can easily identify by the group name as well.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div id="news" role="tabpanel" class="tab-pane">
                      <div class="row">
                        <div class="col-md-6">
                          <div class="tab-image-wrap"><img src="websiteV2/img/Homepage/news1.jpg" class="img-responsive image-custome-style margin-bottom" alt="news"/></div>
                        </div>
                        <div class="col-md-6">
                          <div class="tab-content-wrap">
                            <div class="expl-list news-list">
                              <ul>
                                <!-- <li ng-repeat="news_one in news_list | limitTo:4">
                                   <span class="ion-document-text icon padding-right-10px"></span>
                                  <p class="news-heading"><span  ng-bind="news_one.date"></span></p>
                                  <p class="txt"><a ng-href="@{{news_one.link}}"><span  ng-bind="news_one.title"></span></a></p>
                                </li> -->
                                <li ng-repeat="news_one in news_list | limitTo:4"><span class="ion-document-text icon padding-right-10px"></span>
                                  <p class="news-heading"><span class="title-size">@{{news_one.modified | angularDateFormat | date:'MMM d, y'}}</span></p>
                                  <p class="txt"><a ng-href="@{{news_one.link}}"><span  ng-bind="news_one.title" class="description-size"></span></a></p>
                                </li>
                                <!-- <li><span class="ion-document-text icon padding-right-10px"></span>
                                  <p class="news-heading"><span class="title-size">28th April 2015</span></p>
                                  <p class="txt"><a href="#"><span class="description-size">Acestra  partners with Rice University research group to Cardiac wellness app</span></a></p>
                                </li>
                                <li><span class="ion-document-text icon padding-right-10px"></span>
                                  <p class="news-heading"><span  class="title-size">10 Feb 2015</span></p>
                                  <p class="txt"><a href="#"><span  class="description-size">Coastal Flow Announces BirdDog IS Mobile App for Apple iPhone</span></a></p>
                                </li> -->
                              </ul>
                            </div>
                            <a class="btn  button-default  padding-top" href="/blog">Check more news <i class="ion-chevron-right padding2"></i></a>

                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </section>



<!-- <section id="landing-layout1-logo" class="landing-layout1-logo section">

      </section> -->

 <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
    <script src="/websiteV2/assets/lib/jquery/dist/app10.js"></script>





@stop
