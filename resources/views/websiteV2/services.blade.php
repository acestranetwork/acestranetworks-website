@extends('layouts/appV2')
@section('content')




<section class=" padding-top-80">
  <div id="particles-js2">
        <div class="container"  id="innerheader2">
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="">
                <h1 class="header-title_new_part_add header-title_new_part"><span
     class="txt-rotate"
     data-period="2000"
     data-rotate='[ "Our Proficiency"]'></span>
                <p class=" inner-title hidden-xs">Acestra Network provides innovative solutions and top quality services for a great variety of business organizations. We can help you by developing and maintaining software solutions, upgrade technology and migrate services to cloud.</p></h1>
              </div>

              <!-- <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Services</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>Services</li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
        </div>
      </section>
      <section id="ourExpertise1" class="ourExpertise1 section">
            <div class="container">
              <div class="row">
                <div class="col-sm-12 head_focus sec-head one  wow fadeInUp expertise-header" data-wow-delay="0.2s">
                  <div>
                    <h2>Our Proficiency</h2>
                  </div>
                </div>
                <div class="col-sm-12 col-md-4 sm-push-bottom-120  wow fadeInUp " data-wow-delay="0.4s">
                  <p>
                    Create a website that you are gonna be proud of. Be it Business Portfolio, Agency,
                    Photography, eCommer developmen, startup and much more.
                  </p>
                  <!-- <a href="/services" data-wow-delay="0.3s" class="btn btn-mountain-flat btn-round wow fadeInUp" >Learn More</a> -->
                </div>
                <div class="col-sm-12 col-md-8">
                  <div id="expertise-circle-wraper" class="circle-wrapper clearfix">
                    <div id="expertise-circle" data-color="rgb(255,87,51)" data-bg-color="rgba(255,87,51,0.4)" data-value="75" class="expertise-circle wow fadeInUp " data-wow-delay="0.4s">
                      <div class="expertise"></div>
                      <h4 class="text-center">html5</h4>
                    </div>
                    <div id="expertise-circle-2" data-color="rgb(0,160,234)" data-bg-color="rgba(0,160,234,0.4)" data-value="90" class="expertise-circle wow fadeInUp " data-wow-delay="0.6s">
                      <div class="expertise"></div>
                      <h4 class="text-center">css3</h4>
                    </div>
                    <div id="expertise-circle-3" data-color="rgb(247,227,79)" data-bg-color="rgba(247,227,79,0.4)" data-value="65" class="expertise-circle wow fadeInUp " data-wow-delay="0.8s">
                      <div class="expertise"></div>
                      <h4 class="text-center">javascript</h4>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </section>


      <!-- /#innerheader2-->
       <div  class="service-3  text-center section-padding " id="id_head">
         <!-- <div class="well clearfix well-sticky  hidden-xs" id="well">




     <ul class="nav navbar-nav" style="width:100%">
  <li class="active"><a class="btn button-default btn-xs" href="#section1">Software Development</a></li>
<li>  <a class="btn  button-default btn-xs" href="#section2">Custom Web Solution</a></li>
<li>  <a class="btn  button-default btn-xs" href="#section3">Mobile Application</a></li>
<li>  <a class="btn  button-default btn-xs" href="#section4">Data Consultancy</a></li>
<li>  <a class="btn  button-default btn-xs" href="#section5">Technical Audit</a></li>
<li>  <a class="btn  button-default btn-xs" href="#section6">Digital Marketing</a></li>
</ul>

         </div> -->

        <div class="container">
          <!-- <div class="row">
            <div class="col-sm-12">
              <div class="header-classic">
                <h2 class="section-title">Our Services</h2>
                <p class="section-subtitle">Acestra Network provides innovative solutions and top quality services for a great variety of business organizations. We can help you by developing and maintaining software solutions, upgrade technology and migrate services to cloud.</p>
              </div>
            </div>
          </div> -->
          <div class="row">
<div class="col-md-12 col-xs-12   padding-top padding-bottom" id="section1">


              <div class="service-box">
<div class="col-md-5 col-sm-5 col-xs-12 wow fadeInUp " data-wow-delay="0.2s">
                <!-- <span class="ion-android-laptop"></span> -->

                <img class="img-responsive image-custome-style margin-top" src="websiteV2/img/services/software.jpg"/>
              </div>
                <div class="col-sm-7 col-sm-7 col-xs-12 text-left wow fadeInUp " data-wow-delay="0.4s">
                <h3 style="color: #72BC12;">Software</h3>
                <p>Our websites are created using a rare blend of design and development expertise resulting in effective web presence. We take pride in supporting clients with unique solutions delivered in a timely manner.</p>


              <div class="bullet-list1 bullet-list padding-left-30 margin-top ">
                <ul>
                  <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Product wire framing</li>
                  <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block">Custom application development</li>
                  <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block">User experience & user interface design</li>
                  <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block">Integration and automation</li>
                  <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block">Code review</li>
                  <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block">Security audit</li>
                </ul>
              </div>
         <!-- </ul> -->




      </div>
              </div>
</div>
<div class="col-md-12 col-xs-12 padding-top  padding-bottom" id="section2">
              <div class="service-box text-center">
                <div class="col-sm-5 col-sm-5 col-xs-12 wow fadeInUp " data-wow-delay="0.6s">
                      <img class="img-responsive image-custome-style margin-top" src="websiteV2/img/services/cloud.jpg"/>

              </div>
              <div class="col-sm-7 col-sm-7 col-xs-12 text-left wow fadeInUp " data-wow-delay="0.8s">
                <h3 style="color:#ED6A24">Cloud</h3>
                <p>Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea consequat.
                </p>

         <div class="bullet-list2 bullet-list padding-left-30 margin-top">
           <ul class="serv_offer">
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">End to end IT solutions </li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Data migration</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Storage management</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">DevOps</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Data center management</li>
           </ul>

         </div>

              </div>
              </div>
</div>
<div class="col-md-12 col-xs-12 padding-top padding-bottom" id="section3">
              <div class="service-box text-center">
                  <div class="col-md-5 col-sm-5 col-xs-12 wow fadeInUp " data-wow-delay="0.2s">
                    <img class="img-responsive image-custome-style margin-top" src="websiteV2/img/services/mobile.jpg"/>

                  </div>
                    <div class="col-md-7 col-sm-7 col-xs-12 text-left wow fadeInUp " data-wow-delay="0.4s">
                <h3 style="color: #4A5EE1;" >Mobile</h3>
                <p>We design flexible and efficient mobile applications aligned to the specific goals of our clients. Our wide range of applications allow you to access information easily and conveniently giving you a great user friendly experience in a time efficient manner.
                </p>

         <div class="bullet-list padding-left-30 margin-top scroll2">
           <ul class="serv_offer" >
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Enterprise mobility solutions  </li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Business apps development</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Mobile website development</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Native mobile application development</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Apps and website development for tablets</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Integration Services for ERP, CRM and Legacy Applications</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Geo tracking/fencing</li>
             <li class="visible-lg-block visible-md-block visible-sm-block visible-xs-block ">Mobile QA and Testing</li>

           </ul>

         </div>

              </div>


</div>
</div>

        </div>
      </div></div>
      <section class="section-padding padding-bottom-zero">
        <div class="container">
          <div class="row">

            <div class="col-md-12 head_focus sec-head two wow slideInLeft" data-wow-delay="0.2s">

              <h2>Our process</h2>
            </div>
              <div class="col-md-8 col-md-offset-2  wow fadeInUp" data-wow-delay="0.4s">

                                <img class="img-responsive pad2" src="websiteV2/img/services/process_1.png"/>
              </div>
          </div>
        </div>
      </section>
       <section id="cta4" class="cta4">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="title-subtitle text-center">
                <p>Thank you for exploring Acestra Network service options. Please contact us with any questions or leave us your feedback.</p>
                 <div style="margin-bottom:25px;" class="clearfix"></div>
                 <a data-wow-delay="0.8s" href="/quote" class="clearfix btn btn-mountain-o  btn-round">Request Quote!</a>
              </div>
            </div>
          </div>
        </div>
      </section>

   <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
    <script src="/websiteV2/assets/js/app1.js"></script>

     @stop
