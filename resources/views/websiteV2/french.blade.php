@extends('layouts/appV2')
@section('content')

<section class="padding-top-80" >
<div id="particles-js4" >

  <div class="container" id="innerheader2" class="innerheader innerheader2">
    <div class="row pad-small">
      <div class="col-sm-12 text-center">
        <div class="inner-title">
          <h1 class="header-title_new_part_pro header-title_new_part"><span
class="txt-rotate"
data-period="2000"
data-rotate='[ "À propos de nous"]'></span></h1>
        </div>
        <!-- <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
              <ol class="breadcrumb">
                <li><a href="/" >Home</a></li>
                <li>À propos de nous</li>
              </ol>
            </div> -->
      </div>
    </div>
  </div>
  </div>
</section>

            <!-- /#innerheader2-->
      <section id="video" class="video video1 section">
        <div class="container">
          <div class="">
                <div class="col-md-12 sec-head one">
                  <h3 data-wow-delay="0.1s" class="section-title wow fadeInUp">Joignez les mains avec Acestra success</h3>

                  <p data-wow-delay="0.2s" class="wow fadeInUp">Depuis sa création, ACESTRA a évolué pour devenir une société de développement de logiciels compétents fournissant des solutions polyvalentes , évolutives , modulaires . Nous sommes passés d' un petit groupe de programmeurs de logiciels dédiés à une maison de développement de logiciels à grande échelle dans les deux Web et le développement d'applications mobiles. .</p>

                </div>
                  <div class="col-md-12">
              <div class="row">
                <div class="col-sm-7 padding-top-20">
                  <p data-wow-delay="0.3s" class="wow fadeInUp">Nous sommes un groupe de professionnels passionnés , dédiés et de confiance dans les logiciels et le matériel d'ingénierie , les analystes , les programmeurs , les professionnels de commerce électronique, les professionnels de la conception de sites Web et les développeurs d'applications mobiles.</p> <p data-wow-delay="0.4s" class="wow fadeInUp">Nous avons soutenu les entreprises à travers un large éventail d'industries , notamment la fabrication , les soins de santé , les services financiers , juridiques, gouvernement , immobilier, commerce de détail et les organisations sans but lucratif. Avec nos forces et l'expérience en gestion de projet , la conception créative , de la technologie et développement de logiciels de commerce électronique, ACESTRA RÉSEAUX aide ses clients à améliorer leurs performances , gagner recentrage sur les activités de pointe, des normes de qualité élevées et surtout atteindre une rentabilité accrue..</p>
                </div>
                <div class="col-sm-4">
                  <div class="post-img-wrap">
                     <img src="/websiteV2/img/join.jpg" class="img-responsive">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
        <!-- /#innerheader1-->
      <section id="cta2" class="cta2">
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="cta2-text">
                <h4 class="text-center"><sup><i class="ion-quote"></i></sup>&nbsp;Notre mission est de faire original, travail novateur qui est inspiré par nos clients " marque , les objectifs d'affaires et du budget et de la valeur pour l'argent.&nbsp;<sup><i class="ion-quote rotate"></i></sup></h4>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section id="cta4" class="cta4 bgsec">
        <div class="container">
          <div class="row">
            <div class="col-sm-12">
              <div class="title-subtitle text-center">
              <!--  <img src="/websiteV2/img/team.jpg" class="img-responsive">     -->
                <p>Notre objectif est d'exécuter vos projets en utilisant amélioré l'état des logiciels de pointe . notre équipe dévouée et bien équipée cherche à obtenir des résultats rapides et précis.</p>
                 <div style="margin-bottom:25px;" class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
     <script src="/websiteV2/assets/js/app3.js"></script>
@stop
