@extends('layouts/appV2')
@section('content')
<section class="padding-top-80">
  <div  id="particles-js6" >
       <div class="container" id="innerheader2" >
         <div class="row pad-small">
           <div class="col-sm-12 text-center">
             <div class="">
               <h1 class="header-title_new_part_pro header-title_new_part"><span
    class="txt-rotate"
    data-period="2000"
    data-rotate='["Terms and Conditions"]'></span></h1>
  </div>
             <!-- <div class="inner-title">
               <h3 data-wow-delay="0.1s" class="wow fadeInUp">Careers</h3>
             </div>
             <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
               <ol class="breadcrumb">
                 <li><a href="/">Home</a></li>
                 <li>Careers</li>
               </ol>
             </div> -->
           </div>
         </div>
       </div></div>
     </section>
 <!-- <section id="innerheader2" style="" class="innerheader innerheader2">
        <div class="container">
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Terms and Conditions</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="/">Home</a></li>
                  <li>Terms and Conditions</li>
                </ol>
              </div>
            </div>
          </div>
        </div>
      </section> -->

            <!-- /#innerheader2-->
<section class="section privacy">
        <div class="container">
          <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
              <h2 class="text-center marginBottom20">Terms and Conditions</h2>
              <p>Definitions: In this agreement the terms have the following meanings</p>
              <ol>
                <li><p>"The Client" means any person, company, partnership, organisation or body who Acestra Networks Pvt ltd deals with and Acestra Networks Pvt ltd agrees to provide the products and/or services under the terms of the agreement.</p></li>
                <li><p>"The Agreement" means the contract between Acestra Networks Pvt ltd and the Client to which these conditions will apply.</p></li>
                <li><p>"The Order Form" means the digital or printed document completed by the client in order to enter the Agreement.</p></li>
                <li><p>"Order" is the request by the Client for products and/or services in the order form/agreement.</p></li>
                <li><p>"Deliverables" are the outputs of services or products to be supplied under the agreement and shall include but are not limited to, all software and written material, including programs, tapes, listings and other programming documentation.</p></li>
              </ol>

              <h4>Acceptance of Terms</h4>
              <p>The following Terms and Conditions ("the Conditions") are the terms on which Acestra Networks Pvt ltd provides services and supersedes all other terms and conditions. Acestra Networks Pvt ltd shall provide services to the Client as described and specified in the proposal provided by Acestra Networks Pvt ltd.</p>
               <p>The headings in these Conditions are inserted for convenience of reference only and are not intended to be part of, or to affect the meaning, or interpretation of any of the Conditions.</p>
                <p>If any term or provision of these Conditions is held invalid, illegal or unenforceable for any reason by any Court of competent jurisdiction, such provision shall be severed, and the remainder of the provisions shall continue in full force and effect.</p>
                 <p>All prices specified in the Agreement will be honored for 3 months from the date of contract. Acceptance of the Agreement need to be signed in writing within the specified time in order for the Agreement to be valid.</p>

              <h4>Scope</h4>
              <p>The Agreement shall apply to all services and/or products ordered by the Client from Acestra Networks Pvt ltd.</p>

              <h4>General Disclaimer</h4>
              <p>Acestra Networks Pvt ltd disclaims all warranties, either express or implied, including the warranties or merchantability and fitness for a particular purpose. In no event shall Acestra Networks Pvt ltd be liable for any damages whatsoever including direct, indirect, incidental, consequential, loss of business profits or special damages, even if Acestra Networks Pvt ltd or third party agents have been advised of the possibility of such damages.</p>

              <h4>Liability</h4>
               <p>Acestra Networks Pvt ltd, its Employees and/or Contractors will not be held liable from:- all and any liability for loss or damage caused by any inaccuracy; omission; delay or error as the result of negligence or other cause in the production of the service or product; Service interruptions; All and any liability for use of content provided by the Client; All and any liability for loss or damage to clients artwork/photos, supplied for the site. Immaterial whether the loss or damage results from negligence or otherwise.</p>

              <h4>Payment</h4>
              <p>Acestra Networks Pvt ltd expects the Client to make the payments promptly based on the agreed schedule. Whilst any payment due under the Agreement remains outstanding, Acestra Networks Pvt ltd shall be entitled at its sole and absolute discretion to withhold provision of any services and/or products it would otherwise be obliged to provide under the Agreement. All payments by cheque, bankers draft or money order must be made in Canadian dollars unless agreed upon in writing by both parties. All credit card and debit card transactions will be processed in Canadian dollar.</p>
              <p>Price quoted by Acestra Networks Pvt ltd can change and should be agreed at the time of contract. If the Client ceases to trade after work has commenced, any outstanding balance must still be paid.</p>

              <h4>Service Acceptance</h4>
              <p>You shall keep secure any identification, password and other confidential information relating to your account. You shall notify Acestra Networks Pvt ltd immediately of any known or suspected unauthorized use of your account or breach of security, including loss, theft or unauthorized disclosure of your password or other security information.</p>
              <p>On reasonable notice to the Client, Acestra Networks Pvt ltd shall take any and all steps it determines to be necessary to maintain the Services, which may include (without limitation) altering or suspending Services during maintenance. Acestra Networks Pvt ltd will try to ensure that such maintenance, alterations and suspensions to the Services occur outside of normal business hours.</p>
              <p>In the case of an individual User, you warrant that you are at least 18 years of age and if the User is a company, you warrant that the Services will not be used by anyone under the age of 18 years.</p>
              <p>Provided that the Customers perform their obligations under these Conditions, Acestra Networks Pvt ltd represents and warrants that the Services will be provided with reasonable skill and due care. Acestra Networks Pvt ltd does not warrant that the operation of the Services will be uninterrupted, error-free or secure. Acestra Networks Pvt ltd's sole liability for any breach of this warranty shall be to re-perform the affected Services. In the event that Acestra Networks Pvt ltd determines that re-performance is not commercially feasible, Acestra Networks Pvt ltd may terminate this Agreement and will receive payment for the work to date on a pro-rata basis. What is or is not commercially feasible shall be in the sole discretion of Acestra Networks Pvt ltd.</p>

              <h4>International Use</h4>
              <p>Recognizing the global nature of the Internet, you agree to comply with all local rules regarding online conduct and acceptable Content. Specifically, you agree to comply with all laws regarding the transmission of technical data exported from the United States or the country in which you reside.</p>

              <h4>Indemnity</h4>
              <p>The Client agrees to indemnify and hold Acestra Networks Pvt ltd and it employees and associated parties harmless from and against all liabilities, legal fees, damages, losses, costs and other expenses in relation to any cliams or actionS brought against Acestra Networks Pvt ltd arising out of breach by the Client of the Terms and Conditions specified in this web page.</p>

              <h4>Disclaimers</h4>
              <p>Services and/or products are provided on an "AS IS" and "AS AVAILABLE" basis without any representation or endorsement made and without warranty of any kind whether expressed or implied.</p>
              <p>Acestra Networks Pvt ltd is a professional and "family-friendly" business entity. We reserve the right to limit our service to only those individuals, businesses and non-profit organizations whose needs we can realistically meet. Any site may link to our site without permission as long as your site is clean, family-friendly and follows our general philosophy.</p>

              <h4>Termination Provisions</h4>
              <p>Acestra Networks Pvt ltd reserves the right to suspend or terminate all or part of the Services with immediate effect, where there has been a breach by the Client of these terms and conditions or failure to pay outstanding charges. Either Party may terminate the Services with or without cause on giving 30 days written notice to the other. Termination of this agreement will be without prejudice to any accrued rights of either party.</p>

              <h4>Amendment </h4>
              <p>Acestra Networks Pvt ltd reserves the right to amend any provision of Terms and Conditions stated in this document at any time without notice. It is the duty of the Client to check the Terms and Conditions page on the web.</p>

              <h5>Last Updated: 01/06/2016</h5>


            </div>
          </div>
        </div>
      </section>
      <script src="/websiteV2/assets/lib/jquery/dist/particles.js"></script>
       <script src="/websiteV2/assets/lib/jquery/dist/app12.js"></script>
       @stop
