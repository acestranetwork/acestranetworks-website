@extends('website_master')
@section('web_content')
 <!--black nav--> 
<section class="about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">About Us</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top"> 
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> About us</span></h3>
      </div>
    </div>
  </div>
</section>
 <!-- About us -->
<section class="sec1">
<div class="container">
    <h3 class="feature title1">ABOUT US</h3>
  </div>
</section>    
<!-- Hands with -->
<section class="sec1"> 
  <div class="container text-left text-justify">
    <div class="row ">
      <div class="col-lg-12">
        <img src="website/img/about.png" alt="about" class="img-responsive abt_img">
        <div class="rw-wrapper">
          <h2 class="rw-sentence hidden-xs">
            <span class="marginRightneg3px">Join hands with Acestra to</span><div class="rw-words rw-words-1">
                <span>progress</span>
                <span>advance</span>
                <span>succeed</span>
                </div>
          </h2>
          <h4 class="visible-xs text-center">Join hands with Acestra to succeed</h4>
        </div>
        <p class="para_mor">
          Since inception , ACESTRA has evolved into a competent software development company providing versatile, scalable, modular solutions. We have grown from a small group of dedicated software programmers to a full scale software development house in both Web and mobile app development .
        </p>
        <p class="para_mor">
         We are a group of Passionate, dedicated and trusted professionals in software & hardware engineering, analysts, programmers, ecommerce professionals, web design professionals and mobile application developers.
        </p>
        <p class="para_mor">
         We have supported businesses across a broad range of industries that include manufacturing, healthcare, financial services, legal, government, real estate, retail and non-profit organisations. With our strengths and experience in project management, creative design, software technology and ecommerce development, ACESTRA NETWORKS helps its CLIENTS improve performance, gain advanced business focus, higher quality standards and most importantly attain increased profitability.
        </p>        
      </div>
    </div>
    <hr class="para_bottom">    
  </div>
</section>
<!-- Choose us -->
<section class="sec1 choose_bg">
  <div class="container">
    <div class="row text-left text-justify">
      <div class="col-lg-12">
        <h3 class="choose_us">OUR MISSION is to do original, innovative work that is inspired by our clients” brand, business objectives and budget and deliver value for money.</h3>
      </div>
    </div>
  </div>
</section>
<section class="sec1">
<div class="container">
<div class="row">
  <div class="col-md-12 text-center">
   <img src="/website/img/team.jpg" class="img-responsive">
   <div class="col-sm-8 col-sm-offset-2">
     <h4 class="para_mor"> Our  goal is to execute your projects using enhanced state of art software. our dedicated and well-equipped team strives to deliver prompt and accurate results.  </h4>
   </div>  
  </div>
</div>
</div>
</section>
@section('script')
  <script type="text/javascript">
  $('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
  </script>
  @stop
@stop