@extends('website_master')
@section('web_content')
 <!--black nav--> 
<section class="sec1 about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">Careers</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top">
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> Careers</span></h3>
      </div>
    </div>
  </div>
</section>
 <section class="sec1">
<div class="container">
    <h3 class="feature title1 ">Techstars wanted!</h3>
    <div class="row">
      <div class="rw-wrapper">
        <p class="para_mor">Our employees are important to us and while we offer market rate salaries, we also offer a supportive and fun work environment. Candidates can expect us to present them with opportunities that best match their talents and desires. We look forward to meeting you and thank you for your interest! </p>
      </div>
    </div>
  </div>
</section>
<section class="sec1">
  <div class="container">
    <div class="row">
    <div class="col-lg-8 col-md-8 text-left">
    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading career_border" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseP1405" aria-expanded="true" aria-controls="collapseP1405" class="panel-hover">
            <i class="fa fa-angle-up control-icon"></i>
            Web Software Developer
           <p class="pull-right">Posted on 27 Jan 2017 &nbsp;<img src="/website/img/live.png" width="50"> &nbsp;</p> 
        </a>

      </h4>
    </div>
    <div id="collapseP1405" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body career_info">
         <h5 style="color: red">Job ID: P1405 - Location: Chennai </h5>
         <p class="para_mor">We are seeking an Web Software Developer who will be responsible for designing and developing new systems, supporting and enhancing existing systems, and documenting technical specifications.</p>
         <h4 style="color: red">Training Period</h4>
          <p>
            The selected candidate shall undergo a mandatory training period of 3 months.The candidate who is willing to learn and proves his/her potential shall be absorbed as a full time employee after the successful completion of the training period. Kindly note that remuneration will be in the form of a stipend during the training period.
          </p>
          <h5 style="color: red">Job Requirements</h5>
             <ul class="" style="list-style-type:disc">
          <li>Education: Graduate degree/diploma in computer information technology or equivalent experience </li>
          <li>Basic knlowedge with: C#, ASP.NET, SQL Server, UML, Java, PHP. </li>
          <li>Technical knowledge in designing, building and implementing large scale, complex databases using SQL Server 2008/2012. </li>
          <li>Be proficient in HTML,CSS,JS,AngularJS . </li>
          <li>Functions at a high level of autonomy in setting objectives, expectations and tracking performance. </li>
          <li>Excellent verbal and written communication skills. </li>
          <li>Any Batch fresher can apply. And total vacancies 4. </li>
          <li>Provides work estimates for their own development tasks and verifies works estimates of development tasks on project plans. </li>
          <li>Have basic skills: Git/SVN, Objective C++, Ionic, PhoneGAP. <br><br>  <a href="/career/P1405/job_apply" class="btn btn-primary" alt="Wanted web software fresher">Apply Now</a> </li>
        </ul>
      </div>
    </div>
  </div>
     <div class="panel panel-default">
        <div class="panel-heading career_border" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="panel-hover">
            <i class="fa fa-angle-up control-icon"></i>
            Analyst Programmer
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body career_info">
         <h5>Job ID: P1401 - Location: Chennai - Department: Technology</h5>
         <p class="para_mor">We are seeking an Web software Programmer who will be responsible for designing and developing new systems, supporting and enhancing existing systems, and documenting technical specifications.</p>
          <h5>Job Requirements</h5>
             <ul class="" style="list-style-type:disc">
          <li>Education: Graduate degree/diploma in computer information technology or equivalent experience </li>
          <li>Three or more years of experience working with: C#, ASP.NET, SQL Server, UML, Java, PHP. </li>
          <li>Full technical knowledge and experience in designing, building and implementing large scale, complex databases using SQL Server 2008/2012. </li>
          <li>Be proficient in HTML5, XML, AJAX, SOAP. </li>
          <li> Functions at a high level of autonomy in setting objectives, expectations and tracking performance. </li>
          <li>Excellent verbal and written communication skills. </li>
          <li>Provides work estimates for their own development tasks and verifies works estimates of development tasks on project plans. </li>
          <li>Nice to have skills: Git/SVN, Objective C++, Android, PhoneGAP. <br><br>  <a href="/career/P1401/job_apply" class="btn btn-primary">Apply Now</a> </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading career_border" role="tab" id="headingTwo">
      <h4 class="panel-title">
        <a class="collapsed panel-hover" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" >
        <i class="fa fa-angle-up control-icon"></i>
        Web Application Developer
        </a>
      </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
      <div class="panel-body career_info">
         <h5>Job ID: P1402 - Location: Chennai - Department: Technology</h5>
         <p class="para_mor">We are looking for a motivated Web Developer who enjoys bringing out client's ideas to life by developing websites that stand out from the rest!</p>
          <h5>Job Requirements</h5>
             <ul class="" style="list-style-type:disc">
          <li>Education: Graduate degree/diploma in computer information technology or equivalent experience </li>
          <li>Competent with HTML5, CSS3, JavaScript, Jquery, PHP, MySQL, WordPress.</li>
          <li>Have an eye for design and must be detail-oriented.</li>
          <li>Thorough knowledge of web development techniques and procedures. </li>
          <li> Design for mobile devices, responsive design and cross-platform development.</li>
          <li>Able to prioritize multiple project deadlines.</li>
          <li>Ability to work independently with minimal supervision in a fast-paced team environment.</li>
          <li>Great communication and customer service skills.</li>
          <li>Nice to have skills: Git/SVN, Angular, Node, MongoDB <br><br>  <a href="/career/P1402/job_apply" class="btn btn-primary">Apply Now</a> </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading career_border" role="tab" id="heading3">
      <h4 class="panel-title">
        <a class="collapsed panel-hover" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="false" aria-controls="collapse3" >
        <i class="fa fa-angle-up control-icon"></i>
        Project Co-ordinator
        </a>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
      <div class="panel-body career_info">
         <h5>Job ID: P1403 - Location: Chennai - Department: Management</h5>
         <p class="para_mor">The Project Co-ordinator role has direct responsibility for complete life cycle management and accountability of project initiatives within Acestra Network, ensuring compliance to project management policy, standards and procedures. The Project Co-ordinator must also have a genuine interest to make a valuable contribution to increase capability and maturity of a rapidly growing company. </p>
          <h5>Job Requirements</h5>
             <ul class="" style="list-style-type:disc">
          <li> Education: Qualified MA/BA with computing skills </li>
          <li> Project management experience a plus</li>
          <li> Fluent in English, oral and written</li>
          <li> Strong conceptual thinking and presentation skills </li>
          <li>  Ability to take direction and critique</li>
          <li> Ability to collaborate with a team and also work autonomously</li>
          <li> Proven problem-solving skills and adaptability to change <br><br>  <a href="/career/P1403/job_apply" class="btn btn-primary">Apply Now</a> </li>
        </ul>
      </div>
    </div>
  </div>  
    <div class="panel panel-default">
    <div class="panel-heading career_border" role="tab" id="heading4">
      <h4 class="panel-title">
        <a class="collapsed panel-hover" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="false" aria-controls="collapse4" >
        <i class="fa fa-angle-up control-icon"></i>
        Copy Writer
        </a>
      </h4>
    </div>
    <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
      <div class="panel-body career_info">
         <h5>Job ID: P1404 - Location: Chennai - Department: Marketing</h5>
         <p class="para_mor">Do you have the ability to write compelling creative copy? Do you know how to fashion ideas to achieve a communications strategy, incorporating marketing techniques into finely crafted prose that drives people to action? </p>
          <h5>Job Requirements</h5>
             <ul class="" style="list-style-type:disc">
          <li> Education: Bachelor's Degree (English or Journalism preferred) </li>
          <li>  Strong writing skills with 3+ years' web journalism or copywriting experience</li>
          <li>  Proficiency in Microsoft Word, Excel</li>
          <li>  Proven ability to meet deadlines, handle multiple jobs simultaneously, and re-prioritize at a moment's notice</li>
          <li>   Ability to work both independently and as part of a team</li>
          <li>  Fast, flexible, detail-oriented, and cooperative work style</li>
          <li> Project management experience a plus, but not required <br><br>  <a href="/career/P1404/job_apply" class="btn btn-primary">Apply Now</a> </li>
        </ul>
      </div>
    </div>
  </div>  
</div>  
</div>
    <div class="col-lg-4 col-md-4">
      <div class="how_apply">
        <h3>How to Apply?</h3>
        <hr class="para_bottom">
          <ul class="apply_list text-left career_info">
            <li><i class="fa fa-check"></i> You must Apply Online.</li>
            <li><i class="fa fa-check"></i> Cover letter, resume within five (5) pages.</li>
            <li><i class="fa fa-check"></i> Add Job ID in the subject line (if indicated).</li>
            <li><i class="fa fa-check"></i> Compensation based on experience.</li>
            <li><i class="fa fa-check"></i> Indicate your availability for the interview.</li>
          </ul>
      </div>
    </div>
</div>
</div>
</section>
@stop