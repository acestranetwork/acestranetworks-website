@extends('website_master')
@section('web_content')
<section class="sec1 about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">Services</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top">
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> Services</span></h3>
      </div>
    </div>
  </div>
</section>
 <!-- About us -->
<section class="sec1">
<div class="container">
    <h3 class="feature title1">Our Services</h3>
    <div class="row">
      <div class="rw-wrapper">
        <p class="para_mor">Acestra Network provides innovative solutions and top quality services for a great variety of business organizations. We can help you by developing and maintaining software solutions, upgrade technology and migrate services to cloud.</p>
      </div>
    </div>
  </div>
</section>
<!-- Choose us -->
<article class="tricons">
    <ul class="group">
        <li class="tricon t1">
            <div class="tricon--title">
                <div>
                 <i class="fa fa-pencil-square-o service_icon" aria-hidden="true"></i>
                </div>
            </div>
             <h3 class="text-center">Software Development</h3>
            <p class="para_mor_size text-center para_mor">Acestra Network develops high-performance software tailored to the needs of results oriented businesses. Among the  services we can provide are systems integration, System programming, System integration, Telecom &amp; EDP/MIS.</p>
        </li>
        <li class="tricon t2">
            <div class="tricon--title">
                <div>
                      <i class="fa fa-book service_icon" aria-hidden="true"></i>
                </div>
            </div>
             <h3 class="text-center">Custom Web Solution</h3>
            <p class="para_mor_size text-center para_mor">Our websites are created using a rare blend of design and development expertise resulting in effective web presence. We take pride in supporting clients with unique solutions delivered in a timely manner.</p>
        </li>
        <li class="tricon t3">
            <div class="tricon--title">
                <div>
                     <i class="fa fa-mobile service_icon" aria-hidden="true"></i>
                </div>
            </div>
             <h3 class="text-center">Mobile Application</h3>
            <p class="para_mor_size text-center para_mor">We design flexible and efficient mobile applications aligned to the specific goals of our clients. Our wide range of applications allow you to access information easily and conveniently giving you a great user friendly experience in a time efficient manner.</p>
        </li>
    </ul>
     <ul class="group">
        <li class="tricon t1">
            <div class="tricon--title">
                <div>
                 <i class="fa fa-cog service_icon" aria-hidden="true"></i>
                </div>
            </div>
             <h3 class="text-center">Data Consultancy</h3>
            <p class="para_mor_size text-center para_mor">Our data consultants specialize in working with clients to enhance both the collection and management of accurate data. With high quality and usable data you will gain maximum benefit through cost savings and efficient operations.</p>
        </li>
        <li class="tricon t2">
            <div class="tricon--title">
                <div>
                     <i class="fa fa-code service_icon" aria-hidden="true"></i>
                </div>
            </div>
             <h3 class="text-center">Technical Audit</h3>
            <p class="para_mor_size text-center para_mor">Our experience in software development allows us to assist clients in maintaining high quality software. Reduce risks and increase customer satisfaction using our technical audit.</p>
        </li>
        <li class="tricon t3">
            <div class="tricon--title">
                <div>
                     <i class="fa fa-globe service_icon" aria-hidden="true"></i>
                </div>
            </div>
             <h3 class="text-center">Digital Marketing</h3>
            <p class="para_mor_size text-center para_mor">WE will help you come up with a digital marketing strategy and in getting the right balance between ‘What you would like to do” and  “ what you can do”.</p>
    
        </li>
    </ul>
    <div class="dna-1 layer" data-depth="0.60"></div>
    <div class="dna-2 layer" data-depth="0.40"></div>
    <div class="dna-3 layer" data-depth="0.20"></div>
</article>  
   <hr class="para_bottom hr_line"> 
<section class="sec1 service_contact">
  <div class="container">
    <div class="row text-center">
      <div class="col-lg-12">
        <h4>Thank you for exploring Acestra Network service options. Please contact us with any questions or leave us your feedback. </h4>
        <a href="/contact" class="btn btn-lg btn-primary">Contact Us</a>
      </div>
    </div>
  </div>
</section>
<script src="/js/parallax.js"></script>
@stop