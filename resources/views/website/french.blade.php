@extends('website_master')
@section('web_content')
 <!--black nav--> 
<section class="about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">À PROPOS DE NOUS</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top"> 
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> About us</span></h3>
      </div>
    </div>
  </div>
</section>
 <!-- About us -->
<section class="sec1">
<div class="container">
    <h3 class="feature title1">À PROPOS DE NOUS</h3>
  </div>
</section>    
<!-- Hands with -->
<section class="sec1"> 
  <div class="container text-left text-justify">
    <div class="row ">
      <div class="col-lg-12">
        <img src="website/img/about.png" alt="about" class="img-responsive abt_img" style="margin-top: 4%;">
        <div class="rw-wrapper">
          <h3 class="rw-sentence hidden-xs">
            <span class="marginRightneg3px">Joignez les mains avec Acestra</span><div class="rw-words rw-words-1">
                <span>la progression</span>
                <span>avance</span>
                <span>réussir</span>
                </div>
          </h3>
          <h4 class="visible-xs text-center">Joignez-vous les mains avec Acestra pour réussir</h4>
        </div>
        <div class="row">
        <p class="para_mor">
          Depuis sa création, ACESTRA a évolué pour devenir une société de développement de logiciels compétents fournissant des solutions polyvalentes , évolutives , modulaires . Nous sommes passés d' un petit groupe de programmeurs de logiciels dédiés à une maison de développement de logiciels à grande échelle dans les deux Web et le développement d'applications mobiles.
        </p>
        <p class="para_mor">
         Nous sommes un groupe de professionnels passionnés , dédiés et de confiance dans les logiciels et le matériel d'ingénierie , les analystes , les programmeurs , les professionnels de commerce électronique, les professionnels de la conception de sites Web et les développeurs d'applications mobiles.
        </p>
        <p class="para_mor">
         Nous avons soutenu les entreprises à travers un large éventail d'industries , notamment la fabrication , les soins de santé , les services financiers , juridiques, gouvernement , immobilier, commerce de détail et les organisations sans but lucratif. Avec nos forces et l'expérience en gestion de projet , la conception créative , de la technologie et développement de logiciels de commerce électronique, ACESTRA RÉSEAUX aide ses clients à améliorer leurs performances , gagner recentrage sur les activités de pointe, des normes de qualité élevées et surtout atteindre une rentabilité accrue.
        </p> 
        </div>       
      </div>
    </div>
    <hr class="para_bottom">    
  </div>
</section>
<!-- Choose us -->
<section class="sec1 choose_bg">
  <div class="container">
    <div class="row text-left text-justify">
      <div class="col-lg-12">
        <h3 class="choose_us">Notre mission est de faire original, travail novateur qui est inspiré par nos clients " marque , les objectifs d'affaires et du budget et de la valeur pour l'argent.</h3>
      </div>
    </div>
  </div>
</section>
<section class="sec1">
<div class="container">
<div class="row">
  <div class="col-md-12 text-center">
   <img src="/website/img/team.jpg" class="img-responsive">
   <div class="col-sm-8 col-sm-offset-2">
     <h4 class="para_mor"> Notre objectif est d'exécuter vos projets en utilisant amélioré l'état des logiciels de pointe . notre équipe dévouée et bien équipée cherche à obtenir des résultats rapides et précis.  </h4>
   </div>  
  </div>
</div>
</div>
</section>
@section('script')
  <script type="text/javascript">
  $('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
  </script>
  @stop
@stop