@extends('website_master')
@section('web_content')
<section class="sec1 about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">Terms & Conditions</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top hidden-xs">
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> Terms & Conditions</span></h3>
      </div>
    </div>
  </div>
</section>
    <section class="terms_sec text-justify">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div>
               <h3 class="feature title1">Terms and Conditions</h3>
              <h5 class="define">Definitions: In this agreement the terms have the following meanings</h5>
              <ol class="para_mor" type="1">
                <li>"The Client" means any person, company, partnership, organisation or body who Acestra Networks Pvt ltd deals with and Acestra Networks Pvt ltd agrees to provide the products and/or services under the terms of the agreement.</li>
                <li>"The Agreement" means the contract between Acestra Networks Pvt ltd and the Client to which these conditions will apply.</li>
                <li>"The Order Form" means the digital or printed document completed by the client in order to enter the Agreement.</li>
                <li>"Order" is the request by the Client for products and/or services in the order form/agreement.</li>
                <li>"Deliverables" are the outputs of services or products to be supplied under the agreement and shall include but are not limited to, all software and written material, including programs, tapes, listings and other programming documentation.</li>
              </ol>
            </div>
            <div>
              <h4>Acceptance of Terms</h4>
              <p class=" para_mor">The following Terms and Conditions ("the Conditions") are the terms on which Acestra Networks Pvt ltd provides services and supersedes all other terms and conditions. Acestra Networks Pvt ltd shall provide services to the Client as described and specified in the proposal provided by Acestra Networks Pvt ltd.</p>
              <p class=" para_mor">The headings in these Conditions are inserted for convenience of reference only and are not intended to be part of, or to affect the meaning, or interpretation of any of the Conditions.</p>
              <p class=" para_mor">If any term or provision of these Conditions is held invalid, illegal or unenforceable for any reason by any Court of competent jurisdiction, such provision shall be severed, and the remainder of the provisions shall continue in full force and effect.</p>
              <p class=" para_mor">All prices specified in the Agreement will be honored for 3 months from the date of contract. Acceptance of the Agreement need to be signed in writing within the specified time in order for the Agreement to be valid.</p>
            </div>
            <div>
              <h4>Scope</h4>
              <p class=" para_mor">The Agreement shall apply to all services and/or products ordered by the Client from Acestra Networks Pvt ltd.</p>
            </div>
            <div>
              <h4>General Disclaimer</h4>
              <p class=" para_mor">Acestra Networks Pvt ltd disclaims all warranties, either express or implied, including the warranties or merchantability and fitness for a particular purpose. In no event shall Acestra Networks Pvt ltd be liable for any damages whatsoever including direct, indirect, incidental, consequential, loss of business profits or special damages, even if Acestra Networks Pvt ltd or third party agents have been advised of the possibility of such damages.</p>
            </div>
            <div>
              <h4>Liability</h4>
              <p class=" para_mor">Acestra Networks Pvt ltd, its Employees and/or Contractors will not be held liable from:- all and any liability for loss or damage caused by any inaccuracy; omission; delay or error as the result of negligence or other cause in the production of the service or product; Service interruptions; All and any liability for use of content provided by the Client; All and any liability for loss or damage to clients artwork/photos, supplied for the site. Immaterial whether the loss or damage results from negligence or otherwise.</p>
            </div>
            <div>
              <h4>Payment</h4>
              <p class=" para_mor">Acestra Networks Pvt ltd expects the Client to make the payments promptly based on the agreed schedule. Whilst any payment due under the Agreement remains outstanding, Acestra Networks Pvt ltd shall be entitled at its sole and absolute discretion to withhold provision of any services and/or products it would otherwise be obliged to provide under the Agreement. All payments by cheque, bankers draft or money order must be made in Canadian dollars unless agreed upon in writing by both parties. All credit card and debit card transactions will be processed in Canadian dollar.</p>
              <p class=" para_mor">Price quoted by Acestra Networks Pvt ltd can change and should be agreed at the time of contract. If the Client ceases to trade after work has commenced, any outstanding balance must still be paid.</p>
            </div>
            <div>
              <h4>Service Acceptance</h4>
              <p class=" para_mor">You shall keep secure any identification, password and other confidential information relating to your account. You shall notify Acestra Networks Pvt ltd immediately of any known or suspected unauthorized use of your account or breach of security, including loss, theft or unauthorized disclosure of your password or other security information.</p>
              <p class=" para_mor">On reasonable notice to the Client, Acestra Networks Pvt ltd shall take any and all steps it determines to be necessary to maintain the Services, which may include (without limitation) altering or suspending Services during maintenance. Acestra Networks Pvt ltd will try to ensure that such maintenance, alterations and suspensions to the Services occur outside of normal business hours.</p>
              <p class=" para_mor">In the case of an individual User, you warrant that you are at least 18 years of age and if the User is a company, you warrant that the Services will not be used by anyone under the age of 18 years.</p>
              <p class=" para_mor">Provided that the Customers perform their obligations under these Conditions, Acestra Networks Pvt ltd represents and warrants that the Services will be provided with reasonable skill and due care. Acestra Networks Pvt ltd does not warrant that the operation of the Services will be uninterrupted, error-free or secure. Acestra Networks Pvt ltd's sole liability for any breach of this warranty shall be to re-perform the affected Services. In the event that Acestra Networks Pvt ltd determines that re-performance is not commercially feasible, Acestra Networks Pvt ltd may terminate this Agreement and will receive payment for the work to date on a pro-rata basis. What is or is not commercially feasible shall be in the sole discretion of Acestra Networks Pvt ltd.</p>
            </div>
              <div>
              <h4>International Use</h4>
              <p class=" para_mor">Recognizing the global nature of the Internet, you agree to comply with all local rules regarding online conduct and acceptable Content. Specifically, you agree to comply with all laws regarding the transmission of technical data exported from the United States or the country in which you reside.</p>
            </div>
            <div>
              <h4>Indemnity</h4>
              <p class=" para_mor">The Client agrees to indemnify and hold Acestra Networks Pvt ltd and it employees and associated parties harmless from and against all liabilities, legal fees, damages, losses, costs and other expenses in relation to any cliams or actionS brought against Acestra Networks Pvt ltd arising out of breach by the Client of the Terms and Conditions specified in this web page.</p>
            </div>
              <div>
              <h4>Disclaimers</h4>
              <p class=" para_mor">Services and/or products are provided on an "AS IS" and "AS AVAILABLE" basis without any representation or endorsement made and without warranty of any kind whether expressed or implied.</p>
              <p class=" para_mor">Acestra Networks Pvt ltd is a professional and "family-friendly" business entity. We reserve the right to limit our service to only those individuals, businesses and non-profit organizations whose needs we can realistically meet. Any site may link to our site without permission as long as your site is clean, family-friendly and follows our general philosophy.</p>
            </div>
              <div>
              <h4>Termination Provisions</h4>
              <p class=" para_mor">Acestra Networks Pvt ltd reserves the right to suspend or terminate all or part of the Services with immediate effect, where there has been a breach by the Client of these terms and conditions or failure to pay outstanding charges. Either Party may terminate the Services with or without cause on giving 30 days written notice to the other. Termination of this agreement will be without prejudice to any accrued rights of either party.</p>
              
            </div>
              <div>
              <h4>Amendment</h4>
              <p class=" para_mor">Acestra Networks Pvt ltd reserves the right to amend any provision of Terms and Conditions stated in this document at any time without notice. It is the duty of the Client to check the Terms and Conditions page on the web.</p>
              
            </div>
          </div>
        </div>
  
        <hr>
        <h4>Last Updated: 01/06/2016</h4>
</div>
 </section>
@stop