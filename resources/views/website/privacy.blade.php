@extends('website_master')
@section('web_content')
<section class="sec1 about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">Privacy</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top">
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> Products</span></h3>
      </div>
    </div>
  </div>
</section>
    <section class="privacy_sec">
      <div class="container">
 <h3 class="feature title1">Privacy policy</h3>
        <div class="row">
          
          <div class="col-lg-12 pri_botom">
            <p class=" para_mor">Your privacy is important to us. In order to protect your privacy we provide this notice in explaining our online information practices and the choices you can make about the way your information is collected and used. To make this notice searchable we have made it available on all pages in the footer of the web pages.</p>
            <p class=" para_mor">We may collect information such as your name and contact details for the purpose of our business. Information collected on the website helps us respond to questions and will serve as an asset to improve our web site, ban misbehaving IP's and hacking attempts. Data thus collected is not shared with third parties except as required by law or to investigate abuse of this Web site.</p>
            <h4>Our Services</h4>
            <ol class="para_mor" type="1">
              <li>Acestra Networks Pvt ltd is a professional, family friendly business entity. We reserve the right to limit our service to only those individuals, businesses and non-profit organizations whose needs we can realistically meet.</li>
              <li>Software or web applications developed by Acestra Networks Pvt ltd cannot be modified without permission and should adhere to Acestra Networks Pvt ltd's philosophy.</li>
            </ol>
            <h4>Our Website</h4>
            <ol class=" para_mor" type="1">        
              <li>Web Site is designed for the purpose of marketing Acestra Networks Pvt ltd's services and is targeted at commercial and non-commercial organizations.</li>
              <li>Visitors of the Web Site shall not use the search engines, links, or free graphics for: Illegal purposes, Adult-material purposes, Or any other purpose that does not follow our philosophy</li>
              <li>Constructive comments about the site and the site content are appreciated and should be e-mailed to info[@]acestranetwork.com.</li>
              <li>External sites may link to our site without permission as long as your site is clean, family-friendly and follows our ethical philosophy.</li>
            </ol>
            <h4>Alert Us</h4>
            <p class=" para_mor">If you suspect that our Web Site is being used inappropriately by anyone, or is conveying inappropriate information, or that it contains links, graphics, or text that were reproduced inappropriately, please e-mail contact[@]acestranetwork.com immediately. </p>
            <p class=" para_mor">Thank you.</p>
          </div>
        </div>
      </div>
 </section>
 <section class="privacy_bottom">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h4 class="privacy_font"><b>ACESTRA</b> provides tailor-made solutions that fits your business needs!  <a href="/quote" class="btn btn-lg btn-primary">Request Quote</a></h4>
        
      </div>
    </div>
  </div>
</section>
@stop