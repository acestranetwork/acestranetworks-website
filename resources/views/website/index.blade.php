@extends('website_master')
@section('web_content')
<section>  
    <ul class="anim-slider">
      <li class="anim-slide slider1_img" style='background-image: url("website/img/slider1.jpg")'>
        <h2 id="plugin" class="slider1_content"><img src="/website/img/border.png" class="side_img"> Your search for a quality software solutions provider for all your back office needs ends here. <img src="/website/img/border.png"  class="side_img1"> </h2>
        <h1 id="slider" class="slider1_content"> "Evolve your business with the help of Custom software business solutions at ACESTRA"</h1>
        <h3 id="license" class="slider1_content global_soft">Global Software services</h3>
      </li>
      <!-- Slide No2 -->
      <li class="anim-slide slider2_img" style='background-image: url("website/img/slider2.jpg")'>
       <h1 id="slider" class="slider2_content slider3_1"> OUR SPECIALITIES are system integration and software development. </h1>
       <h2 id="plugin" class="slider1_content slider3_2 second_slider_2nd_head">"<i>ULTRA SHARP HIGH RESOLUTION GRAPHICS</i>"</h2>
       <h3 id="bounce" class="second_slider_3rd_head">A software solutions provider extraordinaire</h3>
     </li>
     <!-- Slide No3 -->
     <li class="anim-slide slider3_img" style='background-image: url("website/img/slider3.jpg")'> 
      <h1 id="slider" class="slider1_content slider3_1">Designing Developing and Deployment!</h1>
      <h2 id="plugin" class="slider1_content slider3_2 deliv_digi">"<i>Delivering Digital Transformation</i>"</h2>
      <h3 id="license" class="slider1_content animation_text3"> We Promise to deliver unmatched results that turn your dreams to reality </h3>
      <h4 id="version" class="slider1_content animation_text3">High end site testing</h4>
    </li>
    <!-- Arrows -->
    <nav class="anim-arrows">
      <span class="anim-arrows-prev">
        <i class="fa fa-angle-left fa-3x"></i>
      </span> 
      <span class="anim-arrows-next"> 
        <i class="fa fa-angle-right fa-3x"></i> 
      </span> 
    </nav>
  </ul>
</section>
<!-- Who we section -->
<section class="sec1 slider_bg">
  <div class="container">
    <h3 class="feature title1">Who We Are</h3>
    <div class="row">
      <div class="rw-wrapper">
       <h2 class="Ace_head_title"><p class="quote">Acestra Network provides <span id="box1words"></span> software and mobile solutions.</p></h2>
        <p class="para_mor">We are leading Web Design & Website Application Development company having offices in Chennai, India and Toronto,Canada. We also develop Mobile Applications, e­Commerce Websites, Social Media Websites, CM based Websites, Design Logo's and Design Brochures and provide Domian & Hosting Solutions. Our services and solutions result in improved quality and increased efficiency, coupled with data analytics that provide vital insight for you to enhance your business and support effective decision making. We use our services, products and expertise to find a perfect fit for your business needs and transform our promise into reality. We Deliver solutions with a platform of your choice that is built to your needs for enhanced user experience with custom features and functionality that drives conversions </p>
      </div>
    </div>
  </div>
</section>
<!-- Section feature -->
<section class="sec1">
  <div class="container">
    <h3 class="feature title1">Features</h3>
    <div class="row feature_padding">
      <div class="col-md-4 col-sm-6 col-sm-6 redu_resp about-grids para_imp_resp">
        <div class="hi-icon-wrap hi-icon-effect-8">
          <i class="fa fa-users icon-large effect-1 hi-icon hi-icon-archive"></i>
         </div> 
          <h4>Quality and Adaptability</h4>
          <p class="para_mor featute_para_hight para_imp_resp1">Our custom solutions ensure the highest level of accuracy, quality,adaptability and the manuverability to innovate and adds business value.</p><!-- <img src="website/img/shadow.png" class="shd_img"> -->
      </div>
      <div class="col-md-4 col-sm-6 ad_gs1 redu_resp about-grids para_imp_resp">
       <div class="hi-icon-wrap hi-icon-effect-8">
        <i class="fa fa-user icon-large hi-icon hi-icon-archive"></i>
       </div> 
        <h4>Reduce Development costs</h4>
        <p class="para_mor featute_para_hight para_imp_resp1">Automated solutions for all day to day activities to speed up your operations. Gain a complete control over your developing process without having to compromise an quality at competitive prices. </p><!-- <img src="website/img/shadow.png" class="shd_img shd-img1"> -->
      </div>  
      <div class="col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-0 about-grids para_imp_resp">
       <div class="hi-icon-wrap hi-icon-effect-8">
        <i class="fa fa-eye icon-large hi-icon hi-icon-archive"></i>
       </div> 
        <h4>Improved responsiveness</h4>
        <p class="para_mor featute_para_hight para_imp_resp1">Improve your overall operations with streamlined business methods for better visibility and impressive results. Customized application software that paves way for true business transformation and happy customers.</p><!-- <img src="website/img/shadow.png" class="shd_img"> -->
      </div>
    </div>   
    <div class="row text-center">        
      <div class="col-md-4 col-sm-6 about-grids para_imp_resp">
       <div class="hi-icon-wrap hi-icon-effect-8">
        <i class="fa fa-search icon-large hi-icon hi-icon-archive"></i>
       </div> 
        <h4>On time, every time.</h4>
        <p class="para_mor featute_para_hight para_imp_resp1">Out­of­box custom software solutions with additional security features to stay safe and achieve desired results within the specified time frame.</p><!-- <img src="website/img/shadow.png" class="shd_img"> -->
      </div>
      <div class="col-md-4 col-sm-6 about-grids para_high_tech">
       <div class="hi-icon-wrap hi-icon-effect-8">
        <i class="fa fa-flag-o icon-large hi-icon hi-icon-archive"></i>
       </div> 
        <h4>Highly trained technical staff</h4>
        <p class="para_mor featute_para_hight para_high_tech1">At Acestra, our people are passionate, highly trained and experienced and are proudly aware the impact it can have on people and organizations.Yes ,it is one thing to love technology, and what makes us unique is that we focus on getting to know you and your business first. Once we understand you and your business we can then get to work on finding a technology solution that best suits your needs.  </p><!-- <img src="website/img/shadow.png" class="shd_img"> -->
      </div>
      <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-0 about-grids para_tech_sup">
       <div class="hi-icon-wrap hi-icon-effect-8">
        <i class="fa fa-adjust icon-large hi-icon hi-icon-archive"></i>
       </div> 
        <h4 style="font-size:17px;">Application maintenance! 24/7 Tech support</h4>
        <p class="para_mor featute_para_hight para_tech_sup1">24/7 support of technical resource are available to immediately respond to your issues however trivial, 24 hours a day, 7 days a week. we, at Acestra, spend a considerable amount of time, money and resources on on proactively monitoring our client's networks and working with them to improve performance that will prevent failure fromhappening. Routine system performance checks, security updates, availability test or done to ensure all your data, software and all other technicalities are not compromised or corrupt. But technology, as we know just "seems to break" at the worst time. Our support system philosophy is that we focus on how we respond in these difficult times and learn from them to limit the same problems in future. </p><!-- <img src="website/img/shadow.png" class="shd_img"> -->
      </div>
    </div>   
  </div>
</section>
<section class="sec1 sec_bg fullscreen background parallax  text-center" data-diff="300">
  <div class="container"> 
    <div class="row">
      <h2 class="provide_info">We provide the latest technologies to help you succeed in your business.</h2>
      <h4 class="text-center" style="color:black"><strong>Talk to our experts on the technologies that we use.</strong></h4>
      <a href="/quote" target="_self" class="btn btn-lg btn-primary appear-animation bounceIn appear-animation-visible" data-appear-animation="bounceIn">Request Quote!</a>
    </div>
  </div>
</section>
<section class="sec1  slider_bg">
  <div class="container text-left"> 
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="progress_head"><span>Our Specialization</span></h3>
        <ul class="progress_list skills">
          <li class='progressbar2' data-width='90' data-target='95'>Web Development</li>
          <li class='progressbarPhp' data-width='80' data-target='90'>Software Development</li>
          <li class='progressbarGit' data-width='90' data-target='95'>Mobile Application Development</li>
          <li class='progressbar3' data-width='80' data-target='80'>UI Design</li>
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="progress_head"><span>First time to Acestra </span></h3>
        <div class="panel-group" id="accordion">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"  class="panel-hover">
                  <i class="fa fa-angle-up control-icon"></i>
                  <i class="fa fa-phone-square"></i>&nbsp;
                  Contact us for Queries?
                </a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body">
              <a href="/contact"> Phone or email us</a> now
             </div>
           </div>
         </div>
         <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
             <a class="collapsed panel-hover" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
              <i class="fa fa-angle-up control-icon"></i>
              <i class="fa fa-envelope"></i>&nbsp;
              Enquire for a Quote
            </a>
          </h4>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
          <div class="panel-body">
            <a href="/quote"> Get a quote</a> from the best technology integrators and solution providers. 
          </div>
        </div>
      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title">
            <a class="collapsed panel-hover" data-toggle="collapse" data-parent="#accordion" href="#collapse3" >
              <i class="fa fa-angle-up control-icon"></i>
              <i class="fa fa-envelope"></i>&nbsp;
              Add to CV bank
            </a>
          </h4>
        </div>
        <div id="collapse3" class="panel-collapse collapse">
          <div class="panel-body">
           We add consultants to our CV bank and your CV will receive first preference when need arise.
         </div>
       </div>
     </div>  
   </div>  
 </div>
</div>
</div>
</section>
<section class="sec1 slider_bg_companies">
  <div class="container">
   <ul class="bxslider" style="height:48px;">
    <li style="width:48px;"><img data-u="image" src="website/img/dynamic.png" class="img-responsive" style="" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/html.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/micro.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/python.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/ios.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/jquery.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/php.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/rails.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/share.png" class="img-responsive" /></li>
    <li style="width:48px;"><img data-u="image" src="website/img/spring.png" class="img-responsive" /></li>
    <li><img data-u="image" src="website/img/sql.png" class="img-responsive" /></li>
  </ul>
</div>
</section> 
@section('script')
<script type="text/javascript" src="website/js/jssor.js"></script>
<script type="text/javascript" src="website/js/wordrotator.js"></script>
<script type="text/javascript" src="website/js/parallaxbg.js"></script>
<script type="text/javascript">
  $('.carousel[data-type="multi"] .item').each(function(){
    var next = $(this).next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));
    for (var i=0;i<2;i++) {
      next=next.next();
      if (!next.length) {
        next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));
    }
  });
</script>
<script type="text/javascript">
  $('.bxslider').bxSlider({
    minSlides: 5,
    maxSlides: 5,
    slideWidth: 170,
    slideMargin: 10,
    ticker: true,
    speed: 12000
  });
</script>
<script>
 $(".anim-slider").animateSlider(
 {
  autoplay  :true,
  interval  :8000,
  animations  : 
  {
          0 :   //Slide No1
          {
            h1  : 
            {
              show      : "bounceIn",
              hide    : "fadeOutLeftBig",
              delayShow : "delay2s"
            },
            h2:
            {
              show    : "fadeInUpBig",
              hide    : "fadeOutDownBig",
              delayShow : "delay1-5s"
            },
            h3  :
            {
              show      : "bounceInRight",
              hide    : "fadeOutRightBig",
              delayShow : "delay1-5s"
            },
            // h4:
            // {
            //   show    : "bounceInUp",
            //   hide    : "fadeOutLeftBig",
            //   delayShow : "delay2s"
            // } 
          },
          1 : //Slide No2
          { 
            h1  : 
            {
              show      : "bounceIn",
              hide    : "flipOutX",
              delayShow : "delay1s"
            },
            h2:
            {
              show    : "fadeInUpBig",
              hide    : "fadeOutDownBig",
              delayShow : "delay1-5s"
            },            
            "#bounce"   :
            {
              show    : "bounceIn",
              hide    : "bounceOut",
              delayShow   : "delay2s"
            }
          },
          2 : //Slide No3
          {
            h1  : 
            {
              show      : "bounceIn",
              hide    : "flipOutX",
              delayShow : "delay1s"
            },
            h2:
            {
              show    : "fadeInUpBig",
              hide    : "fadeOutDownBig",
              delayShow : "delay1-5s"
            },
            h3  :
            {
              show      : "bounceInRight",
              hide    : "fadeOutRightBig",
              delayShow : "delay1-5s"
            },
            h4:
            {
              show    : "bounceInUp",
              hide    : "fadeOutLeftBig",
              delayShow : "delay2s"
            } 
          }
        }
      });
</script>
<script type="text/javascript">
      $(function () {
        $("#box1words").wordsrotator({
          words: ['Scalable','Modular','Innovative','Versatile']
        });
            });
</script>
@stop
@stop