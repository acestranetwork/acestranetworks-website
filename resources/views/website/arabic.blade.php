@extends('website_master')
@section('web_content')
 <!--black nav--> 
<section class="about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">معلومات عنا</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top"> 
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> About us</span></h3>
      </div>
    </div>
  </div>
</section>
 <!-- About us -->
<section class="sec1">
<div class="container">
    <h3 class="feature title1">معلومات عنا</h3>
  </div>
</section>    
<!-- Hands with -->
<section class="sec1"> 
  <div class="container text-left text-justify">
    <div class="row ">
      <div class="col-lg-12">
        <img src="website/img/about.png" alt="about" class="img-responsive abt_img">
        <div class="rw-wrapper">
          <h2 class="rw-sentence hidden-xs">
            <span class="marginRightneg3px">يدا بيد مع Acestra ل &nbsp;</span><div class="rw-words rw-words-1">
                <span>تقدم</span>
                <span>تقدم</span>
                <span>نجح</span>
                </div>
          </h2>
          <h4 class="visible-xs text-center">يدا بيد مع Acestra للنجاح</h4>
        </div>
        <p class="para_mor">
          منذ تأسيسها، وقد تطورت ACESTRA في شركة لتطوير البرمجيات المختصة التي تقدم تنوعا ، قابلة لل حلول وحدات. لقد نمت من مجموعة صغيرة من المبرمجين برامج مخصصة ل منزل تطوير البرمجيات واسعة النطاق في كل من الإنترنت، وتطوير التطبيقات النقالة .
        </p>
        <p class="para_mor">
         نحن مجموعة من المهنيين عاطفي والتفاني و موثوق به في برنامج و الأجهزة الهندسية و المحللين والمبرمجين و المتخصصين في التجارة الإلكترونية ، والمهنيين تصميم مواقع الإنترنت و مطوري تطبيقات الهاتف المتحرك .
        </p>
        <p class="para_mor">
         قمنا بدعم الشركات عبر مجموعة واسعة من الصناعات التي تشمل التصنيع والرعاية الصحية ، والخدمات المالية والقانونية و الحكومة ، والعقارات ، وتجارة التجزئة والمنظمات غير الهادفة للربح . مع القوة و الخبرة في إدارة المشاريع ، والتصميم الإبداعي ، والتكنولوجيا، وتطوير البرمجيات و التجارة الإلكترونية لدينا، ACESTRA شبكات بمساعدة عملائها على تحسين الأداء و اكتساب تركيز الأعمال المتقدمة، و أعلى معايير الجودة والأهم من تحقيق زيادة الربحية .
        </p>        
      </div>
    </div>
    <hr class="para_bottom">    
  </div>
</section>
<!-- Choose us -->
<section class="sec1 choose_bg">
  <div class="container">
    <div class="row text-left text-justify">
      <div class="col-lg-12">
        <h3 class="choose_us">مهمتنا هي أن تفعل الأصلي ، عمل مبتكر مستوحى من قبل عملائنا "العلامة التجارية ، وأهداف العمل و الميزانية و تقديم قيمة مقابل المال .</h3>
      </div>
    </div>
  </div>
</section>
<section class="sec1">
<div class="container">
<div class="row">
  <div class="col-md-12 text-center">
   <img src="/website/img/team.jpg" class="img-responsive">
   <div class="col-sm-8 col-sm-offset-2">
     <h4 class="para_mor"> هدفنا هو تنفيذ المشاريع الخاصة بك باستخدام تعزيز دولة من البرامج الفنية . لدينا فريق عمل متخصص و مجهز تجهيزا جيدا ويسعى لتحقيق نتائج سريعة ودقيقة .  </h4>
   </div>  
  </div>
</div>
</div>
</section>
@section('script')
  <script type="text/javascript">
  $('.carousel[data-type="multi"] .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
      next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});
  </script>
  @stop
@stop