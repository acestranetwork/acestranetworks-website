@extends('website_master')
@section('web_content')
<section class="sec1 about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">Blog</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top">
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> Blog</span></h3>
      </div>
    </div>
  </div>
</section>
<!--Blog-->
<section class="sec1">
  <div class="container">
    <div class="row">
      <div class="col-lg-1 col-md-1 col-sm-2 col-xs-4">
        <div class="date-wrap">
          <span class="date">
            11
          </span>
          <span class="month">
            Jan
          </span>
        </div>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-7 col-sm-12 text-left">
        <img src="website/img/blog.png" class="img-responsive" style="width:100%">
        <h3>Google buys JetPac</h3>
        <p class="product_para justify para_mor">Google who has come up with some interesting artificial intelligence deals in recent days, is now reported to buy JetPac. JetPac is a app which was available in the App Store is used to show the nearby places searched according to the pictures which are already uploaded to other applications or social networking websites by different users. JetPac serves as a City Guide for its users.</p>
        <p class="product_para justify para_mor">JetPac team has been in serious learning in the field of artificial intelligence and image processing. Various researches were made to guide the users with the images uploaded in the social networking websites. Various knowledge training were involved by training the systems using the technology artificial neural networks on lots of information derived from various images, audio and videos and then presenting the information to the system and then receiving interferences about it in response. JetPac in their app came out earlier with an attempt for the users to explore the pictures uploaded by their Facebook friend list and displaying the results to the users to decide the best place to travel. On recent developments the app has developed a method to spot smiles and decide how happy the people are in the city.</p>    
        <p class="product_para justify para_mor">  JetPac in their website www.jetpac.com have already announced about the deal with Google and confirmed that the app will not be available for App Store from September 15.</p>        
        <h3>Google buys JetPac</h3>
        <ul class="blog_list_info" style="list-style-type:1">
          <li>Google’s Latest interests in the “Robotic Sector” the so called future</li>
          <li>Android Security-Google comes up with an app scanner</li>
          <li>Amazon to increase the usage of robots in warehouses</li>
          <li>Adobe releases Flash 14.0.0.145 to safeguard security</li>
          <li>Amazon’s Zocalo a new add to Cloud Sharing and Storage Industry</li>
        </ul>               
      </div>
      <div class="col-lg-3 col-md-3 col-sm-2 col-xs-8 text-left">
        <h4 class="category">Categories</h4>   
        <div class="blog_list">
          <a href="" class="btn btn-default"> Internet Business </a><span class="badge hidden-md hidden-sm">04</span>
     <a href="" class="btn btn-default"> Web Development</a><span class="badge hidden-md hidden-sm">03</span>
    <a href="" class="btn btn-default"> Software Management</a><span class="badge hidden-md hidden-sm">05</span>
        <a href="" class="btn btn-default"> Web Security</a><span class="badge hidden-md hidden-sm">10</span>
     <a href="" class="btn btn-default"> Internet News</a><span class="badge hidden-md hidden-sm">01</span>
      </div>
      <div class="row">
      <div class="col-lg-12  col-md-8 col-sm-12 col-xs-8">
      <h4 class="category">Categories</h4>  </div>
      <div class="col-lg-6  col-md-6 col-sm-12 col-xs-6"><a href="website/img/web.jpg" data-lightbox="image-1"><img src="website/img/web.jpg" class="blog_image"></a></div>
      <div class="col-lg-6  col-md-6 col-sm-12 col-xs-6"><a href="website/img/web.jpg" data-lightbox="image-2"><img src="website/img/web.jpg" class="blog_image"></a></div>
     <div class="col-lg-6  col-md-6 col-sm-12 col-xs-6"> <a href="website/img/web.jpg" data-lightbox="image-3"><img src="website/img/web.jpg" class="blog_image"></a></div>
      <div class="col-lg-6  col-md-6 col-sm-12 col-xs-6"><a href="website/img/web.jpg" data-lightbox="image-4"><img src="website/img/web.jpg" class="blog_image"></a></div>
      <div class="col-lg-6  col-md-6 col-sm-12 col-xs-6"><a href="website/img/web.jpg" data-lightbox="image-5"><img src="website/img/web.jpg" class="blog_image"></a></div>
     <div class="col-lg-6  col-md-6 col-sm-12 col-xs-6"> <a href="website/img/web.jpg" data-lightbox="image-6"><img src="website/img/web.jpg" class="blog_image"></a></div>
      </div>
      </div>
      </div>
       <hr class="para_bottom"> 
    <div class="row carousel_top">
      <div class="col-lg-1  col-md-1 col-sm-2 col-xs-4">
        <div class="date-wrap">
          <span class="date">
            11
          </span>
          <span class="month">
            Jan
          </span>
        </div>
      </div>
      <div class="col-lg-8 col-md-8 col-sm-7 col-xs-12 text-left">
        <img src="website/img/blog.png" class="img-responsive" style="width:100%">
        <h3>Google buys JetPac</h3>
        <p class="product_para justify para_mor">Google who has come up with some interesting artificial intelligence deals in recent days, is now reported to buy JetPac. JetPac is a app which was available in the App Store is used to show the nearby places searched according to the pictures which are already uploaded to other applications or social networking websites by different users. JetPac serves as a City Guide for its users.</p>
        <p class="product_para justify para_mor">JetPac team has been in serious learning in the field of artificial intelligence and image processing. Various researches were made to guide the users with the images uploaded in the social networking websites. Various knowledge training were involved by training the systems using the technology artificial neural networks on lots of information derived from various images, audio and videos and then presenting the information to the system and then receiving interferences about it in response. JetPac in their app came out earlier with an attempt for the users to explore the pictures uploaded by their Facebook friend list and displaying the results to the users to decide the best place to travel. On recent developments the app has developed a method to spot smiles and decide how happy the people are in the city.</p>    
        <p class="product_para justify para_mor">  JetPac in their website www.jetpac.com have already announced about the deal with Google and confirmed that the app will not be available for App Store from September 15.</p>        
        <h3>Google buys JetPac</h3>
        <ul class="blog_list_info" style="list-style-type:1">
          <li>Google’s Latest interests in the “Robotic Sector” the so called future</li>
          <li>Android Security-Google comes up with an app scanner</li>
          <li>Amazon to increase the usage of robots in warehouses</li>
          <li>Adobe releases Flash 14.0.0.145 to safeguard security</li>
          <li>Amazon’s Zocalo a new add to Cloud Sharing and Storage Industry</li>
        </ul>               
      </div>
      <div class="col-lg-3 col-md-3 col-sm-2">
        
      </div>
    </div>    
  </div>
</section>
<script type="text/javascript">
  lightbox.option({
      'resizeDuration': 200,
      'wrapAround': true
    })
</script>
@stop