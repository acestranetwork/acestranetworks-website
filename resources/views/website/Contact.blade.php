@extends('website_master')
@section('web_content')
<!--black nav--> 
<section class="sec1 about_us_bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<h3 class="text-left abt_linkcolor head_align">Contact Us</h3>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 abt_top">
				<h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor">Contact Us</span></h3>
			</div>
		</div>
	</div>
</section>
<div class="container-fluid map_sec maps">
	<div class="maps_iframe">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15547.615403558188!2d80.26492759754817!3d13.04179177774372!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5267d24266d18d%3A0xc10b9cc2b7c1e5db!2sAcestra+Network!5e0!3m2!1sen!2sin!4v1461911558016" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>
<!--Quote-->
<section class="sec1 sec_form">
	<div class="container text-left">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6">
				<form action="contact_post" method="post" onsubmit="return validateContact()"> 
					<input type="hidden" name="_token" value="{{ csrf_token() }}">   
					@if ($errors->has())
					<div class="col-sm-12 ">
						<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Error! &#128546;</strong>
							@foreach ($errors->all() as $error)
							<span class="err_line">
								
								<span class="sr-only">Errors</span>
								<p class="error_message">{{ $error }} <span class="glyphicon glyphicon-exclamation-sign" style="padding-left:10px;" aria-hidden="true"></span></p> 
							</span>      
							@endforeach
						</div>
					</div>
					@endif  
					@if ($message = Session::get('success'))
					<div class="col-sm-12 ">
						<div class="alert alert-success alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>Success  &#128522; </h4>
							{!! $message !!}
						</div>
					</div>
					@endif 
					@if ($error = Session::get('alert'))
					<div class="col-sm-12 ">
						<div class="alert alert-danger alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<h4>Error  &#128546; </h4>
							{!! $error !!}
						</div>
					</div>
					@endif   
					<div class="col-sm-12 highlight" style="display:none">
            <div class="alert alert-danger alert-block">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Error  &#128546; </h4>
             <span class="highlightmsg"></span>
            </div>
          </div>      					  
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-6">
							<label>Your Name:</label>
							<input type="text" placeholder="Your name" name="name" class="textbox_align form-control validate" id="name" value="{{ old('name') }}">
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<label>Your Email:</label>
							<input type="text" placeholder="Your Email" name="email" class="textbox_align form-control validate" id="email" value="{{ old('email') }}">
						</div>          
					</div>
					<label>Subject:</label>
					<input type="text" placeholder="Enter message subject" name="subject" class="textbox_align form-control validate" id="subject" value="{{ old('subject') }}">                    
					<label class="file_browse">Message: </label> <br>
					<textarea name="message" class="textbox_align form-control validate" id="message">{{ old('message') }}</textarea>
					<div class="row">
						<!-- captacha -->
						<div class="col-lg-12 col-md-12 col-sm-12">
							<div class="form-group" >
								<label for="captcha">Security code</label>
								<input name="captcha" type="text" class="form-control validate" placeholder="captcha" autocomplete="off" id="captcha">
							</div>	
							<div class="row">
								<div class="col-xs-2">
									<center><div class="form-group" id="captchadiv">
										<img src="{{ url('captcha') }}" id="captchaimg" class="img-rounded" alt="captcha_img" />
									</div></center>
								</div>
								<div class="col-xs-10 refresh_div text-left">
									<a href="#" class="refreshCaptcha text-success bg-success">
										<i class="fa fa-refresh"></i></a>
									</div>
								</div>
								<!-- captacha -->
								<div class="col-lg-6 col-md-6 col-sm-12 captcha_block">
									<button type="submit" class="btn btn-primary btn-md">Send Message</button>
									<button type="reset" class="btn btn-warning">Reset</button>
								</div>
							</div>           
						</div> 
					</form>  
				</div>
				<div class="col-lg-6 col-md-6 col-sm-6">
					<h4 class="get_in">Get in <b>touch</b></h4>
					<p class="justify getin_para">If you want to talk to us about your project, we would like to hear from you. Please get in touch with us by phone or e-mail. </p>
					<h4 class="get_in">Chennai<b> India</b></h4>
					<ul class="address_list">
						<li><i class="fa fa-map-marker"></i><b> Address: </b>2/2 Venkatesa Agraharam Street, Mylapore, Chennai-600004,India</li>
						<li><i class="fa fa-phone"></i><b> Phone : </b> +91 44 24629069 </li>
						<li><i class="fa fa-print"></i> <b>Fax : </b>+91 44 45010069</li>
						<li><i class="fa fa-envelope"></i><b> Email :</b> support@acestranetwork.com</li>
					</ul>        
				</div>
			</div>
		</div>
	</section>
	<script>
		lightbox.option({
			'resizeDuration': 200,
			'wrapAround': true
		})
	</script>
	@stop