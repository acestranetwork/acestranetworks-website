@extends('website_master')
@section('web_content')
<section class="sec1 about_us_bg">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6">
        <h3 class="text-left abt_linkcolor head_align">Products</h3>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 abt_top">
        <h3 class="text-right about_link"><a href="/index" class="abt_a">Home </a> <span>/</span> <span class="abt_linkcolor"> Products</span></h3>
      </div>
    </div>
  </div>
</section>
 <!-- About us -->
<section class="sec1">
<div class="container">
  <div class="feature_padding">
    <h3 class="feature title1">Our Products</h3>
    <div class="row">
      <div class="col-sm-12"> <h3 class="marginRightneg6px">We know the business secret for your success</h3></div>
    </div>
     <p class="visible-xs">The Business secret for your success</p>
  </div>  
 </div>
 </section>   
 <section class="products_sec">
 <div class="container">
   <div class="row text-left">
     <div class="col-lg-6 col-md-6 col-sm-12 left_align">
        <h3 class="ace_head">About<span class="ace_color"> <b class="ace_c">aceClassRoom</b></span></h3>
        <p class="product_para justify para_mor para_product"> aceClassRoom is a highly interactive web based education management system that provides a complete market leading solution for modern day school administration and management. We recognize the need for a solution that bridges the gap between the School, Parents and Students. It combines the evolution of software and the internet for the purpose of improving education by fostering an environment for any time access, monitoring, communication and secure data exchange.</p>
        <hr class="para_bottom">
        <h3 class="ace_head"><span>Why</span> aceClassRoom</h3>
        <ul class="service_list" style="list-style-type:disc">
          <li>Easy to use, simple to navigate</li>
          <li>Intuitive dashboard</li>
          <li>Remote monitoring, assistance</li>
          <li>  Secure and reliable</li>
          <li>  Multi-user system</li>
          <li>Regular backup</li>
          <li>24 * 7 availability</li>
          <li>Hundreds of Reports</li>
        </ul>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="hovereffect1">
           <img src="website/img/school 2.png" class="img-responsive service_image ace_img">
            <div class="overlay1 over_ace">
                <h2>Ace Classroom</h2>
                <p> 
                  <a class="info" href="http://classroom.chentoro.com/" target="_blank">Click here</a>
                </p> 
            </div>
        </div>           
      </div>
          
   </div> 
  <hr class="style17"> 
</div>   
</section>   
<section class="products_sec">
<div class="container">
   <div class="row text-left">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="hovereffect1">
           <img src="website/img/kollywood3.png" class="img-responsive ser_img kolly_img">
            <div class="overlay1 over_kolly">
                <h2>Kollywood talkies</h2>
                <p> 
                  <a class="info" href="http://www.kollywoodtalkies.com/" target="_blank">Click here</a>
                </p> 
            </div>
        </div> 
      </div>         
     <div class="col-lg-6 col-md-6 col-sm-12">
        <h3 class="ace_head">About<span class="ace_color"><b class="kolly_color"> Kollywood talkies</b></span></h3>
        <p class="product_para justify para_mor para_product"> Kollywood Talkies is an 24 x 7 cine bites entertainment web portal which gives the attention and interest of an audience or gives pleasure and delight. It can be an idea or a task, but is more likely to be one of the activities or events that can be viewed on day to day basis over thousands and millions of people across the country and for the purpose of keeping an audience's attention.</p>
        <hr class="para_bottom">
        <h3 class="ace_head"><span>Why</span> Kollywood talkies</h3>
        <ul class="service_list" style="list-style-type:disc">
          <li>Entertainment 24x7</li>
          <li>Cine Media Marketing & Promotions</li>
          <li>Digital Engagement </li>
          <li>Advertisement & Media Analytics </li>
          <li>Cine PR</li>
          <li>Hundreds of Reports</li>
        </ul>
      </div>
   </div> 
   <hr class="style17"> 
</div> 
</section> 
<section class="products_sec">
<div class="container">
   <div class="row text-left">
     <div class="col-lg-6 col-md-6 col-sm-12">
        <h3 class="ace_head">About<span class="ace_color"><b class="ace_realty_clr"> Acerealty </b></span></h3>
        <p class="product_para justify para_mor para_product"> AcestraRealty is a powerful tool to communicate information about properties and to promote real estate business across the Country. Through Acestra Realty web portal, landlords, builders can post their property to buy and sell to enable builders to build/construct to develop layout projects according to the customers and owners satisfaction.</p>
        <hr class="para_bottom">
        <h3 class="ace_head"><span>Why</span> Acerealty</h3>
        <ul class="service_list" style="list-style-type:disc">
          <li>Social Media Engagement</li>
          <li>Landlord Database</li>
          <li>Call for Joint Ventures</li>
          <li>Contact management</li>
          <li>Sponsored Listings</li>
          <li>Map Enabled Search</li>
          <li>Owner Portal</li>
          <li>Portfolio Management</li>
          <li>Billing & Invoicing</li>
          <li>Residential & Commercial</li>
        </ul>
      </div>     
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="hovereffect1">
           <img src="website/img/acestra realty 1.png" class="img-responsive service_image ace_img">
            <div class="overlay1 over_ace">
                <h2>Acerealty</h2>
                <p> 
                  <a class="info" href="http://www.acestrarealty.com/" target="_blank">Click here</a>
                </p> 
            </div>
        </div>     
      </div>       
   </div>  
<hr class="style17"> 
</div>   
</section>    
<section class="products_sec">
<div class="container">
   <div class="row text-left">
      <div class="col-lg-6 col-md-6 col-sm-12">
        <div class="hovereffect1">
           <img src="website/img/kalyanseva1.jpg" class="img-responsive ser_img kolly_img">
            <div class="overlay1 over_kolly">
                <h2>Kalyanseva</h2>
                <p> 
                  <a class="info" href="http://www.kalyanseva.com/" target="_blank">Click here</a>
                </p> 
            </div>
        </div> 
      </div>   
     <div class="col-lg-6 col-md-6 col-sm-12">
        <h3 class="ace_head">About <span class="ace_color"> <b class="kalyan_clr">Kalyanseva</b></span>  </h3>
        <p class="product_para justify para_mor para_product"> If you are looking to marry off your child or if you are ready to get married log on to <a href="http://www.kalyanseva.com/" target="_blank">kalyanseva.com</a> </p>
        <p class="product_para justify para_mor para_product">Kalyanseva is an unique and different matrimonial website dedicated to help you find your betterhalf.Sign up is free. <a href="http://www.kalyanseva.com/"  target="_blank">JOIN NOW</a> </p>
        <hr class="para_bottom">
        <h3 class="ace_head"><span>Why</span> Kalyanseva</h3>
        <ul class="service_list" style="list-style-type:disc">
          <li>Automated monthly profile highlight view unlimited profiles and privacy enable profile list</li>
          <li>Free registration and smarter match selection</li>
          <li>View member Horoscope and send the personalized messages</li>
          <li>Assisted alliance and find the background screening</li>
          <li>Verified profile activation and daily recommendations as well as chat with mutual likes</li>
          <li>Wedding photography, albums, wedding catering and special wedding hall discounts</li>
        </ul>
      </div>
                
   </div>
</div>   
</section>             
</div>
</section>
<script type="text/javascript" src="/website/js/jquery-2.1.4.min.js"></script>
@stop