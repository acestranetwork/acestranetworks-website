<!DOCTYPE html>
<html>
<head>
	<title><?php
      if(isset($title)){ echo $title; } ?> - Acestra Networks</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="icon" href="/website/img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="/website/css/custom.css">
@yield('style')
</head>
<body  data-ng-app="mainApp">
<div class="se-pre-con"></div>
<!-- <section class="lang_sec">
  <div class="container">
    <div class="row">
      <div class="col-sm-12  col-xs-12 text-right">
        <p class="lang_selec_para">
        <i> Choose your Language</i>&nbsp;&nbsp; :
           <span> <a href="#" >  English </a> </span>
           <span> <a href="/abt_french"> French </a>  </span>
           <span> <a href="#"> Arabic </a></span>
      </p>
      </div>
    </div>
  </div>
</section> -->
  <div class="navbar-default navbar-fixed-top nav-fix">
    <div class="container">
      <div class="navbar-header" id="">
        <button type="button" class="navbar-toggle btn_hgt" data-toggle="collapse" data-target=".nav">
          <i class="fa fa-bars"></i>
        </button>
        <a  href="/index" ><img src="/website/img/acestra_logo.png" class="off_logo" alt="Acestra"></a>
      </div>
      <section class="navbar-collapse collapse nav navbar-nav navbar-right">
        <nav class="nav-effect" id="nav-effect">
        <?php $pages=Route::getCurrentRoute()->getPath();
        $career=explode("/", $pages);
        $page=$career[0];
        ?>
          <li > <a href="/index" data-hover="Home" class="<?php if($page=="index" || $page ==""){echo "active_col";} ?>">Home</a></li>
          <li ><a href="/aboutus" data-hover="About Us"  class="<?php if($page=="aboutus"){echo "active_col";} ?>">About Us</a></li>
          <li ><a href="/services" data-hover="Services"  class="<?php if($page=="services"){echo "active_col";} ?>">Services</a></li>
          <li><a href="/products" data-hover="Products"   class="<?php if($page=="products"){echo "active_col";} ?>">Products</a></li>
          <li ><a href="/careers" data-hover="Careers"  class="<?php if($page=="careers"||$page=="career"){echo "active_col";} ?>">Careers</a></li>
          <li><a href="/blog" data-hover="Blog"   class="<?php if($page=="blog"){echo "active_col";} ?>">Blog</a></li>
          <li ><a href="/quote" data-hover="Quote" class="<?php if($page=="quote"){echo "active_col";} ?>">Quote</a></li>
          <li ><a href="/contact" data-hover="Contact"  class="<?php if($page=="contact"){echo "active_col";} ?>">Contact</a></li>
          <li class="dropdown"><i class="fa fa-globe fa_lang_choose"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="caret"></b></a></i>
              <ul class="dropdown-menu">
                <li><a href="/english"> <i class="fa fa-circle-o-notch" aria-hidden="true"></i>
<!-- <img src="website/img/eng.png" class="lang_img_chose" alt="English" title="English"> --> &nbsp;&nbsp;&nbsp; English</a></li>
                <li><a href="/french"> <i class="fa fa-circle-o-notch" aria-hidden="true"></i>
<!-- <img src="website/img/french.png" class="lang_img_chose" alt="English" title="English"> -->&nbsp;&nbsp;&nbsp; French</a></li>
                <li><a href="/arabic"> <i class="fa fa-circle-o-notch" aria-hidden="true"></i>
<!-- <img src="website/img/arabic.png" class="lang_img_chose" alt="English" title="English"> -->&nbsp;&nbsp;&nbsp; Arabic</a></li>
              </ul>
          </li>

        </nav>
      </section>

      <!--End of the section -->
    </div>
  </div>


  @yield('web_content')

  <!-- Footer -->
 <section class="footer_section">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6">
          <h3 class="footer_head1">ABOUT US</h3>
          <p class="footer_para">ACESTRA has evolved into a competent software development company providing versatile, scalable, modular solutions. We have grown from a ...<a href="/aboutus" class="foot_more">Read More</a></p>
          <img src="/website/img/microsoft.png" alt="microsoft" class="img-responsive footer_image">
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
          <h3 class="footer_head1">CONTACT US</h3>
          <ul class="footer_head2list">
            <li><h5><b>Acestra Networks pvt ltd.</b></h5></li>
            <li> 2/2 Venkatesa Agraharam Street,</li> <li> Mylapore, Chennai-600004,India</li>
            <li><i class="fa fa-phone"></i>&nbsp; Phone :  +91 44 24629069 </li>
            <li> <i class="fa fa-print"></i>&nbsp; Fax : +91 44 45010069</li>
            <li><i class="fa fa-envelope"></i>&nbsp; <a href="mailto:support@acestranetworks.com" class="foot_mail">support@acestranetworks.com</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
          <h3 class="footer_head1">QUICK LINKS</h3>
          <ul class="footer_head2list">
            <li><a href="/aboutus">About Us</a></li>
            <li><a href="/services">Services</a></li>
            <li><a href="/products">Products</a></li>
            <li><a href="/careers">Careers</a></li>
            <li><a href="/blog_wp">Blog</a></li>
            <li><a href="/quote">Request Quote</a></li>
            <li><a href="/contact">Contact Us</a></li>
          </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6">
          <h3 class="footer_head1">GET IN TOUCH</h3>
          <p class="footer_para">Join our mailing list to stay up to date and get notices about our new releases!</p>
          <div class="col-sm-12 success_email" style="display:none;">
            <div class="alert alert-success alert-block error_box">
              <button type="button" class="close" data-dismiss="alert">&times;</button>
              <h4>Success  &#128522; </h4>
              <p class="sucess_msg"></p>
            </div>
          </div>
          <form action="" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}" class="token">
          <label style="color:#eee;"> Subscribe</label>
          <input type="email" required="required" name="email_id" class="email_req" placeholder="Email Address" >
          <a id="subs" class="subscriber_submit btn btn-primary btn-sm">➥</a><br/><span class="mesg" style="color:red"></span>
          </form>
          <h3 class="footer_head1">Follow us</h3>
          <ul class="footer_head2list social_footer">
            <li>
              <a class="facebook" href="https://www.facebook.com/Acestra-Network-1560470734248986/" target="_blank"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
              <a class="twitter" href="https://twitter.com/acestranetwork" target="_blank"><i class="fa fa-twitter" ></i></a>
            </li>
            <li>
              <a class="google" href="#" target="_blank"><i class="fa fa-google-plus" ></i></a>
            </li>
             <li>
              <a class="google" href="https://www.linkedin.com/company/acestra-networks-pvt.-ltd." target="_blank"><i class="fa fa-linkedin" ></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <div class="footer-copyright">
    <div class="container">
      <div class="row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6">
          <a href="/index"><img alt="Acestra" class="copy_img img-responsive" src="/website/img/footer.png"></a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
          <p class="footer_copypara">© Copyright 2015. All Rights Reserved.</p>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-center">
          <ul class="copy_list">
            <li><a href="/privacy">Privacy</a></li>
            <li>/ </li>
            <li><a href="/terms"> Terms &amp; Conditions</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript" src="/website/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
  $('li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
}, function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
});
</script>
<script>
  $(window).load(function() {
    $(".se-pre-con").fadeOut("slow");;
  });
</script>
<script>
  $(document).ready(function() {
    $(".refreshCaptcha").click(function(){
        $("img#captchaimg").remove();
        var id = Math.random();
        $('<img id="captchaimg" src="/captcha?id='+id+'" class="img-rounded" alt="captcha_image"/>' ).appendTo("#captchadiv");
        id ='';
    });

    $(".subscriber_submit").click(function(){
      var email=$(".email_req").val();
      var token=$(".token").val();
      if(email==""){
        $(".mesg").css("color","red");
        $(".mesg").html("Please enter email");

      }
      else{
         $.ajax({
        url: "subsciber_post",
        type: "post",
        data: {"email_id":email, "_token":token} ,
        success: function (response) {
          if(response=="success"){

            $(".mesg").css("color","green");
             $(".mesg").html("You have sucessfully subscribed");
             $(".email_req").val("");
             $(".email_req").addClass('success');
              setTimeout(function(){
                   $(".success_email").css("display","none");
              }, 2000);
          }
          else{
                  $(".mesg").css("color","red");
                  $(".mesg").html("email already exists");

          }
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
      }
    });
    $(".maps_iframe").click(function(){
      $("iframe").css("pointer-events","auto");
    });
});
</script>
<script>
    'use strict';

    var initScene = function () {

        if (document.body.clientWidth > 1023) {
            var scene = $('.tricons').parallax({
                originY: 0
            });
        }
    };

    $(function () {
        initScene();
    });

</script>
<script type="text/javascript">
 function validateForm() {

    flag = true;




       $(".validate").each(function(){
        var form_val = $(this).val();
        var form_id_val =  $(this).attr('id');

          if(form_val == '')
          {

              $("#"+form_id_val).addClass('error');
              flag = false;
          }else{
              $("#"+form_id_val).addClass('success');
          }


      });


        if(flag == false)
          {
              $(".highlight").css('display','block');
              $(".highlightmsg").html('Please fill all highlighted fields');
              return false;
          }

}
function validateContact() {
   flag = true;




       $(".validate").each(function(){
        var form_val = $(this).val();
        var form_id_val =  $(this).attr('id');

          if(form_val == '')
          {

              $("#"+form_id_val).addClass('error');
              flag = false;
          }else{
              $("#"+form_id_val).addClass('success');
          }


      });


        if(flag == false)
          {
              $(".highlight").css('display','block');
              $(".highlightmsg").html('Please fill all highlighted fields');
              return false;
          }else{

          }

}
function jobForm() {
   flag = true;
       $(".validate").each(function(){
        var form_val = $(this).val();
        var form_id_val =  $(this).attr('id');

          if(form_val == '')
          {

              $("#"+form_id_val).addClass('error');
              flag = false;
          }else{
              $("#"+form_id_val).addClass('success');
          }


      });


        if(flag == false)
          {
              $(".highlight").css('display','block');
              $(".highlightmsg").html('Please fill all highlighted fields');
              return false;
          }else{
            //check captcha

          }

}

</script>
<script type="text/javascript" src="/website/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/website/js/Modernizr.js"></script>
<script type="text/javascript" src="/website/js/slider.js"></script>
<script type="text/javascript" src="/website/js/progress.js"></script>
<script type="text/javascript" src="/website/js/lightbox.js"></script>
<script type="text/javascript" src="/website/js/parallax.js"></script>
<script type="text/javascript" src="/website/js/jquery.bxslider.js"></script>
<script type="text/javascript" src="/website/js/my_controller.js"></script>
  @yield('script')
<script type="text/javascript" src="/website/js/custom.js"></script>

<script type="text/javascript">

nav_fix
</script>

</body>
</html>
