<!DOCTYPE html>
<html>
    <head>
        <title>404 Error</title>
    </head>
    <style type="text/css">
        body
        {
    margin: 0px;
    font-family: Arial;
        }
        .error_outer,.error_img
        {
            width: 50%;
            margin: 0px auto;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .error
        {
            padding: 7em 0;
        }
        .error_content
        {
            text-align: center;
            color: #444;
        }

        .error_content h1 {
    font-size: 9em;
    margin: 0px;
}
.error_img img
{
    width:100%;
}
.error_content p{
    font-size: 3em;
    margin: 0px;
}
.home_btn{
       background: #ffcb3f;
    display: inline-block;
    font-size: 16px;
    margin-top: 15px;
    padding: 10px 15px;
    border: 1px solid #FFCB3F;
    font-weight: bold;
    cursor: pointer;
}
.home_btn a{
    color: #000000;
    text-decoration:none;
    }
    </style>
    <body>
    	<div class="error_outer">
    		<div class="error">
    			<div class="error_img">
    				<img src="/websiteV2/img/404.png">
    			</div>
    			<div class="error_content">
    				<h1>404</h1>
    				<p>Page Not Found</p>
                    <a href="/"><button class="home_btn">Take to Home</button></a>
    			</div>
    		</div>
    	</div>
    </body>
    </html>