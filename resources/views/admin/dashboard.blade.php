@extends('admin/admin_master')

@section('admin_content')
<aside class="right-side">

  <!-- Main content -->
  <section class="content">

    <div class="row" style="margin-bottom:5px;">


      <div class=" col-sm-4">
        <div class="sm-st clearfix">
          <a href="/admin/contact"><span class="sm-st-icon st-red"><i class="fa fa-phone"></i></span></a>
          <div class="sm-st-info">
            <span>Total Records {!! $total_Contact_count !!}</span>
            Contact us
          </div>
        </div>
      </div>
      <div class=" col-sm-4">
        <div class="sm-st clearfix">
          <a href="/admin/request_quote"><span class="sm-st-icon st-violet"><i class="fa fa-envelope-o"></i></span></a>
          <div class="sm-st-info">
            <span>Total Records  {!! $total_RequestQuote_count !!}</span>
           Request Quote
          </div>
        </div>
      </div>
      <div class="col-sm-4">
        <div class="sm-st clearfix">
          <a href="/admin/job_apply"><span class="sm-st-icon st-blue"> <i class="fa fa-pencil-square-o"></i></span></a>
          <div class="sm-st-info">
            <span>Total Records {!! $total_JobApply_count !!}</span>
            Apply job
          </div>
        </div>
      </div>
    
    </div>

    <!-- Main row -->
    <div class="row">

      <div class="col-md-8">
        <!--earning graph start-->
        <section class="panel">
          <header class="panel-heading">
            Request Quote
          </header>
          <div class="panel-body">
            <canvas id="linechart" width="400" height="330"></canvas>
          </div>
        </section>
        <!--earning graph end-->

      </div>
      <div class="col-lg-4">

        <!--chat start-->
        <section class="panel">
          <header class="panel-heading">
            Notifications
          </header>
          <div class="panel-body" id="noti-box">

            <div id="chartContainer1" style="height: 300px; width: 100%;">


</div>
            </div>
          </section>



        </div>


      </div>
  

      <!-- row end -->
    </section><!-- /.content -->

  </aside><!-- /.right-side -->
  <script type="text/javascript" src="/web/js/canvasjs.min.js"></script>

<script type="text/javascript" src="/admin_part/js/TableBarChart.js"></script>


  <script type="text/javascript">
  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer1",
    {
      title:{
        text: "Job Applications"
      },
      legend: {
        maxWidth: 350,
        itemWidth: 120
      },
      data: [
      {
        type: "pie",
        showInLegend: true,
        legendText: "{indexLabel}",
        dataPoints: [
        { y: <?php echo $analyst_programmer_count; ?>, indexLabel: "Analyst Programmer" },
        { y: <?php echo $web_developer_count; ?>, indexLabel: "Web Application Developer" },
        { y: <?php echo $project_coordinator_count; ?>, indexLabel: "Project Co-ordinator" },
        { y: <?php echo $copy_writer_count; ?>, indexLabel: "Copy Writer"}

        ]
      }
      ]
    });
    chart.render();
  }
  </script>
  @endsection