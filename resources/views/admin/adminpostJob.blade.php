@extends('admin/admin_master')

@section('admin_content')
<aside class="right-side" ng-app="acestra">

	
	<!-- Main content -->
	<section class="content" ng-controller="PostJobCntrl">
	 <div class="row">
	 	<div class="col-lg-6">
	 		<div class="panel">
					<header class="panel-heading">
						<i class="fa fa-bar-chart-o fa-fw"></i>Post New
                     
					</header>
					
					<div class="panel-body table-responsive">
            <ul ng-if="errors.length !=0">
              <li ng-repeat="error in errors" style="color: red">@{{error[0]}}</li>
            </ul>
            <p ng-if="message" ng-bind="message" style="color: green"></p>
                        <form >
                     
						  <div class="form-group">
						    <label for="email">Job ID</label>
						    <input type="text" class="form-control" id="email" ng-model="job.job_id" ng-disabled="true">
						  </div>
						  <div class="form-group">
						    <label for="pwd">Title:</label>
						    <input type="text" class="form-control" id="pwd" ng-model="job.title">
						  </div>
               <div class="form-group">
                <label for="pwd">Location:</label>
                <input type="text" class="form-control" id="pwd" ng-model="job.location">
              </div>
						  <div class="form-group">
						    <label for="pwd">Description:</label>
						    <textarea class="form-control" ng-model="job.description"></textarea>
						  </div>
						   <div class="form-group">
						    <label for="pwd">Training Period:</label>
						    <textarea class="form-control" ng-model="job.traning"></textarea>
						  </div>
						  <div class="form-group">
						    <label for="pwd">Opening date:</label>
						   <input type="text" class="form-control date" ng-model="job.opening" id="opening">
						  </div>
						   <div class="form-group">
						    <label for="pwd">Closing date:</label>
						    <input type="text" class="form-control date"  ng-model="job.closing" id="closing">
						  </div>
                        <p>Requirements</p>
						  <div class="form-group">
						    <textarea class="form-control" ng-model="job.requirements"></textarea>
                <span style="color:red">Give requirements ponints seperated by star.</span>
						  </div>

						   
						 
						  
						  <button type="button" class="btn btn-default" ng-click="add(job)">Submit</button>
						</form>
					</div>
			</div>
	 	</div>
	 	<div class="col-lg-6">
	 		   <div class="panel">
          <header class="panel-heading">
            <i class="fa fa-bar-chart-o fa-fw"></i>Jobs
          </header>
           <div class="panel-body table-responsive">
            <table class="table table-bordered table-striped">

              <thead>
                  <th>ID</th>
                  <th>Job ID</th>
                  <th>Tiltle</th>
                  <th>Opening Date</th>
                  <th>Closing Date</th>
                  <th>Status</th>
                  <th>Action</th>
              </thead>
               <tbody>
                <tr dir-paginate="application in job_lists  | filter:searchFish | itemsPerPage:10">
                  <td>@{{$index +1+ (currentPage - 1) * 10}}</td>
                  <td>@{{application.job_id}}</td>
                  <td ng-bind="application.title"></td>
                  <td ng-bind="application.opening"></td>
                  <td ng-bind="application.closing"></td>
                  <td>
                     <span ng-if="application.status==0">Closed</span>
                     <span ng-if="application.status==1">Live</span>
                   </td>
                  <td>
                    <a ng-click="edit(application.id)"><button class="label btn-success">Edit</button></a>
                    <a ng-click="status(application.id,1)"  ng-if="application.status==0"><button class="label btn-danger">Make live</button></a>
                    <a ng-click="status(application.id,0)"  ng-if="application.status==1"><button class="label btn-success">Make close</button></a>

                  </td>
                </tr> 
              </tbody>
              </table>
              <dir-pagination-controls
              max-size="5"
              direction-links="true"
              boundary-links="true" >
            </dir-pagination-controls>
          </div>

       </div>   
	 	</div>
	 </div>	
	  
    </section><!-- /.content -->

</aside><!-- /.right-side -->

@endsection