@extends('admin/admin_master')

@section('admin_content')
<aside class="right-side">

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-xs-12 col-lg-7">
				<div class="panel">
					<header class="panel-heading">
						<i class="fa fa-bar-chart-o fa-fw"></i>Subscriber Lists
					</header>
					<!-- <div class="box-header"> -->
					<!-- <h3 class="box-title">Responsive Hover Table</h3> -->

					<!-- </div> -->
					<div class="panel-body table-responsive">
						<div  ng-app="sortApp" ng-controller="mainController">
							<div class="alert alert-info" id="contact_dash">
								Sort Type: <span ng-bind="sortType"> </span><br>
								Sort Reverse: <span ng-bind="sortReverse"> </span><br>
								Search Query: <span ng-bind="searchFish"></span>
							</div>

							<form>
								<div class="form-group">
									<div class="input-group" id="contact_dash">
										<div class="input-group-addon"><i class="fa fa-search"></i></div>
										<input type="text" class="form-control" placeholder="Search" ng-model="searchFish">

									</div>      
								</div>

							</form>

							<table class="table table-bordered table-striped">

								<thead>
									<tr>
										<td>
											<a href="#">S.No.</a>
										</td>
										
										<td>
											<a href="#" ng-click="sortType = 'email'; sortReverse = !sortReverse">
												Email
												<span ng-show="sortType == 'email' && !sortReverse" class="fa fa-caret-down"></span>
												<span ng-show="sortType == 'email' && sortReverse" class="fa fa-caret-up"></span>
											</a>
										</td>

										<td>
										<a href="#">Date</a>
										</td>

									</tr>
								</thead>

								<tbody>
									<tr dir-paginate="subscriber in subscribers | orderBy:sortType:sortReverse | filter:searchFish | itemsPerPage:5">
										<td>@{{$index+1}}</td>
										<td ng-bind="subscriber.email_id"></td>
										<td ng-bind="subscriber.created_at"></td>
									</tr>
								</tbody>

							</table>
							<dir-pagination-controls
							max-size="5"
							direction-links="true"
							boundary-links="true" >
						</dir-pagination-controls>


					</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
	</div>
</section><!-- /.content -->

</aside><!-- /.right-side -->
<!-- /#page-wrapper -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
<script src="/website/js/dirPagination.js"></script> -->
<script>

var myApp = angular.module("sortApp", ['angularUtils.directives.dirPagination']);
myApp.controller('mainController', function($scope, $http){  
             $scope.sortType     = 'name'; // set the default sort type
			  $scope.sortReverse  = false;  // set the default sort order
			  $scope.searchFish   = '';     // set the default search/filter term

			  $http.get("/subscriber_details")
			  .success(function(response){
			  	$scope.subscribers = response;
			  	console.log("data loaded successfully");
			  });     

			});
</script>
@endsection