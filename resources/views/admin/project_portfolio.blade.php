@extends('admin/admin_master')

@section('admin_content')
<aside class="right-side">

	<!-- Main content -->
	<section class="content">
		
		<div class="row">
		@if(isset($quote_details->id))
		<div class="col-lg-6">
			@else
			<div class="col-lg-12">
				@endif
				<div class="panel">
					<header class="panel-heading">
						<i class="fa fa-bar-chart-o fa-fw"></i>Request Quote

					</header>
					<!-- <div class="box-header"> -->
					<!-- <h3 class="box-title">Responsive Hover Table</h3> -->

					<!-- </div> -->
					<div class="panel-body table-responsive">
						<div  ng-app="sortApp" ng-controller="mainController">

						<div class="alert alert-info" id="contact_dash">
							Sort Type: <span ng-bind="sortType"> </span><br>
							Sort Reverse: <span ng-bind="sortReverse"> </span><br>
							Search Query: <span ng-bind="searchFish"></span>
						</div>

						<form>
							<div class="form-group" id="contact_dash">
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-search"></i></div>
									<input type="text" class="form-control" placeholder="Search" ng-model="searchFish">
								</div>      
							</div>
						</form>

						<table class="table table-bordered table-striped">

							<thead>
						<tr>
									<td>
										<a href="#">S.No.</a>
									</td>
									<td>
										<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
											Name
											<span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
											<span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
										</a>
									</td>
									<td>
										<a href="#" ng-click="sortType = 'email'; sortReverse = !sortReverse">
											Email
											<span ng-show="sortType == 'email' && !sortReverse" class="fa fa-caret-down"></span>
											<span ng-show="sortType == 'email' && sortReverse" class="fa fa-caret-up"></span>
										</a>
									</td>
									@if(!isset($quote_details->id))
									<td>
										<a href="#" ng-click="sortType = 'phone'; sortReverse = !sortReverse">
											Mobile No.
											<span ng-show="sortType == 'phone' && !sortReverse" class="fa fa-caret-down"></span>
											<span ng-show="sortType == 'phone' && sortReverse" class="fa fa-caret-up"></span>
										</a>
									</td>
									<td>
										<a href="#" ng-click="sortType = 'subject'; sortReverse = !sortReverse">
											Subject
											<span ng-show="sortType == 'subject' && !sortReverse" class="fa fa-caret-down"></span>
											<span ng-show="sortType == 'subject' && sortReverse" class="fa fa-caret-up"></span>
										</a>
									</td>
									@endif
									<td>
										<a href="#" ng-click="sortType = 'expected_project_start_date'; sortReverse = !sortReverse">
											Expected Start Date
											<span ng-show="sortType == 'expected_project_start_date' && !sortReverse" class="fa fa-caret-down"></span>
											<span ng-show="sortType == 'expected_project_start_date' && sortReverse" class="fa fa-caret-up"></span>
										</a>
									</td>
									<td>
										<a href="#">View</a>
									</td>
									<td>
										<a href="#" >Documents</a>
									</td>
								</tr>
							</thead>

							<tbody>
								<tr dir-paginate="quote in request_quotes | orderBy:sortType:sortReverse | filter:searchFish | itemsPerPage:5">
									<td>@{{$index+1}}</td>
									<td>@{{quote.name}}</td>
									<td ng-bind="quote.email"></td>
									@if(!isset($quote_details->id))
									<td ng-bind="quote.phone"></td>
									<td ng-bind="quote.subject"></td>
									@endif
									<td ng-bind="quote.expected_project_start_date"></td>
									<td><a href="/admin/request_quote/@{{quote.id}}"><i class="fa fa-eye"></i></a></td>
									<td><a href="/@{{quote.file_path}}/@{{quote.document_name}}" download ><button class="label btn-success">Download</button></a></td>
								</tr>
							</tbody>

						</table>
	<dir-pagination-controls
							max-size="5"
							direction-links="true"
							boundary-links="true" >
						</dir-pagination-controls>
					</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
		@if(isset($quote_details->id))
			<div class="col-sm-12 col-lg-6">
				<div class="panel panel-default">
					<div class="panel-heading">
						<i class="fa fa-bar-chart-o fa-fw"></i> {!! $quote_details->name !!} 's Project Information Details
					</div>
					<!-- /.panel-heading -->
					<div class="panel-body">
						<p class="" style="text-align: justify;"><b>Name: </b>{!! $quote_details->name !!}<br><br>
						<b>Email: </b>{!! $quote_details->email !!}<br><br>
						<b>Position: </b>{!! $quote_details->expected_project_start_date !!}<br><br>
						<b>Subject: </b>{!! $quote_details->subject !!}<br><br>
						<b>Phone: </b>{!! $quote_details->phone !!}<br><br>
						<b>Cover Letter: </b>{!! $quote_details->message !!}<br></p>
					</div>
				</div>
			</div>
			@endif 
	</div>
</section><!-- /.content -->

</aside><!-- /.right-side -->
<!-- /#page-wrapper -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
<script src="/website/js/dirPagination.js"></script> -->
 <script src="/admin_part/js/visitor_ang.js" type="text/javascript"></script>
<script>

var myApp = angular.module("sortApp", ['angularUtils.directives.dirPagination']);
            myApp.controller('mainController', function($scope, $http){  
             $scope.sortType     = 'name'; // set the default sort type
			  $scope.sortReverse  = false;  // set the default sort order
			  $scope.searchFish   = '';     // set the default search/filter term

            $http.get("/request_quote_data")
                 .success(function(response){
                     $scope.request_quotes = response;
                     console.log("data loaded successfully");
                });     
  
	});

</script>
@endsection