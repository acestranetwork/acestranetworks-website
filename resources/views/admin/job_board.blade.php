@extends('admin/admin_master')

@section('admin_content')
<aside class="right-side">

	
	<!-- Main content -->
	<section class="content">
		
		<div class="row">
			@if(isset($person_details->id))
		<div class="col-lg-6">
			@else
			<div class="col-lg-12">
				@endif
				<div class="panel">
					<header class="panel-heading">
						<i class="fa fa-bar-chart-o fa-fw"></i>Applied Job
                        <p><a href="/admin/postJob">Post a job</a></p> 
					</header>
					
					<div class="panel-body table-responsive">
						<div  ng-app="sortApp" ng-controller="mainController">

						<div class="alert alert-info" id="contact_dash">
							Sort Type: <span ng-bind="sortType"> </span><br>
							Sort Reverse: <span ng-bind="sortReverse"> </span><br>
							Search Query: <span ng-bind="searchFish"></span>
						</div>

						<form>
							<div class="form-group" id="contact_dash">
								<div class="input-group">
									<div class="input-group-addon"><i class="fa fa-search"></i></div>
									<input type="text" class="form-control" placeholder="Search" ng-model="searchFish">
								</div>      
							</div>
						</form>

						<table class="table table-bordered table-striped">

							<thead>
								<tr>
										<td>
											<a href="#">S.No.</a>
										</td>
										<td>
											<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
												Name
												<span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
												<span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
											</a>
										</td>
										<td>
											<a href="#" ng-click="sortType = 'email'; sortReverse = !sortReverse">
												Email
												<span ng-show="sortType == 'email' && !sortReverse" class="fa fa-caret-down"></span>
												<span ng-show="sortType == 'email' && sortReverse" class="fa fa-caret-up"></span>
											</a>
										</td>
										<td>
											<a href="#" ng-click="sortType = 'position'; sortReverse = !sortReverse">
												Position
												<span ng-show="sortType == 'position' && !sortReverse" class="fa fa-caret-down"></span>
												<span ng-show="sortType == 'position' && sortReverse" class="fa fa-caret-up"></span>
											</a>
										</td>
										@if(!isset($person_details->id))
									<td>
										<a href="#" ng-click="sortType = 'subject'; sortReverse = !sortReverse">
											Subject
											<span ng-show="sortType == 'subject' && !sortReverse" class="fa fa-caret-down"></span>
											<span ng-show="sortType == 'subject' && sortReverse" class="fa fa-caret-up"></span>
										</a>
									</td>
									
									@endif
									<td>
										<a href="#">Date</a>
									</td>
									<td>
										<a href="#">Details</a>
									</td>
									<td>
										<a href="#">Resume</a>
									</td>
								</tr>
							</thead>

							<tbody>
								<tr dir-paginate="application in job_applications  | filter:searchFish | itemsPerPage:10">
									<td>@{{$index +1+ (currentPage - 1) * 10}}</td>
									<td>@{{application.name}}</td>
									<td ng-bind="application.email"></td>
									<td ng-bind="application.position"></td>
									@if(!isset($person_details->id))<td ng-bind="application.subject"></td>
									@endif
									<td ng-bind="application.created_at"></td>
									<td><a href="/admin/job_apply/@{{application.id}}"><i class="fa fa-eye"></i></a></td>
									<td>
										 <a href="/@{{application.file_path}}/@{{application.cv_name}}" download><button class="label btn-success">Download</button></a>
										 <a href="/@{{application.file_path}}/@{{application.cv_name}}" target="_blank"><button class="label btn-success">View</button></a>
										</td>
								</tr> 
							</tbody>

						</table>
	           <dir-pagination-controls
							max-size="5"
							direction-links="true"
							boundary-links="true" >
						</dir-pagination-controls>
					</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
		@if(isset($person_details->id))
		<div class="col-sm-12  col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading">
					<i class="fa fa-bar-chart-o fa-fw"></i> {!! $person_details->name !!} Job Details
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
				<p class="" style="text-align: justify;"><b>Name: </b>{!! $person_details->name !!}<br><br>
				<b>Email: </b>{!! $person_details->email !!}<br><br>
				<b>Position: </b>{!! $person_details->position !!}<br><br>
				<b>Subject: </b>{!! $person_details->subject !!}<br><br>
				<b>Phone: </b>{!! $person_details->phone !!}<br><br>
				<b>Cover Letter: </b>{!! $person_details->cover_letter !!}<br></p>
				</div>
			</div>
		</div>

		@endif 
	</div>
</section><!-- /.content -->

</aside><!-- /.right-side -->
<!-- /#page-wrapper -->
<!-- <script src="/admin/js/angular.min.js"></script>

<script src="/admin/js/dirPagination.js"></script> -->

<script>

	var myApp = angular.module("sortApp", ['angularUtils.directives.dirPagination']);

	myApp.controller('mainController', function($scope, $http){  
		$scope.sortType     = 'name'; // set the default sort type
		$scope.sortReverse  = false;  // set the default sort order
		$scope.searchFish   = '';     // set the default search/filter textrm
          

        $scope.currentPage = 1;
          
		$http.get("/apply_job_data")
		.success(function(response){
			$scope.job_applications = response;
			console.log("data loaded successfully");
		});     

	});

</script>
@endsection