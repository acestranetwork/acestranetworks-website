@extends('admin/admin_master')

@section('admin_content')
<aside class="right-side">

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-xs-12 col-lg-7">
				<div class="panel">
					<header class="panel-heading">
						<i class="fa fa-bar-chart-o fa-fw"></i>Contact Us

					</header>
					<!-- <div class="box-header"> -->
					<!-- <h3 class="box-title">Responsive Hover Table</h3> -->

					<!-- </div> -->
					<div class="panel-body table-responsive">
						<div  ng-app="sortApp" ng-controller="mainController">
							<div class="alert alert-info" id="contact_dash">
								Sort Type: <span ng-bind="sortType"> </span><br>
								Sort Reverse: <span ng-bind="sortReverse"> </span><br>
								Search Query: <span ng-bind="searchFish"></span>
							</div>

							<form>
								<div class="form-group">
									<div class="input-group" id="contact_dash">
										<div class="input-group-addon"><i class="fa fa-search"></i></div>
										<input type="text" class="form-control" placeholder="Search" ng-model="searchFish">

									</div>      
								</div>

							</form>

							<table class="table table-bordered table-striped">

								<thead>
									<tr>
										<td>
											<a href="#">S.No.</a>
										</td>
										<td>
											<a href="#" ng-click="sortType = 'name'; sortReverse = !sortReverse">
												Name
												<span ng-show="sortType == 'name' && !sortReverse" class="fa fa-caret-down"></span>
												<span ng-show="sortType == 'name' && sortReverse" class="fa fa-caret-up"></span>
											</a>
										</td>
										<td>
											<a href="#" ng-click="sortType = 'email'; sortReverse = !sortReverse">
												Email
												<span ng-show="sortType == 'email' && !sortReverse" class="fa fa-caret-down"></span>
												<span ng-show="sortType == 'email' && sortReverse" class="fa fa-caret-up"></span>
											</a>
										</td>

										<td>
											<a href="#" ng-click="sortType = 'subject'; sortReverse = !sortReverse">
												Subject
												<span ng-show="sortType == 'subject' && !sortReverse" class="fa fa-caret-down"></span>
												<span ng-show="sortType == 'subject' && sortReverse" class="fa fa-caret-up"></span>
											</a>
										</td>
										<td>
											<a href="#">View</a>
										</td>

									</tr>
								</thead>

								<tbody>
									<tr dir-paginate="person in visitors | orderBy:sortType:sortReverse | filter:searchFish | itemsPerPage:5">
										<td>@{{$index+1}}</td>
										<td>@{{person.name}}</td>
										<td ng-bind="person.email"></td>
										<td ng-bind="person.subject"></td>
										<td><a href="/admin/contact_us/@{{person.id}}"/><i class="fa fa-eye"></i></a></td>
									</tr>
								</tbody>

							</table>
							<dir-pagination-controls
							max-size="5"
							direction-links="true"
							boundary-links="true" >
						</dir-pagination-controls>


					</div>
				</div><!-- /.box-body -->
			</div><!-- /.box -->
		</div>
		@if(isset($contact_details->id))
	<div class="col-sm-12  col-lg-5">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-bar-chart-o fa-fw"></i> {!! $contact_details->name !!} Contact Details
			</div>
			<!-- /.panel-heading -->
			<div class="panel-body">
			<p class="" style="text-align: justify;"><b>Name:</b>{!! $contact_details->name !!}<br><br>
			<b>Email: </b>{!! $contact_details->email !!}<br><br>
			<b>Subject: </b>{!! $contact_details->subject !!}<br><br>
			<b>Message: </b>{!! $contact_details->message !!}<br><br>
			</div>
		</div>
	</div>
	@endif 
	</div>
</section><!-- /.content -->

</aside><!-- /.right-side -->
<!-- /#page-wrapper -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
<script src="/website/js/dirPagination.js"></script> -->
 <script src="/admin_part/js/visitor_ang.js" type="text/javascript"></script>
<script>

var myApp = angular.module("sortApp", ['angularUtils.directives.dirPagination']);
myApp.controller('mainController', function($scope, $http){  
             $scope.sortType     = 'name'; // set the default sort type
			  $scope.sortReverse  = false;  // set the default sort order
			  $scope.searchFish   = '';     // set the default search/filter term

			  $http.get("/contact_us_data")
			  .success(function(response){
			  	$scope.visitors = response;
			  	console.log("data loaded successfully");
			  });     

			});

</script>
@endsection