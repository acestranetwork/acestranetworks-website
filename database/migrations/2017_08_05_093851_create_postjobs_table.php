<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostjobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postjobs', function (Blueprint $table) {
            $table->increments('id');
             $table->string('job_id');
             $table->string('title');
             $table->string('location')->nullable();
             $table->mediumText('description');
             $table->mediumText('traning')->nullable();
             $table->mediumText('requirements');
             $table->date('opening');
             $table->date('closing');
             $table->integer('status',false,true);  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('postjobs');
    }
}
