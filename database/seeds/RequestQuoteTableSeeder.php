<?php
 
use Illuminate\Database\Seeder;

class RequestQuoteTableSeeder extends Seeder {

public function run()
{
	DB::table('quotes')->delete();

	$request_quote = array(
	  array('id' => '1','name' => 'Brindha','email' => 'ace@gmail.com','phone' => '9874563211','subject' => 'Need to create Cinema News Project','expected_project_start_date' => '6months','file_path' => '','document_name' => '','message' => 'The cinema of Tamil Nadu is a part of Indian cinema, producing films in the Tamil language. Based in the Kodambakkam district of Chennai, India, The industry is colloquially referred to as Kollywood, the term being a portmanteau of the words Kodambakkam and Hollywood.','status' => '1','created_at' => '2015-09-22 09:03:11','updated_at' => '0000-00-00 00:00:00'),
	  array('id' => '2','name' => 'Prasath','email' => 'mprasath@acestranetworks.com','phone' => '9865632168','subject' => 'E School Project','expected_project_start_date' => '6months','file_path' => '','document_name' => '','message' => 'E school management system project report is useful for computer science, bca, mca, mca students for developing this project as mini project. Main objective of this project is to develop a web based software application for schools and colleges for managing various types of students details and helping parents to analyze activities of students through online.','status' => '1','created_at' => '2015-09-22 09:08:10','updated_at' => '0000-00-00 00:00:00'),
	  array('id' => '3','name' => 'Thilaga','email' => 'rthilaga@acestranetworks.com','phone' => '9889564655','subject' => 'Real Estate Project','expected_project_start_date' => '3months','file_path' => '','document_name' => '','message' => 'Real estate is "property consisting of land and the buildings on it, along with its natural resources such as crops, minerals, or water; immovable property of this nature; an interest vested in this (also) an item of real property; (more generally) buildings or housing in general. Also: the business of real estate; the profession of buying, selling, or renting land, buildings or housing."','status' => '1','created_at' => '2015-09-22 09:11:20','updated_at' => '0000-00-00 00:00:00')
	);

	DB::table('quotes')->insert($request_quote);
}
}