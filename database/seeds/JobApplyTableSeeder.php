<?php
 
use Illuminate\Database\Seeder;

class JobApplyTableSeeder extends Seeder {

public function run()
{
	DB::table('job_applies')->delete();

	$job_apply = array(
  array('name' => 'Rajan','email' => 'rajan@gmail.com','phone' => '9898989989','subject' => 'Application for the post of Web Application Developer ','position' => 'web_application_developer','file_path' => 'uploads/Resume/2016/05','cv_name' => 'Resume94648.doc','cover_letter' => 'wqewe','status' => '1','created_at' => '2016-05-20 16:21:24','updated_at' => NULL),
  array('name' => 'dsfgdfg','email' => 'rthilaga@acestranetworks.com','phone' => '9874561230','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/06','cv_name' => 'Resume38491.docx','cover_letter' => 'dfgdfg','status' => '1','created_at' => '2016-06-17 09:38:24','updated_at' => NULL),
  array('name' => 'thilak','email' => 'ace@admin.com','phone' => '9874561230','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/06','cv_name' => 'Resume11197.docx','cover_letter' => 'thilak','status' => '1','created_at' => '2016-06-17 10:27:43','updated_at' => NULL),
  array('name' => 'Uthaya','email' => 'ace@admin.com','phone' => '8898574575','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume88052.doc','cover_letter' => 'I am willing to work in your company','status' => '1','created_at' => '2016-07-14 04:56:19','updated_at' => NULL),
  array('name' => 'Parthiban','email' => 'parthibankm92@gmail.com','phone' => '7418634320','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume93433.doc','cover_letter' => 'Hi 
Am parthiban from Madurai now currently stay in Chennai.

I have 2 and half years of experience in Accenture bpo.

I have 6 month experience in PHP developer

I have good knowledge in PHP HTML CSS JavaScript MySQL jquery','status' => '1','created_at' => '2016-07-21 15:57:19','updated_at' => NULL),
  array('name' => 'chandrasekaran','email' => 'chandrubala20@gmail.com','phone' => '9698765423','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume45450.pdf','cover_letter' => 'Dear sir, 
Please find the attached resume','status' => '1','created_at' => '2016-07-22 03:50:02','updated_at' => NULL),
  array('name' => 'Anandan k','email' => 'anandancse1@gmail.com','phone' => '8524926631','subject' => 'PHP Developer','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume89504.docx','cover_letter' => 'K.ANANDAN						
71, Kamarajar Street,
Vengamedu,
Karur.
Pincode: 639006.
TamilNadu,
 India.
E-mail: anandancse1@gmail.com					
Phone	: +91-8524926631

OBJECTIVE

To get placed in an organization where I could bring out my talents to the best and to contribute to its success through my enhanced technical and managerial skills, in a competitive and dynamic environment.

EDUCATIONAL QUALIFICATION

Course	Institution	University/
Board	Year of passing	Percentage / CGPA
B.E Computer Science	M.P.Nachimuthu M.Jaganathan Engineering College, Erode.	Anna University	2016	61%

H.Sc.,	Valallar Hr.Sec  School,
Karur	State Board	2012	70.25%
S.S.L.C	Valallar  Hr.Sec School, Karur	State Board	2010	75%


FIELD OF INTEREST
	Software Developer
	Computer Network
	Software Testing

COMPUTER COMPETENCY
Programming Languages		: 	C, C++, Java
Scripting  Language                     :           Html, PHP, CSS
           MS Office                                   	: 	Word, Excel, Powerpoint.
 
                                                                                                                         PROJECT:

Title                     	:    	Energy Efficient And Key Based Approach  For   
                                             Forwarding Aggregated Data In Wireless Sensor  
                                             Networks
Platform        		: 	DotNet
Domain                     :          Network Security
Description      	:	Using wireless sensor  to avoiding the missing files   
                                            while the processing of collusion attacks  To  send the    
                                            data securely

EXTRA-CURRICULAR  STUDIES

	Master Of Web And Graphics Designings by  Adoro Institute Of Multimedia In Coimbatore

	PHP by Adoro Institute Of Multimedia In Coimbatore


CO-CURRICULAR ACTIVITIES
	Participated  the Two Days Workshop On "JAVA" in HCL Career Development Centre, Chennai.
	Participated the Seminar on "NETWORKING AND SECURITY" on Avatar Academy.


HOBBIES
	Sports
	Reading  books

PERSONAL DETAILS
Name                		:  	K.Anandan
Father Name     		:  	A.Krishnan
Date Of Birth     		:  	05-01-1995
Gender	                     :  	Male
Language Known		:	Tamil & English
Marital Status    		:  	Single
Nationality &Religion  	: 	Indian &Hindu
Passport  Number              :          N3390304

DECLARATION 
I hereby declare that all the details furnished above are true to the best of my knowledge Should you be pleased to select me,I shall endeavor to offer you satisfactory service.
										                                     Yours Faithfully,
Place: 
Date:  
                                                                                                                (K.ANANDAN)
','status' => '1','created_at' => '2016-07-25 01:46:37','updated_at' => NULL),
  array('name' => 'Shankar','email' => 'Nameshankaran@gmail.com','phone' => '9688884032','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume88600.doc','cover_letter' => 'Sir, I\'m interested in php','status' => '1','created_at' => '2016-07-26 06:58:48','updated_at' => NULL),
  array('name' => 'T.Bharathan','email' => 'bharathan.it.12@petengg.ac.in','phone' => '9025465425','subject' => 'Web Developer','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume76844.pdf','cover_letter' => 'Hi Sir/Madam,

I am searching job for Software Company(UI Developer) and Fresher.

Thanks,
T.Bharathan','status' => '1','created_at' => '2016-07-26 07:08:15','updated_at' => NULL),
  array('name' => 'B Karthik Kumar','email' => 'bkkumar1994@gmail.com','phone' => '9717610597','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume48151.docx','cover_letter' => 'I would like to request you to consider me as you may  have a lot of options for this position to select. But I don\'t know about the rest. What I believe I meet the minimum requirement for this position, but I able to my job for its requirement. And apart from this my words cannot justify my action until I don\'t get the right opportunities to perform. I will be very privilege if you avail me this opportunity.Being a fresher I need a good platform to enhance my skills and to get the positive identity in the team, if you give me the opportunity then I am ready to give my 100% best. 

I am pursuing a course in Bachelor Of Technology in Computer Science and Engineering (final year) from Gurgaon Institute Of Technology & Management  and I would like to mention you that I have  done my summer internship with BILT (Ballarpur Industries Limited) for 3 months  and for 6 months at MERCER. It was a great experience working in a team.

I strongly feel that  training with you would provide me the best exposure in the industry and it would provide me an opportunity to prove my mettle and help me to improve.','status' => '1','created_at' => '2016-07-27 14:19:53','updated_at' => NULL),
  array('name' => 'Angamuthu','email' => 'vnsmuthu@gmail.com','phone' => '9952280202','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume40483.pdf','cover_letter' => 'hi i am angamuthu attached my resume please refer and update me','status' => '1','created_at' => '2016-07-28 06:37:09','updated_at' => NULL),
  array('name' => 'R.Manoraj','email' => 'smilemano23@gmail.com','phone' => '9943827872','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume72356.docx','cover_letter' => 'Respected sir/madam,
I have completed MCA with 8.5 CGPA. I have send my resume for your kind reference. I have knowledge in css, html and php.','status' => '1','created_at' => '2016-07-28 10:06:38','updated_at' => NULL),
  array('name' => 'Gayathri Marimuthu','email' => 'mgayathrimarimuthu@gmail.com','phone' => '9659914587','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/07','cv_name' => 'Resume76522.doc','cover_letter' => '28/07/2016

Hi,

With a proven track record of successful achievements, I am pleased to present my application and CV for your consideration as a Web Developer.

Having one year 3 months experience in the industry, and a strong educational background featuring a degree in Computer Science, I strongly believe I can make a significant contribution to First Media Web Developers.

My web developing expertise includes:

Designing, building and maintaining ecommerce websites.
Strong database knowledge specifically MySQL.
Extensive knowledge of  JavaScript, CSS and (X) HTML.
I possess good communication skills and can liaise effectively with both clients and work colleagues. Other strong points include an ability to work as part of a team or individually, multi-task, prioritize and work to deadlines under pressure.

Therefore in view of all of the above I would be highly gratified if you consider me for this important position.

I am available to come in for an interview at any time, and would appreciate the opportunity to meet with you and your staff. I hope that I am granted an opportunity to talk and perhaps meet with you in the very near future.

Hope to hear from you soon.

Thanks &Regards
<Gayathri.m>','status' => '1','created_at' => '2016-07-28 11:30:21','updated_at' => NULL),
  array('name' => 'dinesh','email' => 'dineshmail.kannan@gmail.com','phone' => '9942080003','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/08','cv_name' => 'Resume19733.docx','cover_letter' => 'dinesh','status' => '1','created_at' => '2016-08-03 14:09:44','updated_at' => NULL),
  array('name' => 'GURUPRASANTH V','email' => 'guruprasanth86@gmail.com','phone' => '8807703167','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/08','cv_name' => 'Resume70788.pdf','cover_letter' => 'Dear sir/madam,

I am V. Guruprasanth completed B.E Computer science and Engineering in J.J
College of Engineering and Technology Trichy with an aggregate of 65%.I am currently
looking for full time permanent positions with esteemed organizations like yours.

Adapting to various work cultures, identifying and resolving bottlenecks and
prioritizing have been striking features of my working style. In addition to the above, I
strongly believe in learning and sharing my knowledge with my colleagues. “Knowledge
parted is knowledge gained” has been my motto in life. I am keen to learn and am sharp
at identifying and solving problems.

Here by i attached a copy of my resume for your kind
reference.

Expecting a Favorable Reply,
Sincerely,
V. Guruprasanth
','status' => '1','created_at' => '2016-08-03 14:34:41','updated_at' => NULL),
  array('name' => 'Umamaheswari.M','email' => 'uma.kcy@gmail.com','phone' => '7708636674','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/08','cv_name' => 'Resume37708.docx','cover_letter' => 'BE Fresher seeking job ','status' => '1','created_at' => '2016-08-04 12:32:05','updated_at' => NULL),
  array('name' => 'Uthayak kumar','email' => 'uthayafriends@gmail.com','phone' => '9791963315','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/08','cv_name' => 'Resume29314.doc','cover_letter' => 'I would like to join your company','status' => '1','created_at' => '2016-08-08 07:28:47','updated_at' => NULL),
  array('name' => 'KATHIRESAN','email' => 'S','phone' => '8680036679','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/08','cv_name' => 'Resume56050.docx','cover_letter' => 'Sub : Job Application','status' => '1','created_at' => '2016-08-16 04:28:15','updated_at' => NULL),
  array('name' => 'PRITHIVIRAJKUMAR T','email' => 'tpritivimca@gmail.com','phone' => '9003312529','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/08','cv_name' => 'Resume65123.pdf','cover_letter' => 'I am looking for the php developer at your company and I am skilled with PHP,  css,  Photoshop.  ','status' => '1','created_at' => '2016-08-16 07:59:33','updated_at' => NULL),
  array('name' => 'PRIYADHARSHINI G','email' => 'gpriya2770@gmail.com','phone' => '8754237179','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/08','cv_name' => 'Resume97854.doc','cover_letter' => 'This is Priyadharshini.G I am from hosur, I did my college M.C.A at Adhiyamaan College of Engineering in Hosur (Autonomous), which is afflicted by Anna university Chennai and I was doing UG B.C.A at M.G.R college in hosur afflicted by Periyar university in salem.','status' => '1','created_at' => '2016-08-17 10:44:12','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume92255.docx','cover_letter' => 'dfgdfg','status' => '1','created_at' => '2016-09-01 09:29:01','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume18225.docx','cover_letter' => 'dfhmkglm','status' => '1','created_at' => '2016-09-01 09:33:46','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume91789.docx','cover_letter' => 'dfhmkglm','status' => '1','created_at' => '2016-09-01 09:34:43','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume68991.docx','cover_letter' => 'dfhmkglm','status' => '1','created_at' => '2016-09-01 09:34:58','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume73528.docx','cover_letter' => 'dfhdfh','status' => '1','created_at' => '2016-09-01 09:36:34','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume31933.docx','cover_letter' => 'sdfgsd','status' => '1','created_at' => '2016-09-01 09:57:26','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume33112.docx','cover_letter' => 'sdfasd','status' => '1','created_at' => '2016-09-01 10:17:44','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume89200.docx','cover_letter' => 'dfhgdfgh','status' => '1','created_at' => '2016-09-01 10:25:15','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume36671.docx','cover_letter' => 'dfhd','status' => '1','created_at' => '2016-09-01 10:27:02','updated_at' => NULL),
  array('name' => 'thilak','email' => 'rthilaga@acestranetworks.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume74625.docx','cover_letter' => 'thilak','status' => '1','created_at' => '2016-09-01 10:28:09','updated_at' => NULL),
  array('name' => 'raja','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume86752.docx','cover_letter' => 'dfhgdfg','status' => '1','created_at' => '2016-09-01 10:30:20','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1401 ','position' => 'P1401','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume40960.docx','cover_letter' => 'fghdfgh','status' => '1','created_at' => '2016-09-01 10:38:48','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1402 ','position' => 'P1402','file_path' => 'uploads/Resume/2016/09','cv_name' => 'Resume49886.docx','cover_letter' => 'sdfgsd','status' => '1','created_at' => '2016-09-01 10:43:54','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1405 ','position' => 'P1405','file_path' => 'uploads/Resume/2017/01','cv_name' => 'Resume48393.docx','cover_letter' => 'thilak','status' => '1','created_at' => '2017-01-28 06:55:19','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1405 ','position' => 'P1405','file_path' => 'uploads/Resume/2017/01','cv_name' => 'Resume41490.docx','cover_letter' => 'thilak','status' => '1','created_at' => '2017-01-28 07:03:09','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1405 ','position' => 'P1405','file_path' => 'uploads/Resume/2017/01','cv_name' => 'Resume95728.docx','cover_letter' => 'thilak','status' => '1','created_at' => '2017-01-28 07:03:33','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1405 ','position' => 'P1405','file_path' => 'uploads/Resume/2017/01','cv_name' => 'Resume84397.docx','cover_letter' => 'thilak','status' => '1','created_at' => '2017-01-28 07:06:31','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9874561230','subject' => 'Application for the post of P1405 ','position' => 'P1405','file_path' => 'uploads/Resume/2017/01','cv_name' => 'Resume52930.docx','cover_letter' => 'sdfg','status' => '1','created_at' => '2017-01-28 08:38:45','updated_at' => NULL),
  array('name' => 'thilak','email' => 'thilakazb@gmail.com','phone' => '9790601088','subject' => 'Application for the post of P1405 ','position' => 'P1405','file_path' => 'uploads/Resume/2017/01','cv_name' => 'Resume94963.docx','cover_letter' => 'fgh','status' => '1','created_at' => '2017-01-28 09:01:27','updated_at' => NULL)
);

	DB::table('job_applies')->insert($job_apply);
}
}