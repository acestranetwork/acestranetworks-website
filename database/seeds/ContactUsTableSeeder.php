<?php
 
use Illuminate\Database\Seeder;

class ContactUsTableSeeder extends Seeder {

public function run()
{
	DB::table('contacts')->delete();

	$contact = array(
	  array('id' => '1','name' => 'Brindha','email' => 'rthilaga@acestranetworks.com','subject' => 'Apply job','message' => 'At W3Schools you will find complete references about tags, attributes, events, color names, entities, character-sets, URL encoding, language codes, HTTP messages, and more.','status' => '1','created_at' => '2015-09-19 07:04:51','updated_at' => '0000-00-00 00:00:00'),
	  array('id' => '2','name' => 'Brindha','email' => 'brin@gmail.co.in','subject' => 'Enquiries','message' => 'This Bootstrap tutorial contains hundreds of Bootstrap examples.  With our online editor, you can edit the code, and click on a button to view the result.','status' => '1','created_at' => '2015-09-19 11:17:56','updated_at' => '0000-00-00 00:00:00'),
	  array('id' => '3','name' => 'Brindha','email' => 'mprasath@acestranetworks.com','subject' => 'Request job','message' => 'A jumbotron indicates a big box for calling extra attention to some special content or information.  A jumbotron is displayed as a grey box with rounded corners. It also enlarges the font sizes of the text inside it.','status' => '1','created_at' => '2015-09-19 11:28:56','updated_at' => '0000-00-00 00:00:00')
	);

	DB::table('contacts')->insert($contact);
}
}