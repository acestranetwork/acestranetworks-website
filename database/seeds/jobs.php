<?php

use Illuminate\Database\Seeder;

class jobs extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('postjobs')->delete();

        $postjobs = array(
  array('id' => '1','job_id' => 'P1401','title' => 'Analyst Programmer','location' => 'chennai','description' => 'We are seeking an Web Software Developer who will be responsible for designing and developing new systems, supporting and enhancing existing systems, and documenting technical specifications.','traning' => 'The selected candidate shall undergo a mandatory training period of 3 months.The candidate who is willing to learn and proves his/her potential shall be absorbed as a full time employee after the successful completion of the training period. Kindly note that remuneration will be in the form of a stipend during the training period.','requirements' => '*Education: Graduate degree/diploma in computer information technology or equivalent experience.
*Basic knlowedge with: C, C#, PHP, MySql,  SQL, JS, AngularJS, HTML, CSS.
*Technical knowledge in designing, building and implementing large scale, complex databases using MySQL.
*Be proficient in HTML,CSS,JS,AngularJS .
*Excellent verbal and written communication skills.
*Any Batch fresher can apply. And total vacancies 4.
*Provides work estimates for their own development tasks and verifies works estimates of development tasks on project plans.
*Have basic skills: Git/SVN, Objective C++, Ionic, PhoneGAP.','opening' => '2017-08-05','closing' => '2017-07-31','status' => '0','created_at' => '2017-08-05 10:52:33','updated_at' => '2017-08-05 13:33:57'),
  array('id' => '2','job_id' => 'P1402','title' => 'Web Application Developer','location' => 'Chennai','description' => 'We are looking for a motivated Web Developer who enjoys bringing out client\'s ideas to life by developing websites that stand out from the rest!','traning' => '','requirements' => '*Education: Graduate degree/diploma in computer information technology or equivalent experience
*Competent with HTML5, CSS3, JavaScript, Jquery, PHP, MySQL, WordPress.
*Have an eye for design and must be detail-oriented.
* Thorough knowledge of web development techniques and procedures.
*Design for mobile devices, responsive design and cross-platform development.
*Able to prioritize multiple project deadlines.
*Ability to work independently with minimal supervision in a fast-paced team environment.
*Great communication and customer service skills.
*Nice to have skills: Git/SVN, Angular, Node, MongoDB','opening' => '2017-08-05','closing' => '2017-07-31','status' => '0','created_at' => '2017-08-05 11:49:40','updated_at' => '2017-08-05 13:34:10'),
  array('id' => '3','job_id' => 'P1403','title' => 'Project Co-ordinator','location' => 'Chennai','description' => 'The Project Co-ordinator role has direct responsibility for complete life cycle management and accountability of project initiatives within Acestra Network, ensuring compliance to project management policy, standards and procedures. The Project Co-ordinator must also have a genuine interest to make a valuable contribution to increase capability and maturity of a rapidly growing company.','traning' => '','requirements' => '*Education: Qualified MA/BA with computing skills
*Project management experience a plus
*Fluent in English, oral and written
*Strong conceptual thinking and presentation skills
*Ability to take direction and critique
* Ability to collaborate with a team and also work autonomously
*Proven problem-solving skills and adaptability to change','opening' => '2017-08-04','closing' => '2017-08-30','status' => '1','created_at' => '2017-08-05 11:56:27','updated_at' => '2017-08-05 13:32:22'),
  array('id' => '4','job_id' => 'P1404','title' => 'Copy Writer','location' => 'Chennai','description' => 'Do you have the ability to write compelling creative copy? Do you know how to fashion ideas to achieve a communications strategy, incorporating marketing techniques into finely crafted prose that drives people to action?','traning' => '','requirements' => '*Education: Bachelor\'s Degree (English or Journalism preferred)
*Strong writing skills with 3+ years\' web journalism or copywriting experience
*Proficiency in Microsoft Word, Excel
*Proven ability to meet deadlines, handle multiple jobs simultaneously, and re-prioritize at a moment\'s notice
*Ability to work both independently and as part of a team
*Fast, flexible, detail-oriented, and cooperative work style
*Project management experience a plus, but not required','opening' => '2017-08-05','closing' => '2017-08-30','status' => '1','created_at' => '2017-08-05 11:57:20','updated_at' => '2017-08-05 13:32:25'),
  array('id' => '5','job_id' => 'P1405','title' => 'Web Software Developer','location' => 'Chennai','description' => 'We are seeking an Web Software Developer who will be responsible for designing and developing new systems, supporting and enhancing existing systems, and documenting technical specifications.','traning' => 'The selected candidate shall undergo a mandatory training period of 3 months.The candidate who is willing to learn and proves his/her potential shall be absorbed as a full time employee after the successful completion of the training period. Kindly note that remuneration will be in the form of a stipend during the training period.','requirements' => '*Education: Graduate degree/diploma in computer information technology or equivalent experience
*Basic knlowedge with: C#, ASP.NET, SQL Server, UML, Java, PHP.
*Technical knowledge in designing, building and implementing large scale, complex databases using SQL Server 2008/2012.
*Be proficient in HTML,CSS,JS,AngularJS .
*Functions at a high level of autonomy in setting objectives, expectations and tracking performance.
*Excellent verbal and written communication skills.
*Any Batch fresher can apply. And total vacancies 4.
*Provides work estimates for their own development tasks and verifies works estimates of development tasks on project plans.
*Have basic skills: Git/SVN, Objective C++, Ionic, PhoneGAP.','opening' => '2017-05-03','closing' => '2017-06-01','status' => '0','created_at' => '2017-08-05 13:21:44','updated_at' => '2017-08-05 13:21:44'),
  array('id' => '6','job_id' => 'P1406','title' => 'Software Developer / App Developer','location' => 'Chennai','description' => 'We are seeking an Web Software Developer who will be responsible for designing and developing new systems, supporting and enhancing existing systems, and documenting technical specifications.','traning' => 'The selected candidate shall undergo a mandatory training period of 3 months.The candidate who is willing to learn and proves his/her potential shall be absorbed as a full time employee after the successful completion of the training period. Kindly note that remuneration will be in the form of a stipend during the training period.','requirements' => '* Education: Graduate degree/diploma in computer information technology or equivalent experience
*Basic knlowedge with: C, C#, PHP, MySql, SQL, JS, AngularJS, HTML, CSS.
*Technical knowledge in designing, building and implementing large scale, complex databases using MySQL.
*Be proficient in HTML,CSS,JS,AngularJS .
*Excellent verbal and written communication skills.
*Any Batch fresher can apply. And total vacancies 4.
*Provides work estimates for their own development tasks and verifies works estimates of development tasks on project plans.
*Have basic skills: Git/SVN, Objective C++, Ionic, PhoneGAP.','opening' => '2017-08-01','closing' => '2017-09-01','status' => '1','created_at' => '2017-08-05 13:24:18','updated_at' => '2017-08-05 13:31:49'),
  array('id' => '7','job_id' => 'P1407','title' => 'Business Development Manager','location' => 'Chennai','description' => 'We are seeking an Business Development Manager who will be responsible for software marketing to communicate with clients.','traning' => 'We Will provide.','requirements' => '*Education: Qualified MA/BA, BBA/MBA with computing skills
*Project management skills a plus
.*Fluent in English, oral and written
*Strong conceptual thinking and presentation skills
*Team management
*Software marketing','opening' => '2017-08-01','closing' => '2017-08-31','status' => '1','created_at' => '2017-08-05 13:29:46','updated_at' => '2017-08-05 13:29:52')
);

 DB::table('postjobs')->insert($postjobs);
    }
}
