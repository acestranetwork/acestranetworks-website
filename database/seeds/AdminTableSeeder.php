<?php
 
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		$admin = array(array('id' => '1','name' => 'Brindha','email' => 'nbrindha@acestranetworks.com','password' => '$2y$10$1XEJUQ2Vs2MqGixjp9QhWeFuWs1xfd3MdTs5L1rkxuV2OmILbBQ1C','remember_token' => 'SkIBml9D38eObSE3Us8FwFA7j52rZJzcRpyeHfB4wK1MFg5SBarw8TjG4oIA','created_at' => '0000-00-00 00:00:00','updated_at' => '2015-09-28 14:35:00'),
		array('id' => '2','name' => 'ace','email' => 'ace@admin.com','password' => '$2y$10$4cuQ/T3kbZPuu5Ct6kJf5uU1G5UOyWWBLlvUcC/sdawUaOw7VT07S','remember_token' => 'SkIBml9D38eObSE3Us8FwFA7j52rZJzcRpyeHfB4wK1MFg5SBarw8TjG4oIA','created_at' => '0000-00-00 00:00:00','updated_at' => '2015-09-28 14:35:00'));

		DB::table('users')->insert($admin);
	}
}