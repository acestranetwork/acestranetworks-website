'use strict';


angular


    .module('quoteApp', ['angularFileUpload'])


    .controller('QuoteController', ['$scope', 'FileUploader', function($scope, FileUploader) {
        var uploader = $scope.uploader = new FileUploader({
            url: 'file_upload'
        }); 

        // FILTERS

              uploader.filters.push({
            name: 'extensionFilter',
            fn: function (item, options) {
                var filename = item.name;
                var extension = filename.substring(filename.lastIndexOf('.') + 1).toLowerCase();
                if (extension == "pdf" || extension == "doc" || extension == "docx" || extension == "jpg" || extension == "jpeg")
                    return true;
                else {
                    alert('Invalid file format. Please select a file with pdf/doc/docx/jpg or jpeg format  and try again.');
                    return false;
                }
            }
        });

        uploader.filters.push({
            name: 'sizeFilter',
            fn: function (item, options) {
                var fileSize = item.size;
                fileSize = parseInt(fileSize) / (1024 * 1024);
                if (fileSize <= 5)
                    return true;
                else {
                    alert('Selected file exceeds the 5MB file size limit. Please choose a new file and try again.');
                    return false;
                }
            }
        });

        uploader.filters.push({
            name: 'itemResetFilter',
            fn: function (item, options) {
                if (this.queue.length < 5)
                    return true;
                else {
                    alert('You have exceeded the limit of uploading files.');
                    return false;
                }
            }
        });

        // CALLBACKS

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);
 $scope.quote={"name":"Brindha","email":"ace@gmail.com","phone":"9874563211","subject":"sample","month":"6months"
,"message":"Message"};
      $scope.quoteSubmitForm=function(){
        $scope.submitted=true;

        if($scope.quoteForm.$valid){
          alert("Form is valid!");
          $http.post("<?php echo url() ?>/request_quote", $scope.quote)
          .success(function(data, status, headers, config){
            if(data=="fails")
            {
              $scope.quote_captcha=true;
              console.log("OOPS data inserted Failed");
            }
            else if(data=="success")
            {
              $scope.quote_form_success=true;
              console.log("data inserted successfully");
            }
            else if(data=="database_error")
            {
              console.log("Database Error.");
            }
                      //alert(data);
                        // $scope.user.push($scope.quote);

                      });

        }
        else{
          alert("Please correct Errors!");
        }
      };



       
}]);



