
 $(function() {
    var progressbar = $( "#progress-bar" ),
      progressLabel = $( ".progress-label" );
  
    progressbar.progressbar({
      value: false,
      change: function() {
        progressLabel.text( progressbar.progressbar( "value" ) + "%" );
      },
      complete: function() {
        progressLabel.text( "Loading Completed!" );
      }
    });
  
    function progress() {
      var val = progressbar.progressbar( "value" ) || 0;
  
      progressbar.progressbar( "value", val + 2 );
  
      if ( val < 83 ) {
        setTimeout( progress, 90 );
      }
    }
  
    setTimeout( progress, 2500 );
  });
  
 $(function() {
    var progressbar = $( "#progress-bar1" ),
      progressLabel = $( ".progress-label1" );
  
    progressbar.progressbar({
      value: false,
      change: function() {
        progressLabel.text( progressbar.progressbar( "value" ) + "%" );
      },
      complete: function() {
        progressLabel.text( "Loading Completed!" );
      }
    });
  
    function progress() {
      var val = progressbar.progressbar( "value" ) || 0;
  
      progressbar.progressbar( "value", val + 2 );
  
      if ( val < 90 ) {
        setTimeout( progress, 90 );
      }
    }
  
    setTimeout( progress, 2500 );
  });

 $(function() {
    var progressbar = $( "#progress-bar2" ),
      progressLabel = $( ".progress-label2" );
  
    progressbar.progressbar({
      value: false,
      change: function() {
        progressLabel.text( progressbar.progressbar( "value" ) + "%" );
      },
      complete: function() {
        progressLabel.text( "Loading Completed!" );
      }
    });
  
    function progress() {
      var val = progressbar.progressbar( "value" ) || 0;
  
      progressbar.progressbar( "value", val + 2 );
  
      if ( val < 78) {
        setTimeout( progress, 90 );
      }
    }
  
    setTimeout( progress, 2500 );
  });

 $(function() {
    var progressbar = $( "#progress-bar3" ),
      progressLabel = $( ".progress-label3" );
  
    progressbar.progressbar({
      value: false,
      change: function() {
        progressLabel.text( progressbar.progressbar( "value" ) + "%" );
      },
      complete: function() {
        progressLabel.text( "Loading Completed!" );
      }
    });
  
    function progress() {
      var val = progressbar.progressbar( "value" ) || 0;
  
      progressbar.progressbar( "value", val + 2 );
  
      if ( val < 88) {
        setTimeout( progress, 90 );
      }
    }
  
    setTimeout( progress, 2500 );
  });

