//Logo Scroll Small
function init() {
	window.addEventListener('scroll', function(e){
		var distanceY = window.pageYOffset || document.documentElement.scrollTop,
		shrinkOn = 100,
		header = document.querySelector("header");
		if (distanceY > shrinkOn) {
			classie.add(header,"smaller");
		} else {
			if (classie.has(header,"smaller")) {
				classie.remove(header,"smaller");
			}
		}
	});
}

window.onload = init();
// Display the progress bar calling progressbar.js
	$('.progressbar1').progressBar({
			shadow : true,
			percentage : false,
			animation : true,
			barColor : "#88CD2A",
	});
	$('.progressbar2').progressBar({
		shadow : true,
		percentage : false,
		animation : true,
		barColor : "#428BCA",
	});
	$('.progressbarPhp').progressBar({
		shadow : true,
		percentage : false,
		animation : true,
		animateTarget : true,
		barColor : "#52ADF9",
	});
	$('.progressbarGit').progressBar({
		shadow : true,
		percentage : false,
		animation : true,
		barColor : "#6adcfa",
	});
	$('.progressbar3').progressBar({
		shadow : true,
		percentage : false,
		animation : true,
		animateTarget : true,
		barColor : "#ED5441",
	});

	//Menu
	$(".spinDown").click(function() {
		var target = $(this).data("target");
		var scrollFix = -80;
		target = "#" + target;
		$("html,body").animate({
			scrollTop : $(target).offset().top + scrollFix
		}, 1000);
		return false;
	});





	// For Demo purposes only (show hover effect on mobile devices)
			[].slice.call( document.querySelectorAll('a[href="#"') ).forEach( function(el) {
				el.addEventListener( 'click', function(ev) { ev.preventDefault(); } );
			} );
//Skillbar Plugin

				jQuery('.skillbar').each(function(){
				jQuery(this).find('.skillbar-bar').animate({
					width:jQuery(this).attr('data-percent')
				},8000);
			});





			//title Scroll

	