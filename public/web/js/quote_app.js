angular.module('sortApp', [])

.controller('mainController', function($scope) {

  $scope.sortType     = 'name'; // set the default sort type
  $scope.sortReverse  = false;  // set the default sort order
  $scope.searchFish   = '';     // set the default search/filter term
  
  // create the list of sushi rolls 
  $scope.sushi = [
    { name: '01/08/2018', email: 'asarveshteki@gmail.com', mobile: '9840778974', tastiness: 2, download: 2 },
    { name: '01/08/2015', email: 'sarveshteki@gmail.com', mobile: '9840578974', tastiness: 4, download: 4},
    { name: '01/02/2015', email: 'sarveshgeki@gmail.com', mobile: '9840575974', tastiness: 7, download: 2 },
    { name: '01/08/2015', email: 'sarveshteki@gmail.com', mobile: '9840978974', tastiness: 6, download: 9 }
  ];
  
});
