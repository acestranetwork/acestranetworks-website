angular.module('sortApp', [])

.controller('mainController', function($scope) {

  $scope.sortType     = 'name'; // set the default sort type
  $scope.sortReverse  = false;  // set the default sort order
  $scope.searchFish   = '';     // set the default search/filter term
  
  // create the list of sushi rolls 
  $scope.sushi = [
    { name: '01/08/2015', fish: 'Sam', Position: 'Analyst Programmer', tastiness: 2, download: '<a href="">Hi</a>' },
    { name: '01/08/2015', fish: 'John', Position: 'Copy Writer', tastiness: 4, download: 2},
    { name: '01/08/2015', fish: 'Doe', Position: 'Software Developer', tastiness: 7, download: 2 },
    { name: '01/08/2015', fish: 'Samuel', Position: 'Project Co-ordinator', tastiness: 6, download: 2 }
  ];
  
});
