$(document).ready(function(){
	/* Nomenclature **************/
	var label= $('label');
	var content= $('.content');
	
	/* Default values ***********/
	$(content).not(":first").hide();
	$(label).addClass('default');
	$('label:first').addClass('clicked');
	
	/* Mouse click (label)****/
	$(label).on("click",function(){
		/* Nomenclature **************/
		var tLabel = $(this);
		var tContent = tLabel.next();
		
		/* Default values ***********/
		content.slideUp("normal");
		tContent.slideDown("slow");

		/* Change Label Color */
		$(label).removeClass('clicked');
		$(tLabel).addClass('clicked');
		
		/* Open and close content */
		if(tContent.is(":visible")) {
			return;
		}
	});	
	
	/* Mouse Over On (label)****/
	$(label).hover(
	function(){
		var tLabel = $(this);
		$(tLabel).addClass('hover');
	},
	function(){
		$(label).removeClass('hover');
		
	});	
});