	var myApp=angular.module('subApp', []);
	myApp.controller('SubController', function($scope, $http){

		$scope.loginForm=function(){
			$scope.submitted=true;
			if($scope.subscriberForm.$valid){
			$http.post("getSubscriberInfo", $scope.subscriber)
			.success(function(data, status, headers, config){
				
				if(data=="email_exists")
				{
					$('#subscriber_success').hide();
					$('#email_exists').show();	
				}
				else if(data=="success")
				{	
					$('#email_exists').hide();	
					$('#subscriber_success').show();		
					$(".cleartext").val("");
				}
            });
		}
		else{
			//alert("Please give your Email Address!");
		}
	};
});