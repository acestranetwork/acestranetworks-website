var myApp=angular.module('mainApp', []);

myApp.controller('ContactController', function($scope, $http){
//$scope.contact={"name":"Brindha","email":"brin@gmail.co.in","subject":"sample","message":"sample message"};
$scope.submitForm=function(){
	$scope.submitted=true;
	if($scope.contactForm.$valid){
		$http.post("contact_user", $scope.contact)
		.success(function(data, status, headers, config){

			if(data=="success")
			{
				$(".cleartext").val("");
				$scope.successTextAlert = data;
				$scope.showCaptchaAlert = false;
				$scope.showSuccessAlert = true;

// switch flag
$scope.switchBool = function (value) {
	$scope[value] = !$scope[value];
};
}
else{
	if(data=="captcha_not_match")
	{
		$scope.successCaptchaAlert = "Sorry Your Captcha not matched";
		$scope.showSuccessAlert = false;
		$scope.showCaptchaAlert = true;


// switch flag
$scope.switchBool = function (value) {
	$scope[value] = !$scope[value];
};
}
}
});
	}
	else{

		$scope.errorAlert = "Sorry! Please correct Error(s)!";
		$scope.showSuccessAlert = false;
		$scope.showCaptchaAlert = false;
		$scope.showErrorAlert = true;
//alert("Please correct Error(s)!");
// switch flag
$scope.switchBool = function (value) {
	$scope[value] = !$scope[value];
};
}
};

//refresh_captcha
$scope.refresh_captcha=function(){
	var base_url=$scope.base_url;
	$http.post("/refresh_captcha")
	.success(function(data, status, headers, config)
	{
		var changed_captcha_img = angular.element( document.querySelector( '#changed_captcha' ) );
		changed_captcha_img.html('<p id="changed_captcha" class="captcha_text">'+data+'</p>'); 
		$scope.contact.captcha_real=data;
		$scope.contact.captcha_user='';
	});
};

});
myApp.controller('SubController', function($scope, $http){

	$scope.submitForm=function(){
		$scope.submitted=true;
		if($scope.subscriber_Form.$valid){
			$http.post("getSubscriberInfo", $scope.subscriber)
			.success(function(data, status, headers, config){

				if(data=="email_exists")
				{
					$('#subscriber_success').hide();
					$('#email_exists').show();	
				}
				else if(data=="success")
				{	
					$('#email_exists').hide();	
					$('#subscriber_success').show();		
					$(".cleartext").val("");
				}
			});
		}
		else{
//alert("Please give your Email Address!");
}
};
});

//-----------------------------------------------------------------------------------------------------------------
myApp.directive('fileModel', ['$parse', function ($parse) {

	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function(){
				scope.$apply(function(){
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
}]);

myApp.service('fileUpload1', ['$http', function($http) {
	this.uploadFileToUrl = function(name,email,phone,subject,month,file,message,captcha_real,captcha_user,uploadUrl){
		var fd = new FormData();

		fd.append("name", name);
		fd.append('email', email);
		fd.append('phone', phone);
		fd.append('subject', subject);
		fd.append('month', month);
		fd.append('file', file);
		fd.append('message', message);
		fd.append('captcha_real', captcha_real);
		fd.append('captcha_user', captcha_user);

		$http.post(uploadUrl, fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		})
		.success(function(data){

			if(data=="success")
			{ 
				$("#file_error_btn").hide();
				$("#error_btn").hide(); 
				$("#success_btn").show();
				$("#quote_file").val("");
				$(".cleartext").val("");
				$('input[name="month"]').attr('checked', false);
			}
			else{
				if(data=="captcha_not_match")
				{
					$("#file_error_btn").hide();
					$(".captcha_input").val("");
					$("#success_btn").hide();
					$("#error_btn").show();
				}
				if(data=="file_error")
				{
					$("#quote_file").val("");
					$("#success_btn").hide();
					$("#error_btn").hide();
					$("#file_error_btn").show();
				}
			}

		});

	}
}]);

myApp.controller('QuoteController', ['$scope', 'fileUpload1','$http', function($scope, fileUpload1, $http){

	$scope.uploadFile = function(){

		var name = $scope.name;
		var email = $scope.email;
		var phone = $scope.phone;
		var subject = $scope.subject;
		var month = $scope.month;
		var file = $scope.myFile;
		var message = $scope.message;
		var captcha_real = $scope.captcha_real;
		var captcha_user = $scope.captcha_user;

		var uploadUrl = "request_quote";
		fileUpload1.uploadFileToUrl(name,email,phone,subject,month,file,message,captcha_real,captcha_user,uploadUrl);
	};


	$scope.submitForm = function () {
// Set the 'submitted' flag to true
$scope.submitted = true;
if ($scope.quoteForm.$valid) {
//alert("Form is valid!");
}
else {
	$scope.errorAlert = "Sorry! Please correct Error(s)!";
	$scope.showErrorAlert = true;
// switch flag
$scope.switchBool = function (value) {
	$scope[value] = !$scope[value];
};
}
};

//refresh_captcha

$scope.refresh_captcha=function(){
	$http.post("/refresh_captcha")
	.success(function(data, status, headers, config)
	{
		var changed_captcha_img = angular.element( document.querySelector( '#changed_captcha' ) );
		changed_captcha_img.html('<p id="changed_captcha" class="captcha_text">'+data+'</p>'); 
		$scope.captcha_real=data;
//$scope.captcha_user='';
});
};


}]);
//----------------------------------------------------------------------------------------------------------------
//Apply
myApp.service('fileUpload', ['$http', function ($http) {
	this.uploadFileToUrl = function(position,name,email,phone,subject,project_file,file,cover_letter,captcha_real,captcha_user,uploadUrl){
		var fd = new FormData();

		fd.append("position", position);
		fd.append("name", name);
		fd.append('email', email);
		fd.append('phone', phone);
		fd.append('subject', subject);
		fd.append('project_file', project_file);
		fd.append('file', file);
		fd.append('cover_letter', cover_letter);
		fd.append('captcha_real', captcha_real);
		fd.append('captcha_user', captcha_user);

		$http.post(uploadUrl, fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		})
		.success(function(data){
// alert(data);
if(data=="success")
{ 
	$("#file_error_btn").hide();
	$("#error_btn").hide(); 
	$("#success_btn").show();
	$("#apply_file").val("");
	$(".cleartext").val("");
}
if(data=="captcha_not_match")
{
	$("#file_error_btn").hide();
	$(".captcha_input").val("");
	$("#success_btn").hide();
	$("#error_btn").show();
}
if(data=="file_error")
{
	$("#apply_file").val("");
	$("#success_btn").hide();
	$("#error_btn").hide();
	$("#file_error_btn").show();
}
});
	}
}]);


myApp.controller('ApplyController', ['$scope', 'fileUpload','$http', function($scope, fileUpload, $http){

	$scope.uploadFile = function(){

		var position = $scope.position;
		var name = $scope.name;
		var email = $scope.email;
		var phone = $scope.phone;
		var subject = $scope.subject;
		var project_file = $scope.project_file;
		var file = $scope.myFile;
		var cover_letter = $scope.cover_letter;
		var captcha_real = $scope.captcha_real;
		var captcha_user = $scope.captcha_user;

		var uploadUrl = "job_apply_insert";
		fileUpload.uploadFileToUrl(position,name,email,phone,subject,project_file,file,cover_letter,captcha_real,captcha_user,uploadUrl);
	};

	$scope.submitForm = function () {
// Set the 'submitted' flag to true
$scope.submitted = true;

if ($scope.applyForm.$valid) {
//alert("Form is valid!");
}
else {

	$scope.errorAlert = "Sorry! Please correct Error(s)!";
	$scope.showErrorAlert = true;
// switch flag
$scope.switchBool = function (value) {
	$scope[value] = !$scope[value];
};
}
};

$scope.refresh_captcha=function(){
	$http.post("/refresh_captcha")
	.success(function(data, status, headers, config)
	{
		var changed_captcha_img = angular.element( document.querySelector( '#changed_captcha' ) );
		changed_captcha_img.html('<p id="changed_captcha" class="captcha_text">'+data+'</p>'); 
		$scope.captcha_real=data;
//$scope.captcha_user='';
});
};


}]);