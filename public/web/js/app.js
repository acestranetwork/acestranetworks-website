
var app = angular.module('sortApp', ['angularUtils.directives.dirPagination']);

app.controller('mainController',function($scope, $http){
  $scope.users = []; //declare an empty array
  $http.get("mockJson/mock.json").success(function(response){ 
    $scope.users = response;  //ajax request to fetch data into $scope.data
  });
  
  $scope.sort = function(keyname){
    $scope.sortKey = keyname;   //set the sortKey to the param passed
    $scope.reverse = !$scope.reverse; //if true make it false and vice versa
  }
});