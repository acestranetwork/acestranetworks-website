 jQuery(document).ready(function ($) {
            
            var jssor_1_options = {
              $AutoPlay: true,
              $Idle: 0,
              $AutoPlaySteps: 4,
              $SlideDuration: 1600,
              $SlideEasing: $Jease$.$Linear,
              $PauseOnHover: 4,
              $SlideWidth: 140,
              $Cols: 7
            };
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 809);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
//////////////progress bar
         // Display the progress bar calling progressbar.js
            $('.progressbar1').progressBar({
                shadow: true,
                percentage: false,
                animation: true,
                barcolor: "#035E8D",
                height: 10,
            });
            $('.progressbar2').progressBar({
                shadow: true,
                percentage: false,
                animation: true,
                barColor: "#045E92",
                height: 10,
            });
            $('.progressbarPhp').progressBar({
                shadow: true,
                percentage: false,
                animation: true,
                animateTarget: true,
                barColor: "#139475",
                height: 10,
            });
            $('.progressbarGit').progressBar({
                shadow: true,
                percentage: false,
                animation: true,
                barColor: "#E98B05",
                height: 10,
            });
            $('.progressbar3').progressBar({
                shadow: true,
                percentage: false,
                animation: true,
                animateTarget: true,
                barColor: "#9369BB",
                height: 10,
            });

            //Menu
            $(".spinDown").click(function () {
                var target = $(this).data("target");
                var scrollFix = -80;
                target = "#" + target;
                $("html,body").animate({
                    scrollTop: $(target).offset().top + scrollFix
                }, 1000);
                return false;
            });

//////////

$(document).ready(function() {
    $('#Carousel').carousel({
        interval: 2000
    })
});



// var mql = window.matchMedia("screen and (max-width: 800px)");
// console.log(window.screen.width);
if(window.screen.width>1262)
 {
$(window).scroll(function () {
        if ($(window).scrollTop() > 50) {
          $(".navbar-default").addClass("nav_scroll");
        } else {
          $(".navbar-default").removeClass("nav_scroll");
        }
    });
   
 }
else
{
       
} 

   //////////
   $(function() {

  var $window           = $(window),
      win_height_padded = $window.height() * 1.1,
      isTouch           = Modernizr.touch;

  if (isTouch) { $('.revealOnScroll').addClass('animated'); }

  $window.on('scroll', revealOnScroll);

  function revealOnScroll() {
    var scrolled = $window.scrollTop(),
        win_height_padded = $window.height() * 1.1;

    // Showed...
    $(".revealOnScroll:not(.animated)").each(function () {
      var $this     = $(this),
          offsetTop = $this.offset().top;

      if (scrolled + win_height_padded > offsetTop) {
        if ($this.data('timeout')) {
          window.setTimeout(function(){
            $this.addClass('animated ' + $this.data('animation'));
          }, parseInt($this.data('timeout'),10));
        } else {
          $this.addClass('animated ' + $this.data('animation'));
        }
      }
    });
    // Hidden...
   $(".revealOnScroll.animated").each(function (index) {
      var $this     = $(this),
          offsetTop = $this.offset().top;
      if (scrolled + win_height_padded < offsetTop) {
        $(this).removeClass('animated fadeInUp flipInX lightSpeedIn')
      }
    });
  }

  revealOnScroll();
});