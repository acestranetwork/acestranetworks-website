

var myApp = angular.module("acestra", ['angularUtils.directives.dirPagination']);

	myApp.controller('PostJobCntrl', function($scope, $http,GenFactory){  
		//alert('hi');
		$scope.sortType     = 'name'; // set the default sort type
		$scope.sortReverse  = false;  // set the default sort order
		$scope.searchFish   = '';     // set the default search/filter textrm
          

        $scope.currentPage = 1;
          

    $scope.add = function (job) {
        var x=angular.element(document.getElementById("opening"));
        $scope.job.opening = x.val();

        var y=angular.element(document.getElementById("closing"));
        $scope.job.closing = y.val();


			$http.post("/admin/add_job_data",job)
			.success(function(response){
				if(response.status == 'wrong'){
					$scope.errors = response.data;
				}else{
					$scope.message = "Success";
					$scope.job = '';
					list();
				}
			});  
		}   

     		  var list  = function () {
     		  	GenFactory.get_List('/admin/posted_jobs').then(function(result){
                      $scope.job_lists = result.data;
                });
     		  }
     		  list();

		  $scope.edit = function (id) {
             GenFactory.post_List_obj('/admin/edit_posted_jobs',{'id':id}).then(function(result){
                      $scope.job = result.data;
              });
		  }

		  $scope.status  = function (id,status) {
		  	 GenFactory.post_List_obj('/admin/status_posted_jobs',{'id':id,'status':status}).then(function(result){
                      list();
              });
		  }


	});

myApp.factory('GenFactory',function($http,$log,$q){
  return{
    get_List:function(url){
    var deferred = $q.defer();
     $http.get(url)
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  },
  post_List:function(url,id){
    var deferred = $q.defer();
     $http.post(url,{'id':id})
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  },
  post_List_obj:function(url,obj){
    var deferred = $q.defer();
     $http.post(url,obj)
     .success(function(data){
      deferred.resolve(data);
     }).error(function(msg,code){
      deferred.reject(msg);
          $log.error(msg, code);
     });
     return deferred.promise;
  }

  }

});
