<?php 
$currentCategory = get_query_var('cat');
			/*
			var_dump($currentCategory);
			$category_args = array(
				'post_type' 	  		   => 'shortfilm',
				'child_of'                 => $currentCategory
			); 
			$categories = get_categories( $category_args );
			var_dump($categories);
			*/
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;  
			$args = array(				
				'paged' 		  => $paged,				
				'category__in'	  => $currentCategory,
				'orderby'		  => 'ID',
				'order'			  => 'DESC',
				'post_status' => 'publish'
				);
			$gloop = new WP_Query( $args );
			//var_dump($gloop);
			// echo "<pre>";
			// print_r($gloop);			
			if($gloop->have_posts()):	
				$currentPageCategoryName = "";
			while ( $gloop->have_posts() ) : $gloop->the_post();
			?>
	<div class="wp_contents row blog_post">			
<div class="row">
    <div class="col-lg-2 col-md-2 col-sm-2 text-left">
     	 	    	 <div class="date-wrap">        
			          <span class="date">
			            <?php the_time('j') ?>
			          </span>
			          <span class="month">
			            <?php the_time('M') ?>
			          </span>
			        </div>
     	</div>
     	<div class="col-lg-10 col-md-10 col-sm-10 text-left">
     	 	  		
     	 	  		<h3 class="upper-text"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>
		
     	 </div> 

</div>			
<div class="row wp_contents_para">
         <?php 
    	       $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
				<?php if($feat_image!=""):?>
			      <div class="col-lg-12 wp_content_image">		
				     <img src="<?php echo $feat_image ?>" class="img-responsive wp_img" style="width:100%">
				 </div>
				<?php endif;
	    ?>

        				
				<?php the_excerpt(); ?>

				  <?php $orig_post = $post;
				global $post;
				$categories = get_the_category($post->ID);
				if ($categories) {
				$category_ids = array();
				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
				$args=array(
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=> 4, // Number of related posts that will be shown.
				'caller_get_posts'=>1
				);
				$my_query = new wp_query( $args );
				if( $my_query->have_posts() ) {
				echo '<div id="related_posts"><h3 class="h3-color">Related Posts</h3><ol>';
				while( $my_query->have_posts() ) {
				$my_query->the_post();?>
				<li>
				<div class="relatedcontent" id="relatedcontent">
				<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</div>
				</li>
				<?php
				}
				echo '</ol></div>';
				}
				}
				$post = $orig_post;
				wp_reset_query(); ?>
				<?php if(has_tag()): ?>
				 <p><?php the_tags('Tag: ',','); ?></p>
				<?php else: ?>
				<p>Tags: No tags added yet</p>
				<?php endif; ?>

</div>			
   </div>  
       <hr class="para_bottom"> 
<?php
		endwhile; // end of the loop.
		?>
		
		<div class="col-xs-12">
			<?php
			$total_pages = $gloop->max_num_pages;
			$format = get_post_format(); 
			if ($total_pages > 1){
				$current_page = max(1, get_query_var('paged'));
				echo '<div class="page_nav">';
				echo paginate_links(array(
					'base' => get_pagenum_link(1) . '%_%',
					'format' => 'page/%#%',
					'current' => $current_page,
					'total' => $total_pages,
					'prev_text' => 'Prev',
					'next_text' => 'Next'
					));
				echo '</div>';
			}
			?>
		</div>
		<?php
	else:// display other posts in the page
	?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
	<?php 
	endif;
	?>	