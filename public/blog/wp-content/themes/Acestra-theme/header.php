<!DOCTYPE html>
<html>
<head>
  <title>Acestra - Blog</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="description" content="ACESTRA has evolved into a competent software development company providing versatile, scalable, modular solutions."/>
  <meta name="keywords" content="acestranetworks, acestra, Our Expertise, Portfolio, Retail & Ecommerce, Our Products, IT company"/>
  <meta name="author" content="Suthaharan Tharmalingam"/>
   <link rel="icon" href="/blog/wp-content/themes/Acestra-theme/img/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" type="text/css" href="/website/css/custom.css">
   <link rel="stylesheet" href="/blog/wp-content/themes/Acestra-theme/style.css">
<!--    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->

 <!-- start -->
     <link rel="apple-touch-icon" sizes="57x57" href="/websiteV2/img/fav/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/websiteV2/img/fav/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/websiteV2/img/fav/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/websiteV2/img/fav/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/websiteV2/img/fav/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/websiteV2/img/fav/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/websiteV2/img/fav/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/websiteV2/img/fav/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/websiteV2/img/fav/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/websiteV2/img/fav/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/websiteV2/img/fav/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/websiteV2/img/fav/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/websiteV2/img/fav/favicon-16x16.png">
<link rel="manifest" href="/websiteV2/img/fav/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/websiteV2/img/fav/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
    <!--
    Preloader
    =============================================

    -->
    <style>
      body main {
/*
          opacity: 0;
          filter: alpha(opacity=0);
*/
      }

    </style>
    <script type="text/javascript">
      paceOptions = {
          ajax: false,
          document: false,
          restartOnPushState: false,
          restartOnRequestAfter: false
      }
    </script>
    <link href="/websiteV2/assets/lib/pace/themes/blue/pace-theme-center-simple.css" rel="stylesheet">

    <script data-pace-options='{ "elements": { "selectors": [".selector"] }, "startOnPageLoad": false }' src="/websiteV2/assets/lib/pace/pace.min.js"></script>
    <link href="/websiteV2/assets/lib/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <link href="/websiteV2/assets/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/websiteV2/assets/lib/animate-css/animate.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/websiteV2/assets/lib/menuzord/css/menuzord.css">
    <link href="/websiteV2/assets/lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="/websiteV2/assets/lib/owl.carousel.2.1.0/assets/owl.carousel.min.css" rel="stylesheet">
        <link href="/websiteV2/assets/css/particle.css" rel="stylesheet">
<!--     <link href="/websiteV2/assets/css/style.css" rel="stylesheet"> -->
        <link id="color-scheme" href="/websiteV2/assets/css/colors/custom.css" rel="stylesheet">
        <link href="/websiteV2/assets/css/style.css" rel="stylesheet">
         <link href="/websiteV2/css/custom.css" rel="stylesheet">
         <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/cookieconsent2/2.0.0/dark-bottom.css" />
 <!-- end -->
</head>
<body  data-ng-app="mainApp" style="background: none">

<main>

  <div id="menuzord" class="menuzord clearfix navigationbar-animation"><a href="/" class="menuzord-brand"><img src="/websiteV2/img/logo.png" class="default-logo" alt="default-logo"/><img src="/websiteV2/img/logo_thumb1.png" class="dark-logo"  alt="dark-logo"/></a>
        <ul class="menuzord-menu menuzord-right">
          <li><a href="/">Home</a></li>
          <li><a href="/aboutus">About</a></li>
          <li><a href="#">Services</a>
            <ul class="dropdown">
              <li><a href="/services">Our Proficiency</a></li>
              <li><a href="/portfolio">Portfolio</a></li>
            </ul>
          </li>
          <li><a href="/products">Products</a></li>
          <li><a href="/blog/">Blog</a></li>
          <li><a href="#">Contact</a>
            <ul class="dropdown">
                <li><a href="/quote">Quote</a></li>
                <li><a href="/careers">Careers</a></li>
                 <li><a href="/contact">Contact Us</a></li>
            </ul>
          </li>
           <li><a href="#"><i class="ion-android-globe"></i></a>
            <ul class="dropdown">
                <li><a href="/english">English</a></li>
                <li><a href="/french">French</a></li>
                 <li><a href="/arabic">Arabic </a></li>
            </ul>
          </li>
        </ul>
      </div>

 <section class="padding-top-80">
   <div id="particles-js8" >
        <div class="container" id="innerheader2" >
          <div class="row pad-small">
            <div class="col-sm-12 text-center">
              <div class="">
                <h1 class="header-title_new_part_pro header-title_new_part"><span
            class="txt-rotate"
            data-period="2000"
            data-rotate='["Blog"]'></span></h1>
            </div>
              <!-- <div class="inner-title">
                <h3 data-wow-delay="0.1s" class="wow fadeInUp">Blog</h3>
              </div>
              <div data-wow-delay="0.1s" class="inner-breadcrumb wow fadeInUp">
                <ol class="breadcrumb">
                  <li><a href="index.html">Home</a></li>
                  <li>Blog</li>
                </ol>
              </div> -->
            </div>
          </div>
        </div>
      </div>
      </section>
