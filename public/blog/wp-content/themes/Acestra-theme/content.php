<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>
<div class="row">
  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
       <div class="col-lg-2 col-md-2 col-sm-2 text-left">
     	 	    	 <div class="date-wrap">        
			          <span class="date">
			            <?php the_time('j') ?>
			          </span>
			          <span class="month">
			            <?php the_time('M') ?>
			          </span>
			        </div>
     	</div>
     	<div class="col-lg-10 col-md-10 col-sm-10 text-left">
     	 	  		
     	 	  		<?php
			if ( is_single() ) :
				the_title( '<h3 class="upper-text wp_title_left">', '</h3>' );
			else :
				the_title( sprintf( '<h2 class="entry-title wp_h_content wp_h_content1"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' );
			endif;
		?>
     	 </div> 
   </div>
    <div class="row wp_contents_para">
    	 <?php 
    	       $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
				<?php if($feat_image!=""):?>
			      <div class="col-lg-12 wp_content_image">		
				     <img src="<?php echo $feat_image ?>" class="img-responsive wp_img" style="width:100%">
				 </div>
				<?php endif;
	    ?>
	     <?php
			/* translators: %s: Name of current post */
			the_content( sprintf(
				__( 'Continue reading %s', 'twentyfifteen' ),
				the_title( '<span class="screen-reader-text">', '</span>', false )
			) );
            wpb_set_post_views(get_the_ID());
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfifteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
	

	<footer class="entry-footer">		
		<?php edit_post_link( __( 'Edit', 'twentyfifteen' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

				<?php $orig_post = $post;
				global $post;
				$categories = get_the_category($post->ID);
				if ($categories) {
				$category_ids = array();
				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
				$args=array(
				'category__in' => $category_ids,
				'post__not_in' => array($post->ID),
				'posts_per_page'=> 2, // Number of related posts that will be shown.
				'caller_get_posts'=>1
				);
				$my_query = new wp_query( $args );
				if( $my_query->have_posts() ) {
				echo '<div id="related_posts"><h3 class="h3-color">Related Posts</h3><ol>';
				while( $my_query->have_posts() ) {
				$my_query->the_post();?>
				<li>
				<div class="relatedcontent">
				<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</div>
				</li>
				<?php
				}
				echo '</ol></div>';
				}
				}
				$post = $orig_post;
				wp_reset_query(); ?>
				<?php if(has_tag()): ?>
				 <p><?php the_tags('Tag: ',','); ?></p>
				<?php else: ?>
				<p>Tags: No tags added yet</p>
				<?php endif; ?> 
    </div>  	 
       

   
