<?php get_header() ?>
<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$args = array(
				'post_type' 	  => 'post',
				'paged' 		  => $paged,
				'orderby'		  => 'ID',
				'order'			  => 'DESC',
				'post_status' => 'publish'
				);
			$gloop = new WP_Query( $args );
?>

<?php if ( have_posts() ) : ?>
	<?php if (! is_home() && ! is_front_page() ) : ?>
			<!-- 	<header>
					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
				</header> -->
<?php endif; ?>

	 


  <div class="container wp_section">
     <div class="row">
     	<!-- <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 text-left">
       <?php //if(function_exists('bavota_breadcrumbs')) bavota_breadcrumbs(); ?>
        </div> -->
     </div>
     <div class="row">
     	 <div class="col-lg-9 col-md-9 col-sm-8 col-sm-12">
     	 	<div class="row">
     	 		 <div class="col-lg-12 col-xs-12 col-sm-12 col-md-12 menu_bottom menu_top">
		       <!--  <a href="http://www.kollywoodtalkies.com/">
		          <img src="<?php //echo get_option('kollywoodads');?>" alt="Kollywood Ads" class="img-responsive img-center"/>
		         </a> -->
		     </div>
     	 	</div>
              <?php while ( have_posts() ) : the_post(); ?>
              	<div class="wp_contents row blog_post">
			       <div class="row">
	     	 	    <div class="col-lg-2 col-md-2 col-sm-2  col-xs-3 text-left">
	     	 	    	 <div class="date-wrap">
				          <span class="date">
				            <?php the_time('j') ?>
				          </span>
				          <span class="month">
				            <?php the_time('M') ?>
				          </span>
				        </div>
	     	 	    </div>
	     	 	  	<div class="col-lg-10 col-md-10 col-sm-9 col-xs-8">
	     	 	  		<h3 class="upper-text wp_title_left"><a href="<?php echo the_permalink(); ?>"><?php the_title(); ?></a></h3>
	     	 	  	</div>
	     	 	  </div>
     	 	  <div class="row wp_contents_para">
     	 	  	 <?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );?>
				<?php if($feat_image!=""):?>
				 <div class="col-lg-12 wp_content_image">
				   <img src="<?php echo $feat_image;?>" class="img-responsive" style="width:100%">
                 </div>
				<?php endif; ?>
				<?php the_excerpt(); ?>

					<?php $orig_post = $post;
					global $post;
					$categories = get_the_category($post->ID);
					if ($categories) {
					$category_ids = array();
					foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
					$args=array(
					'category__in' => $category_ids,
					'post__not_in' => array($post->ID),
					'posts_per_page'=> 4, // Number of related posts that will be shown.
					'caller_get_posts'=>1
					);
					$my_query = new wp_query( $args );
					if( $my_query->have_posts() ) {
					echo '<div id="related_posts"><h3 class="h3-color">Related Posts</h3><ol>';
					while( $my_query->have_posts() ) {
					$my_query->the_post();?>
					<li>
					<div class="relatedcontent" id="relatedcontent">
					<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
					</div>
					</li>
					<?php
					}
					echo '</ol></div>';
					}
					}
					$post = $orig_post;
					wp_reset_query(); ?>
					<?php if(has_tag()): ?>
					 <p><?php the_tags('Tag: ',','); ?></p>
					<?php else: ?>
					<p>Tags: No tags added yet</p>
					<?php endif; ?>
     	 	  </div>
     	 	  </div>
     	 	 <!--  <hr class="para_bottom">  -->
     	 	  	<?php endwhile; ?>
              <!--pagination -->
              <div class="col-sm-12	text-center pagination_head">
			<?php
			$total_pages = $gloop->max_num_pages;
			$format = get_post_format();
			if ($total_pages > 1){
				$current_page = max(1, get_query_var('paged'));
				echo '<div class="pagination-wrap">';
				echo paginate_links(array(
					'base' => get_pagenum_link(1) . '%_%',
					'format' => 'page/%#%',
					'current' => $current_page,
					'total' => $total_pages,
					'prev_text' => 'Prev',
					'next_text' => 'Next'
					));
				echo '</div>';
			}
			?>
		</div>
			<?php
		// If no content, include the "No posts found" template.
				else :
					get_template_part( 'content', 'none' );
				endif;
				?>

     	 </div>
     	 <div class="col-lg-3 col-md-3 col-sm-4 col-sm-6">
     	 		<?php get_sidebar();?>
     	 </div>
     </div>
</div>

  <?php get_footer() ?>
