<?php
if ( ! function_exists( 'fw_setup' ) ) :
	function fw_setup() {
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'mainpost-thumb', 700, 300 ); // Hard Crop
		add_image_size( 'medium-thumb', 300, 200, true ); // Hard Crop Mode		
		// This theme supports a variety of post formats.
		add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'quote', 'status' ) );		
		add_theme_support( 'automatic-feed-links' );
	}
endif; // tstd_setup
add_action( 'after_setup_theme', 'fw_setup' );	
function wpdocs_excerpt_more( $more ) {
		return sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
			get_permalink( get_the_ID() ),
			__( ' Read More &raquo;', 'textdomain' )
			);
	}
	add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );
	/*update_option( 'siteurl', 'http://acestranetworks.com/blog' );
    update_option( 'home', 'http://acestranetworks.com/blog' );*/

register_sidebar( array(
'name' => 'Blog category wideget',
'id' => 'blogcategory-wideget',
'description' => 'Blog category area',
'before_widget' => '<aside id="%1$s" class=" %2$s">',
'after_widget' => '</aside>',
'before_title' => '<h3 class="widget-title footer_head ">',
'after_title' => '</h3>',
) );

function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function add_class_to_category( $thelist, $separator, $parents){
    $class_to_add = 'btn btn-primary';
    return str_replace('<a href="',  '<a class="'. $class_to_add. '" href="', $thelist);
}


add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {
	$phpmailer->isSMTP();
	$phpmailer->Host       = SMTP_HOST;
	$phpmailer->SMTPAuth   = SMTP_AUTH;
	$phpmailer->Port       = SMTP_PORT;
	$phpmailer->Username   = SMTP_USER;
	$phpmailer->Password   = SMTP_PASS;
	$phpmailer->SMTPSecure = SMTP_SECURE;
	$phpmailer->From       = SMTP_FROM;
	$phpmailer->FromName   = SMTP_NAME;
}

add_filter('the_category', __NAMESPACE__ . '\\add_class_to_category',10,3);

function bavota_breadcrumbs() {
	if(!is_home()) {
		echo '<div class="btn-group btn-breadcrumb marginLeft">';
		echo '<a href="'.home_url('/').'" class="btn btn-primary">Blog</a><span class="divider"></span>';
		if (is_category() || is_single()) {
			the_category(' <span class="divider">/</span> ');
			if (is_single()) {
				//echo ' <span class="divider">/</span> ';
				the_title('<span class="btn btn-primary">','</span>');
			}
		} elseif (is_page()) {
			echo the_title();
		}
		echo '</div>';
	}else{
		echo '<div class="btn-group btn-breadcrumb marginLeft">';
		echo '<a href="'.home_url('/').'" class="btn btn-primary">Blog</a>';
		echo '</div>';
	}
}
?>
<?php
function theme_settings_page()
{
	?>
	<div class="wrap">
		<h1>Theme Panel</h1>
		<form method="post" action="options.php" enctype="multipart/form-data">
			<?php
			settings_fields("section");
			do_settings_sections("theme-options");      
			submit_button(); 
			?>          
		</form>
	</div>
	<?php
}
function add_theme_menu_item()
{
	add_menu_page("Theme Panel", "Theme Panel", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}
add_action("admin_menu", "add_theme_menu_item");
function kalyansevaads_display()
{
	?>
	<input type="file" name="kalyansevaads"  /> 
	<?php echo get_option('kalyansevaads'); ?>
	<?php
}
function acestrarealityads_display()
{
	?>
	<input type="file" name="acestrarealityads"  /> 
	<?php echo get_option('acestrarealityads'); ?>
	<?php
}
function kollywoodads_display()
{
	?>
	<input type="file" name="kollywoodads"  /> 
	<?php echo get_option('kollywoodads'); ?>
	<?php
}
function aceschooldads_display()
{
	?>
	<input type="file" name="aceschooldads"  /> 
	<?php echo get_option('aceschooldads'); ?>
	<?php
}
function display_Addlist()
{

  ?>

    <select id="AdsDrop" name="AdsDrop">
  <option value="volvo">Volvo</option>
  <option value="saab">Saab</option>
  <option value="mercedes">Mercedes</option>
  <option value="audi">Audi</option>
</select>
  <?php   
}
function kalyansevaads_upload()
{
	if(!empty($_FILES["kalyansevaads"]["tmp_name"]))
	{
		$urls = wp_handle_upload($_FILES["kalyansevaads"], array('test_form' => FALSE));
		$temp = $urls["url"];
		return $temp;   
	}
	else if (get_option('kalyansevaads')) {
    # code..
		$option=get_option('kalyansevaads');
		return $option;
	}
}
function acestrarealityads_upload()
{
	if(!empty($_FILES["acestrarealityads"]["tmp_name"]))
	{
		$urls = wp_handle_upload($_FILES["acestrarealityads"], array('test_form' => FALSE));
		$temp = $urls["url"];
		return $temp;   
	}
	else if (get_option('acestrarealityads')) {
    # code..
		$option=get_option('acestrarealityads');
		return $option;
	}
}
function kollywoodads_upload()
{
	if(!empty($_FILES["kollywoodads"]["tmp_name"]))
	{
		$urls = wp_handle_upload($_FILES["kollywoodads"], array('test_form' => FALSE));
		$temp = $urls["url"];
		return $temp;   
	}
	else if (get_option('kollywoodads')) {
    # code..
		$option=get_option('kollywoodads');
		return $option;
	}
}
function aceschooldads_upload()
{
	if(!empty($_FILES["aceschooldads"]["tmp_name"]))
	{
		$urls = wp_handle_upload($_FILES["aceschooldads"], array('test_form' => FALSE));
		$temp = $urls["url"];
		return $temp;   
	}
	else if (get_option('aceschooldads')) {
    # code..
		$option=get_option('aceschooldads');
		return $option;
	}
}
function display_theme_panel_fields()
{
	add_settings_section("section", "All Settings", null, "theme-options");	
	add_settings_field("AdsDrop", "Drop Down Menu", "display_Addlist", "theme-options", "section");
	add_settings_field("kalyansevaads", "KalyanSeva Ads", "kalyansevaads_display", "theme-options", "section");
	add_settings_field("acestrarealityads", "AcestraReality Ads", "acestrarealityads_display", "theme-options", "section");
	add_settings_field("kollywoodads", "kollywood Ads", "kollywoodads_display", "theme-options", "section");
	add_settings_field("aceschooldads", "AceSchool Ads", "aceschooldads_display", "theme-options", "section");
	register_setting("section", "AdsDrop");
	register_setting("section", "kalyansevaads","kalyansevaads_upload");
	register_setting("section", "acestrarealityads","acestrarealityads_upload");
	register_setting("section", "kollywoodads","kollywoodads_upload");
	register_setting("section", "aceschooldads","aceschooldads_upload");		
		get_option('acestrarealityads');
}	


add_action("admin_init", "display_theme_panel_fields");
?>