-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 05, 2016 at 07:15 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `acestra2015`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Brindha', 'nbrindha@acestranetworks.com', '$2y$10$1XEJUQ2Vs2MqGixjp9QhWeFuWs1xfd3MdTs5L1rkxuV2OmILbBQ1C', 0, 'SkIBml9D38eObSE3Us8FwFA7j52rZJzcRpyeHfB4wK1MFg5SBarw8TjG4oIA', '0000-00-00 00:00:00', '2015-09-28 18:35:00', 0, 0),
(2, 'ace', 'ace@admin.com', '$2y$10$H371RP6RlicmyHmSXYvta./0FmhZcajpb.7nKbt8i8RFPbI6gnmJS', 0, 'SkIBml9D38eObSE3Us8FwFA7j52rZJzcRpyeHfB4wK1MFg5SBarw8TjG4oIA', '0000-00-00 00:00:00', '2015-09-28 18:35:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Brindha', 'rthilaga@acestranetworks.com', 'Apply job', 'At W3Schools you will find complete references about tags, attributes, events, color names, entities, character-sets, URL encoding, language codes, HTTP messages, and more.', 1, '2015-09-19 11:04:51', '0000-00-00 00:00:00'),
(2, 'Brindha', 'brin@gmail.co.in', 'Enquiries', 'This Bootstrap tutorial contains hundreds of Bootstrap examples.  With our online editor, you can edit the code, and click on a button to view the result.', 1, '2015-09-19 15:17:56', '0000-00-00 00:00:00'),
(3, 'Brindha', 'mprasath@acestranetworks.com', 'Request job', 'A jumbotron indicates a big box for calling extra attention to some special content or information.  A jumbotron is displayed as a grey box with rounded corners. It also enlarges the font sizes of the text inside it.', 1, '2015-09-19 15:28:56', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Uthaya', 'uday@gmail.com', 'Interview', 'qwwqw', 1, '2016-05-20 11:00:06', NULL),
(2, 'dfg', 'sarulmanickambe@gmail.com', 'dfsgdsfg', 'sdfsdfsdf', 1, '2016-06-17 04:42:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_applies`
--

CREATE TABLE IF NOT EXISTS `job_applies` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cv_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cover_letter` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `job_applies`
--

INSERT INTO `job_applies` (`id`, `name`, `email`, `phone`, `subject`, `position`, `file_path`, `cv_name`, `cover_letter`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Rajan', 'rajan@gmail.com', 9898989989, 'Application for the post of Web Application Developer ', 'web_application_developer', 'uploads/Resume/2016/05', 'Resume94648.doc', 'wqewe', 1, '2016-05-20 10:51:24', NULL),
(2, 'dsfgdfg', 'rthilaga@acestranetworks.com', 9874561230, 'Application for the post of P1401 ', 'P1401', 'uploads/Resume/2016/06', 'Resume38491.docx', 'dfgdfg', 1, '2016-06-17 04:08:24', NULL),
(3, 'thilak', 'ace@admin.com', 9874561230, 'Application for the post of P1401 ', 'P1401', 'uploads/Resume/2016/06', 'Resume11197.docx', 'thilak', 1, '2016-06-17 04:57:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `job_apply`
--

CREATE TABLE IF NOT EXISTS `job_apply` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cv_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cover_letter` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `job_apply`
--

INSERT INTO `job_apply` (`id`, `name`, `email`, `phone`, `subject`, `position`, `file_path`, `cv_name`, `cover_letter`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Brindha', 'nbrindha@acestranetworks.com', 9698585986, 'work with php', 'analyst_programmer', '2015/09', 'resume-DL6CCR.docx', 'Please find enclosed my CV in application for the post advertised in the Guardian on 30 November.', 1, '2015-09-22 16:25:35', '0000-00-00 00:00:00'),
(2, 'Prasath', 'mprasath@acestranetworks.com', 9874563211, 'E School Project', 'analyst_programmer', '2015/09', 'resume-i61WWN.pdf', 'quote document', 1, '2015-09-24 08:48:03', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_09_13_112212_create_admin_table', 1),
('2015_09_19_054101_create_contact_table', 1),
('2015_09_19_054137_create_request_quote_table', 1),
('2015_09_19_054206_create_job_apply_table', 1),
('2015_10_05_151446_create_subscriber_info_table', 2),
('2016_05_06_054613_create_contacts_table', 3),
('2016_05_09_120131_create_quotes_table', 3),
('2016_05_09_125628_create_job_applies_table', 3),
('2016_05_11_093533_create_subscribers_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotes`
--

CREATE TABLE IF NOT EXISTS `quotes` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expected_project_start_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `quotes`
--

INSERT INTO `quotes` (`id`, `name`, `email`, `phone`, `subject`, `expected_project_start_date`, `file_path`, `document_name`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'silver', 'thilak@gmail.com', 9790601088, 'this is msg', '3 Month', 'uploads/document/2016/05', 'document_86006.docx', 'hfgfghfghfgh', 1, '2016-05-14 14:44:41', NULL),
(2, 'Brindha', 'brindha@gmail.com', 9898989899, 'Hello', '1 Month', 'uploads/document/2016/05', 'document_33495.txt', 'qww', 1, '2016-05-20 11:00:50', NULL),
(3, 'ram', 'ram@gmail.com', 123456789, 'sdgkjsdjf', '6 Month', 'uploads/document/2016/06', 'document_16535.docx', '                                               asfasdfs                               ', 1, '2016-06-17 04:35:51', NULL),
(4, 'dfg', 'rthilaga@acestranetworks.com', 0, 'sdfasdfsad', 'Not decided', 'uploads/document/2016/06', 'document_30101.docx', '                                        wergfdsfg            ', 1, '2016-06-17 04:38:08', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_quote`
--

CREATE TABLE IF NOT EXISTS `request_quote` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expected_project_start_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `request_quote`
--

INSERT INTO `request_quote` (`id`, `name`, `email`, `phone`, `subject`, `expected_project_start_date`, `file_path`, `document_name`, `message`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Brindha', 'ace@gmail.com', 9874563211, 'Need to create Cinema News Project', '6months', '', '', 'The cinema of Tamil Nadu is a part of Indian cinema, producing films in the Tamil language. Based in the Kodambakkam district of Chennai, India, The industry is colloquially referred to as Kollywood, the term being a portmanteau of the words Kodambakkam and Hollywood.', 1, '2015-09-22 13:03:11', '0000-00-00 00:00:00'),
(2, 'Prasath', 'mprasath@acestranetworks.com', 9865632168, 'E School Project', '6months', '', '', 'E school management system project report is useful for computer science, bca, mca, mca students for developing this project as mini project. Main objective of this project is to develop a web based software application for schools and colleges for managing various types of students details and helping parents to analyze activities of students through online.', 1, '2015-09-22 13:08:10', '0000-00-00 00:00:00'),
(3, 'Thilaga', 'rthilaga@acestranetworks.com', 9889564655, 'Real Estate Project', '3months', '', '', 'Real estate is "property consisting of land and the buildings on it, along with its natural resources such as crops, minerals, or water; immovable property of this nature; an interest vested in this (also) an item of real property; (more generally) buildings or housing in general. Also: the business of real estate; the profession of buying, selling, or renting land, buildings or housing."', 1, '2015-09-22 13:11:20', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
`id` int(10) unsigned NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `subscribers`
--

INSERT INTO `subscribers` (`id`, `email_id`, `created_at`, `updated_at`) VALUES
(1, 'aceazb@gmail.com', '2016-05-20 11:04:12', NULL),
(2, 'uday@gmail.com', '2016-05-20 11:17:12', NULL),
(3, 'uyg', '2016-06-14 17:18:59', NULL),
(4, 'dfgdsfg', '2016-06-17 06:18:00', NULL),
(5, 'sdfgdsfg', '2016-06-17 06:19:17', NULL),
(6, 'ace@gmail.com', '2016-06-17 06:19:36', NULL),
(7, 'ace@gmail.coma', '2016-06-17 06:21:28', NULL),
(8, 'ace@gmail.comd', '2016-06-17 06:22:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subscriber_info`
--

CREATE TABLE IF NOT EXISTS `subscriber_info` (
`id` int(10) unsigned NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `subscriber_info`
--

INSERT INTO `subscriber_info` (`id`, `email_id`, `created_at`, `updated_at`) VALUES
(1, 'nbrindha@acestranetworks.com', '2015-10-14 23:36:32', '0000-00-00 00:00:00'),
(2, 'sarveshteki@gmail.com', '2015-10-16 22:07:51', '0000-00-00 00:00:00'),
(3, 'brindhavijayan34@gmail.com', '2015-10-16 22:14:46', '0000-00-00 00:00:00'),
(4, 'brin@gmail.com', '2015-10-28 15:00:53', '0000-00-00 00:00:00'),
(5, 'brindha@gmail.com', '2015-12-28 17:21:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ace', 'ace@gmail.com', '$2y$10$go6kw.TcR9EAfgwuictflebnbgsivaRx1biPpyDwl7w2daFXsRzn2', 'Ha2QvCEJg5Yle2DZMns2an6hp923ilhmk5W4KkHEv5w5YHTqCTW9dCT5BnO3', '2016-05-14 14:55:10', '2016-05-20 11:29:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `admin_email_unique` (`email`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_applies`
--
ALTER TABLE `job_applies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_apply`
--
ALTER TABLE `job_apply`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `quotes`
--
ALTER TABLE `quotes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_quote`
--
ALTER TABLE `request_quote`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscriber_info`
--
ALTER TABLE `subscriber_info`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `subscriber_info_email_id_unique` (`email_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `job_applies`
--
ALTER TABLE `job_applies`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `job_apply`
--
ALTER TABLE `job_apply`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `quotes`
--
ALTER TABLE `quotes`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `request_quote`
--
ALTER TABLE `request_quote`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `subscriber_info`
--
ALTER TABLE `subscriber_info`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
