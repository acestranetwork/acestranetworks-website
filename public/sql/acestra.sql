-- MySQL dump 10.13  Distrib 5.5.49, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: anetwork_in
-- ------------------------------------------------------
-- Server version	5.5.49-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'Brindha','nbrindha@acestranetworks.com','$2y$10$1XEJUQ2Vs2MqGixjp9QhWeFuWs1xfd3MdTs5L1rkxuV2OmILbBQ1C',0,'SkIBml9D38eObSE3Us8FwFA7j52rZJzcRpyeHfB4wK1MFg5SBarw8TjG4oIA','0000-00-00 00:00:00','2015-09-28 18:35:00',0,0),(2,'ace','ace@admin.com','$2y$10$H371RP6RlicmyHmSXYvta./0FmhZcajpb.7nKbt8i8RFPbI6gnmJS',0,'SkIBml9D38eObSE3Us8FwFA7j52rZJzcRpyeHfB4wK1MFg5SBarw8TjG4oIA','0000-00-00 00:00:00','2015-09-28 18:35:00',0,0);
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES (1,'Brindha','rthilaga@acestranetworks.com','Apply job','At W3Schools you will find complete references about tags, attributes, events, color names, entities, character-sets, URL encoding, language codes, HTTP messages, and more.',1,'2015-09-19 11:04:51','0000-00-00 00:00:00'),(2,'Brindha','brin@gmail.co.in','Enquiries','This Bootstrap tutorial contains hundreds of Bootstrap examples.  With our online editor, you can edit the code, and click on a button to view the result.',1,'2015-09-19 15:17:56','0000-00-00 00:00:00'),(3,'Brindha','mprasath@acestranetworks.com','Request job','A jumbotron indicates a big box for calling extra attention to some special content or information.  A jumbotron is displayed as a grey box with rounded corners. It also enlarges the font sizes of the text inside it.',1,'2015-09-19 15:28:56','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,'Uthaya','uday@gmail.com','Interview','qwwqw',1,'2016-05-20 11:00:06',NULL),(2,'dfg','sarulmanickambe@gmail.com','dfsgdsfg','sdfsdfsdf',1,'2016-06-17 04:42:23',NULL),(0,'Ram Keerthana','c.ramkeerthana@gmail.com','Applying for job','If any openings for freshers let me know immediately. ',1,'2016-08-14 02:52:36',NULL);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_applies`
--

DROP TABLE IF EXISTS `job_applies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_applies` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cv_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cover_letter` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_applies`
--

LOCK TABLES `job_applies` WRITE;
/*!40000 ALTER TABLE `job_applies` DISABLE KEYS */;
INSERT INTO `job_applies` VALUES (1,'Rajan','rajan@gmail.com',9898989989,'Application for the post of Web Application Developer ','web_application_developer','uploads/Resume/2016/05','Resume94648.doc','wqewe',1,'2016-05-20 10:51:24',NULL),(2,'dsfgdfg','rthilaga@acestranetworks.com',9874561230,'Application for the post of P1401 ','P1401','uploads/Resume/2016/06','Resume38491.docx','dfgdfg',1,'2016-06-17 04:08:24',NULL),(3,'thilak','ace@admin.com',9874561230,'Application for the post of P1401 ','P1401','uploads/Resume/2016/06','Resume11197.docx','thilak',1,'2016-06-17 04:57:43',NULL),(0,'Uthaya','ace@admin.com',8898574575,'Application for the post of P1401 ','P1401','uploads/Resume/2016/07','Resume88052.doc','I am willing to work in your company',1,'2016-07-13 23:26:19',NULL),(0,'Parthiban','parthibankm92@gmail.com',7418634320,'Application for the post of P1402 ','P1402','uploads/Resume/2016/07','Resume93433.doc','Hi \r\nAm parthiban from Madurai now currently stay in Chennai.\r\n\r\nI have 2 and half years of experience in Accenture bpo.\r\n\r\nI have 6 month experience in PHP developer\r\n\r\nI have good knowledge in PHP HTML CSS JavaScript MySQL jquery',1,'2016-07-21 10:27:19',NULL),(0,'chandrasekaran','chandrubala20@gmail.com',9698765423,'Application for the post of P1402 ','P1402','uploads/Resume/2016/07','Resume45450.pdf','Dear sir, \r\nPlease find the attached resume',1,'2016-07-21 22:20:02',NULL),(0,'Anandan k','anandancse1@gmail.com',8524926631,'PHP Developer','P1402','uploads/Resume/2016/07','Resume89504.docx','K.ANANDAN						\r\n71, Kamarajar Street,\r\nVengamedu,\r\nKarur.\r\nPincode: 639006.\r\nTamilNadu,\r\n India.\r\nE-mail: anandancse1@gmail.com					\r\nPhone	: +91-8524926631\r\n\r\nOBJECTIVE\r\n\r\nTo get placed in an organization where I could bring out my talents to the best and to contribute to its success through my enhanced technical and managerial skills, in a competitive and dynamic environment.\r\n\r\nEDUCATIONAL QUALIFICATION\r\n\r\nCourse	Institution	University/\r\nBoard	Year of passing	Percentage / CGPA\r\nB.E Computer Science	M.P.Nachimuthu M.Jaganathan Engineering College, Erode.	Anna University	2016	61%\r\n\r\nH.Sc.,	Valallar Hr.Sec  School,\r\nKarur	State Board	2012	70.25%\r\nS.S.L.C	Valallar  Hr.Sec School, Karur	State Board	2010	75%\r\n\r\n\r\nFIELD OF INTEREST\r\n	Software Developer\r\n	Computer Network\r\n	Software Testing\r\n\r\nCOMPUTER COMPETENCY\r\nProgramming Languages		: 	C, C++, Java\r\nScripting  Language                     :           Html, PHP, CSS\r\n           MS Office                                   	: 	Word, Excel, Powerpoint.\r\n \r\n                                                                                                                         PROJECT:\r\n\r\nTitle                     	:    	Energy Efficient And Key Based Approach  For   \r\n                                             Forwarding Aggregated Data In Wireless Sensor  \r\n                                             Networks\r\nPlatform        		: 	DotNet\r\nDomain                     :          Network Security\r\nDescription      	:	Using wireless sensor  to avoiding the missing files   \r\n                                            while the processing of collusion attacks  To  send the    \r\n                                            data securely\r\n\r\nEXTRA-CURRICULAR  STUDIES\r\n\r\n	Master Of Web And Graphics Designings by  Adoro Institute Of Multimedia In Coimbatore\r\n\r\n	PHP by Adoro Institute Of Multimedia In Coimbatore\r\n\r\n\r\nCO-CURRICULAR ACTIVITIES\r\n	Participated  the Two Days Workshop On \"JAVA\" in HCL Career Development Centre, Chennai.\r\n	Participated the Seminar on \"NETWORKING AND SECURITY\" on Avatar Academy.\r\n\r\n\r\nHOBBIES\r\n	Sports\r\n	Reading  books\r\n\r\nPERSONAL DETAILS\r\nName                		:  	K.Anandan\r\nFather Name     		:  	A.Krishnan\r\nDate Of Birth     		:  	05-01-1995\r\nGender	                     :  	Male\r\nLanguage Known		:	Tamil & English\r\nMarital Status    		:  	Single\r\nNationality &Religion  	: 	Indian &Hindu\r\nPassport  Number              :          N3390304\r\n\r\nDECLARATION \r\nI hereby declare that all the details furnished above are true to the best of my knowledge Should you be pleased to select me,I shall endeavor to offer you satisfactory service.\r\n										                                     Yours Faithfully,\r\nPlace: \r\nDate:  \r\n                                                                                                                (K.ANANDAN)\r\n',1,'2016-07-24 20:16:37',NULL),(0,'Shankar','Nameshankaran@gmail.com',9688884032,'Application for the post of P1402 ','P1402','uploads/Resume/2016/07','Resume88600.doc','Sir, I\'m interested in php',1,'2016-07-26 01:28:48',NULL),(0,'T.Bharathan','bharathan.it.12@petengg.ac.in',9025465425,'Web Developer','P1402','uploads/Resume/2016/07','Resume76844.pdf','Hi Sir/Madam,\r\n\r\nI am searching job for Software Company(UI Developer) and Fresher.\r\n\r\nThanks,\r\nT.Bharathan',1,'2016-07-26 01:38:15',NULL),(0,'B Karthik Kumar','bkkumar1994@gmail.com',9717610597,'Application for the post of P1402 ','P1402','uploads/Resume/2016/07','Resume48151.docx','I would like to request you to consider me as you may  have a lot of options for this position to select. But I don\'t know about the rest. What I believe I meet the minimum requirement for this position, but I able to my job for its requirement. And apart from this my words cannot justify my action until I don\'t get the right opportunities to perform. I will be very privilege if you avail me this opportunity.Being a fresher I need a good platform to enhance my skills and to get the positive identity in the team, if you give me the opportunity then I am ready to give my 100% best. \r\n\r\nI am pursuing a course in Bachelor Of Technology in Computer Science and Engineering (final year) from Gurgaon Institute Of Technology & Management  and I would like to mention you that I have  done my summer internship with BILT (Ballarpur Industries Limited) for 3 months  and for 6 months at MERCER. It was a great experience working in a team.\r\n\r\nI strongly feel that  training with you would provide me the best exposure in the industry and it would provide me an opportunity to prove my mettle and help me to improve.',1,'2016-07-27 08:49:53',NULL),(0,'Angamuthu','vnsmuthu@gmail.com',9952280202,'Application for the post of P1402 ','P1402','uploads/Resume/2016/07','Resume40483.pdf','hi i am angamuthu attached my resume please refer and update me',1,'2016-07-28 01:07:09',NULL),(0,'R.Manoraj','smilemano23@gmail.com',9943827872,'Application for the post of P1402 ','P1402','uploads/Resume/2016/07','Resume72356.docx','Respected sir/madam,\r\nI have completed MCA with 8.5 CGPA. I have send my resume for your kind reference. I have knowledge in css, html and php.',1,'2016-07-28 04:36:38',NULL),(0,'Gayathri Marimuthu','mgayathrimarimuthu@gmail.com',9659914587,'Application for the post of P1402 ','P1402','uploads/Resume/2016/07','Resume76522.doc','28/07/2016\r\n\r\nHi,\r\n\r\nWith a proven track record of successful achievements, I am pleased to present my application and CV for your consideration as a Web Developer.\r\n\r\nHaving one year 3 months experience in the industry, and a strong educational background featuring a degree in Computer Science, I strongly believe I can make a significant contribution to First Media Web Developers.\r\n\r\nMy web developing expertise includes:\r\n\r\nDesigning, building and maintaining ecommerce websites.\r\nStrong database knowledge specifically MySQL.\r\nExtensive knowledge of  JavaScript, CSS and (X) HTML.\r\nI possess good communication skills and can liaise effectively with both clients and work colleagues. Other strong points include an ability to work as part of a team or individually, multi-task, prioritize and work to deadlines under pressure.\r\n\r\nTherefore in view of all of the above I would be highly gratified if you consider me for this important position.\r\n\r\nI am available to come in for an interview at any time, and would appreciate the opportunity to meet with you and your staff. I hope that I am granted an opportunity to talk and perhaps meet with you in the very near future.\r\n\r\nHope to hear from you soon.\r\n\r\nThanks &Regards\r\n<Gayathri.m>',1,'2016-07-28 06:00:21',NULL),(0,'dinesh','dineshmail.kannan@gmail.com',9942080003,'Application for the post of P1402 ','P1402','uploads/Resume/2016/08','Resume19733.docx','dinesh',1,'2016-08-03 08:39:44',NULL),(0,'GURUPRASANTH V','guruprasanth86@gmail.com',8807703167,'Application for the post of P1402 ','P1402','uploads/Resume/2016/08','Resume70788.pdf','Dear sir/madam,\r\n\r\nI am V. Guruprasanth completed B.E Computer science and Engineering in J.J\r\nCollege of Engineering and Technology Trichy with an aggregate of 65%.I am currently\r\nlooking for full time permanent positions with esteemed organizations like yours.\r\n\r\nAdapting to various work cultures, identifying and resolving bottlenecks and\r\nprioritizing have been striking features of my working style. In addition to the above, I\r\nstrongly believe in learning and sharing my knowledge with my colleagues. “Knowledge\r\nparted is knowledge gained” has been my motto in life. I am keen to learn and am sharp\r\nat identifying and solving problems.\r\n\r\nHere by i attached a copy of my resume for your kind\r\nreference.\r\n\r\nExpecting a Favorable Reply,\r\nSincerely,\r\nV. Guruprasanth\r\n',1,'2016-08-03 09:04:41',NULL),(0,'Umamaheswari.M','uma.kcy@gmail.com',7708636674,'Application for the post of P1402 ','P1402','uploads/Resume/2016/08','Resume37708.docx','BE Fresher seeking job ',1,'2016-08-04 07:02:05',NULL),(0,'Uthayak kumar','uthayafriends@gmail.com',9791963315,'Application for the post of P1401 ','P1401','uploads/Resume/2016/08','Resume29314.doc','I would like to join your company',1,'2016-08-08 01:58:47',NULL),(0,'KATHIRESAN','S',8680036679,'Application for the post of P1402 ','P1402','uploads/Resume/2016/08','Resume56050.docx','Sub : Job Application',1,'2016-08-15 22:58:15',NULL),(0,'PRITHIVIRAJKUMAR T','tpritivimca@gmail.com',9003312529,'Application for the post of P1402 ','P1402','uploads/Resume/2016/08','Resume65123.pdf','I am looking for the php developer at your company and I am skilled with PHP,  css,  Photoshop.  ',1,'2016-08-16 02:29:33',NULL),(0,'PRIYADHARSHINI G','gpriya2770@gmail.com',8754237179,'Application for the post of P1402 ','P1402','uploads/Resume/2016/08','Resume97854.doc','This is Priyadharshini.G I am from hosur, I did my college M.C.A at Adhiyamaan College of Engineering in Hosur (Autonomous), which is afflicted by Anna university Chennai and I was doing UG B.C.A at M.G.R college in hosur afflicted by Periyar university in salem.',1,'2016-08-17 05:14:12',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume92255.docx','dfgdfg',1,'2016-09-01 03:59:01',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume18225.docx','dfhmkglm',1,'2016-09-01 04:03:46',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume91789.docx','dfhmkglm',1,'2016-09-01 04:04:43',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume68991.docx','dfhmkglm',1,'2016-09-01 04:04:58',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume73528.docx','dfhdfh',1,'2016-09-01 04:06:34',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume31933.docx','sdfgsd',1,'2016-09-01 04:27:26',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume33112.docx','sdfasd',1,'2016-09-01 04:47:44',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume89200.docx','dfhgdfgh',1,'2016-09-01 04:55:15',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume36671.docx','dfhd',1,'2016-09-01 04:57:02',NULL),(0,'thilak','rthilaga@acestranetworks.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume74625.docx','thilak',1,'2016-09-01 04:58:09',NULL),(0,'raja','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume86752.docx','dfhgdfg',1,'2016-09-01 05:00:20',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1401 ','P1401','uploads/Resume/2016/09','Resume40960.docx','fghdfgh',1,'2016-09-01 05:08:48',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1402 ','P1402','uploads/Resume/2016/09','Resume49886.docx','sdfgsd',1,'2016-09-01 05:13:54',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1405 ','P1405','uploads/Resume/2017/01','Resume48393.docx','thilak',1,'2017-01-28 01:25:19',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1405 ','P1405','uploads/Resume/2017/01','Resume41490.docx','thilak',1,'2017-01-28 01:33:09',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1405 ','P1405','uploads/Resume/2017/01','Resume95728.docx','thilak',1,'2017-01-28 01:33:33',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1405 ','P1405','uploads/Resume/2017/01','Resume84397.docx','thilak',1,'2017-01-28 01:36:31',NULL),(0,'thilak','thilakazb@gmail.com',9874561230,'Application for the post of P1405 ','P1405','uploads/Resume/2017/01','Resume52930.docx','sdfg',1,'2017-01-28 03:08:45',NULL),(0,'thilak','thilakazb@gmail.com',9790601088,'Application for the post of P1405 ','P1405','uploads/Resume/2017/01','Resume94963.docx','fgh',1,'2017-01-28 03:31:27',NULL);
/*!40000 ALTER TABLE `job_applies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `job_apply`
--

DROP TABLE IF EXISTS `job_apply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `job_apply` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cv_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cover_letter` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `job_apply`
--

LOCK TABLES `job_apply` WRITE;
/*!40000 ALTER TABLE `job_apply` DISABLE KEYS */;
INSERT INTO `job_apply` VALUES (1,'Brindha','nbrindha@acestranetworks.com',9698585986,'work with php','analyst_programmer','2015/09','resume-DL6CCR.docx','Please find enclosed my CV in application for the post advertised in the Guardian on 30 November.',1,'2015-09-22 16:25:35','0000-00-00 00:00:00'),(2,'Prasath','mprasath@acestranetworks.com',9874563211,'E School Project','analyst_programmer','2015/09','resume-i61WWN.pdf','quote document',1,'2015-09-24 08:48:03','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `job_apply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_09_13_112212_create_admin_table',1),('2015_09_19_054101_create_contact_table',1),('2015_09_19_054137_create_request_quote_table',1),('2015_09_19_054206_create_job_apply_table',1),('2015_10_05_151446_create_subscriber_info_table',2),('2016_05_06_054613_create_contacts_table',3),('2016_05_09_120131_create_quotes_table',3),('2016_05_09_125628_create_job_applies_table',3),('2016_05_11_093533_create_subscribers_table',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotes`
--

DROP TABLE IF EXISTS `quotes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotes` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expected_project_start_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotes`
--

LOCK TABLES `quotes` WRITE;
/*!40000 ALTER TABLE `quotes` DISABLE KEYS */;
INSERT INTO `quotes` VALUES (1,'silver','thilak@gmail.com',9790601088,'this is msg','3 Month','uploads/document/2016/05','document_86006.docx','hfgfghfghfgh',1,'2016-05-14 14:44:41',NULL),(2,'Brindha','brindha@gmail.com',9898989899,'Hello','1 Month','uploads/document/2016/05','document_33495.txt','qww',1,'2016-05-20 11:00:50',NULL),(3,'ram','ram@gmail.com',123456789,'sdgkjsdjf','6 Month','uploads/document/2016/06','document_16535.docx','                                               asfasdfs                               ',1,'2016-06-17 04:35:51',NULL),(4,'dfg','rthilaga@acestranetworks.com',0,'sdfasdfsad','Not decided','uploads/document/2016/06','document_30101.docx','                                        wergfdsfg            ',1,'2016-06-17 04:38:08',NULL),(0,'thilak','thilakazb@gmail.com',9874561230,'dsfhg','1 Month','uploads/document/2016/07','document_28245.docx','sdafsd',1,'2016-07-06 08:48:53',NULL);
/*!40000 ALTER TABLE `quotes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_quote`
--

DROP TABLE IF EXISTS `request_quote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `request_quote` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` bigint(20) unsigned NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `expected_project_start_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `document_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_quote`
--

LOCK TABLES `request_quote` WRITE;
/*!40000 ALTER TABLE `request_quote` DISABLE KEYS */;
INSERT INTO `request_quote` VALUES (1,'Brindha','ace@gmail.com',9874563211,'Need to create Cinema News Project','6months','','','The cinema of Tamil Nadu is a part of Indian cinema, producing films in the Tamil language. Based in the Kodambakkam district of Chennai, India, The industry is colloquially referred to as Kollywood, the term being a portmanteau of the words Kodambakkam and Hollywood.',1,'2015-09-22 13:03:11','0000-00-00 00:00:00'),(2,'Prasath','mprasath@acestranetworks.com',9865632168,'E School Project','6months','','','E school management system project report is useful for computer science, bca, mca, mca students for developing this project as mini project. Main objective of this project is to develop a web based software application for schools and colleges for managing various types of students details and helping parents to analyze activities of students through online.',1,'2015-09-22 13:08:10','0000-00-00 00:00:00'),(3,'Thilaga','rthilaga@acestranetworks.com',9889564655,'Real Estate Project','3months','','','Real estate is \"property consisting of land and the buildings on it, along with its natural resources such as crops, minerals, or water; immovable property of this nature; an interest vested in this (also) an item of real property; (more generally) buildings or housing in general. Also: the business of real estate; the profession of buying, selling, or renting land, buildings or housing.\"',1,'2015-09-22 13:11:20','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `request_quote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscriber_info`
--

DROP TABLE IF EXISTS `subscriber_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscriber_info` (
  `id` int(10) unsigned NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscriber_info`
--

LOCK TABLES `subscriber_info` WRITE;
/*!40000 ALTER TABLE `subscriber_info` DISABLE KEYS */;
INSERT INTO `subscriber_info` VALUES (1,'nbrindha@acestranetworks.com','2015-10-14 23:36:32','0000-00-00 00:00:00'),(2,'sarveshteki@gmail.com','2015-10-16 22:07:51','0000-00-00 00:00:00'),(3,'brindhavijayan34@gmail.com','2015-10-16 22:14:46','0000-00-00 00:00:00'),(4,'brin@gmail.com','2015-10-28 15:00:53','0000-00-00 00:00:00'),(5,'brindha@gmail.com','2015-12-28 17:21:24','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `subscriber_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subscribers`
--

DROP TABLE IF EXISTS `subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subscribers` (
  `id` int(10) unsigned NOT NULL,
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subscribers`
--

LOCK TABLES `subscribers` WRITE;
/*!40000 ALTER TABLE `subscribers` DISABLE KEYS */;
INSERT INTO `subscribers` VALUES (1,'aceazb@gmail.com','2016-05-20 11:04:12',NULL),(2,'uday@gmail.com','2016-05-20 11:17:12',NULL),(3,'uyg','2016-06-14 17:18:59',NULL),(4,'dfgdsfg','2016-06-17 06:18:00',NULL),(5,'sdfgdsfg','2016-06-17 06:19:17',NULL),(6,'ace@gmail.com','2016-06-17 06:19:36',NULL),(7,'ace@gmail.coma','2016-06-17 06:21:28',NULL),(8,'ace@gmail.comd','2016-06-17 06:22:15',NULL),(0,'rthilaga@acestranetworks.com','2016-07-19 00:47:28',NULL),(0,'thilakazb@gmail.com','2016-07-19 01:07:41',NULL),(0,'mr.thilagaraja@gmail.com','2016-07-19 01:09:09',NULL),(0,'mljm280192@gmail.com','2016-07-19 01:13:00',NULL),(0,'cjegadeesh@acestranetworks.com','2016-07-19 04:33:17',NULL),(0,'cnjegadeesh1991@gmail.com','2016-07-19 04:34:38',NULL),(0,'enihitha@acestranetworks.com','2016-07-19 04:38:30',NULL),(0,'ssaravanan@acestranetwors.com','2016-07-19 04:48:39',NULL),(0,'ssaravanan@acestranetworks.com','2016-07-19 04:50:13',NULL),(0,'9941777735','2016-07-21 23:54:29',NULL),(0,'c.ramkeerthana@gmail.com','2016-08-14 02:50:46',NULL);
/*!40000 ALTER TABLE `subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ace','ace@gmail.com','$2y$10$go6kw.TcR9EAfgwuictflebnbgsivaRx1biPpyDwl7w2daFXsRzn2','Ha2QvCEJg5Yle2DZMns2an6hp923ilhmk5W4KkHEv5w5YHTqCTW9dCT5BnO3','2016-05-14 14:55:10','2016-05-20 11:29:12');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-01-28 14:46:09
