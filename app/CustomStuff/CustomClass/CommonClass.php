<?php namespace App\CustomStuff\CustomClass;
use Illuminate\Support\Facades\Session;
use DB;
use Auth;

class CommonClass
{

	public static function CommonDate()
	{
		return date("Y-m-d H:i:s");			//get current Date
	}

	public static function getSessionId()
	{
		return Session::getId();	//get session id for current user
	}

	public static function alphanumericValue()
	{
		$length = 6;
        $characters = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $alphanumeric_val = '';
        for ($i = 0; $i < $length; $i++) {
            $alphanumeric_val .= $characters[rand(0, $charactersLength - 1)];
        }
        return $alphanumeric_val;

		//$alphanumeric_val=substr( "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" ,mt_rand( 0 ,50 ) ,1 ) .substr( md5( time() ), 1);
		//return $alphanumeric_val;
	}

	public static function unlink($image_old_path_name)
	{
		if(unlink($image_old_path_name))
			echo "deleted";
		else
			echo "not deleted";

	}

  public static function getLoggedUserName($current_user_id)
  {
    $current_user_name=DB::table('admin')
              ->where('id','=',$current_user_id)
              ->first();

    return $current_user_name->name;
  }
}		

