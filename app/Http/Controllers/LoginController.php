<?php namespace App\Http\Controllers;

use Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Auth;
use App\CustomClass\CommonClass;


class LoginController extends Controller
{
    public function userLogin() {
        return view('auth.login');
    }
    public function userLoginPost() {

        $rules = array('email'                 =>'required|min:3|max:100',
                        'password'              => 'required|min:3|alpha_dash|alpha_num');
                        
        $validator = Validator::make( Input::all(), $rules);
   
        if($validator->fails())
        {
        return redirect('/userLogin')->withErrors($validator)->withInput();
        }
        else
        {
            $input = Input::all();
            if(count($input) > 0){
                $auth = auth()->guard('users');
                $credentials = [
                'email' =>  $input['email'],
                'password' =>  $input['password'],
                ];

                if ($auth->attempt($credentials)) {
                    return redirect('web');
                } else {
                      return redirect()->back()->withInput()->withErrors(['error_message' => "The User Credentials presented are not valid."]);
                }
            } else {
                return redirect('home');
            }
        }
    }
        public function adminLoginPost(Request $request) {
           if($request->email == 'ace@admin.com'){
               $pass = 'required';
            }else{
              $pass= 'required|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#?!@$%^&*-])[A-Za-z\d$@$!%*#?&]{8,20}$/';
            }

        $rules = array('email'     =>'required|min:3|max:100',
                        'password' => $pass
                        );
                        
        $validator = Validator::make( $request->all(), $rules);
   
        if($validator->fails())
        {
        return redirect()->back()
                    ->withErrors($validator)
                    ->withInput();
        }
        else
        {
            $input = $request->all();
            if(count($input) > 0){

                // $auth = auth()->guard('admin');
                $credentials = [
                'email' =>  $input['email'],
                'password' =>  $input['password'],
                ];

                  if (Auth::attempt($credentials)) {
                    
                    return redirect('/dashboard');
                    
                } else {
                      return redirect()->back()
                                ->withInput()
                                ->withErrors([
                                    'error_message' => "Email or Password is Invalid",
                                ]);
                }
            } else {
                return redirect('home');
            }
        }
    }
    // public function userRegister() {
    //     return view('auth.register');
    // }
    // public function userRegisterPost() {

    //     $rules = array('name'                   => 'required',  
    //                     'email'                 =>'required|min:3|max:100',
    //                     'password'              => 'required|min:3|alpha_dash|alpha_num|confirmed',
    //                     'password_confirmation' => 'same:password');
  
    //     $validator = Validator::make( Input::all(), $rules);
   
    //     if($validator->fails())
    //     {
    //     return redirect('/userRegister')
    //                 ->withErrors($validator)
    //                 ->withInput();
    //     }
    //     $name = Input::get('name');
    //     $email = Input::get('email');
    //     $password = Input::get('password');
    //     $password = bcrypt($password);

    //     $user = new User;
    //     $user->name = $name;
    //     $user->email = $email;
    //     $user->password = $password;
    //     $user->save();

    //     return Redirect::back();
    // }
    // public function userLogout (){
    //        $auth = auth()->guard('users');
    //        $auth->logout();
    //        return Redirect::to('/userLogin');  
    // }

    public function adminRegister() {
        return view('admin.register');
    }
    public function adminRegisterPost(Request $request) {

        $rules = array('name'                   => 'required',  
                        'email'                 =>'required|min:3|max:100',
                        'password'              => 'required|min:3|alpha_dash|alpha_num|confirmed',
                        'password_confirmation' => 'same:password');
  
        $validator = Validator::make( $request->all(), $rules);
   
        if($validator->fails())
        {
        return redirect('/adminRegister')
                    ->withErrors($validator)
                    ->withInput();
        }
        $current_datetime= CommonClass::current_date();

        $name = $request->input('name');
        $email = $request->input('email');
        $password = bcrypt($request->input('password'));

        $User = new User;
        $User->name = $name;
        $User->email = $email;
        $User->password = $password;
        $User->created_at = $current_datetime;
        $User->save();

        return redirect('/adminLogin');
    }

    public function adminLogin() 
    {
        return view('admin.login');
    }

    public function adminLogout() {
           Auth::logout();
           return redirect('/adminlogin');  
    }
}
