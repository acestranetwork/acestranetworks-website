<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DateTime;
use Input;
use App\Http\Requests;
use App\Models\Contact;
use App\Models\quote;
use App\Models\JobApply;
use App\Models\Subscriber;
use File;
use DB;
use Mail;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function captcha()
    {
        session_start();
        $code=rand(10000,99999);
        $_SESSION["code"]=$code;
        $im = imagecreatetruecolor(55, 24);
        $bg = imagecolorallocate($im, 22, 86, 165); //background color blue
        $fg = imagecolorallocate($im, 255, 255, 255);//text color white
        imagefill($im, 0, 0, $bg);
        imagestring($im, 5, 5, 5,  $code, $fg);
        header("Cache-Control: no-cache, must-revalidate");
        header('Content-type: image/png');
        imagepng($im);
        imagedestroy($im);
    }
    public function contact_post(Request $request)
    {
        $rule = array('name'    => 'required',
            'email'    => 'required',
            'subject'    => 'required',
            'message'    => 'required',
            'captcha' => 'required'); 

        $messages = array('email.required' => 'Please fill your email Address.',
                        'subject.required' => 'Please enter your subject.',
                        'captcha.required' => 'Please enter the Captcha',
                        'message.required' => 'Please enter your message');  
        
        $validation = Validator::make($request->all(), $rule,$messages);

        if ( $validation->fails() )
        {
            return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        else
        {
            session_start();
            if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&$_SESSION["code"]==$_POST["captcha"])
            {
                $now = new DateTime();
                $Contact = new Contact();   //contact=model
                $Contact->name=$request->input('name');
                $Contact->email=$request->input('email');
                $Contact->subject=$request->input('subject');
                $Contact->message=$request->input('message');
                $Contact->status=1;
                $Contact->created_at=$now;
                $Contact->save();
            }
             else{
                Session::flash('alert', 'Please enter the correct captcha'); 
                return redirect()->back()->withInput();
            }
            if(isset($Contact->id))
            {
                Session::flash('success', 'Successfully stored in database');
                return redirect()->back();
            }
        }
}

   public function quote_post(Request $request)
    {
        $rule = array('name'    => 'required',
            'email'    => 'required', 
            'phone'    => 'required',
            'subject'    => 'required',
            'message'    => 'required',
             'expected_project_start_date'    => 'required',
             'document_name'=>'required'); 

        $messages = array('name.required' => 'Please enter your name.',
                        'email.required' => 'Please fill your email Address.',
                        'phone.required' => 'Please enter your phone number.',
                        'subject.required' => 'Please enter your subject.',
                        'message.required' => 'Please enter your message.',
                        'expected_project_start_date.required' => 'Please select project estimation time.',
                        'document_name.required' => 'Please upload your project file');  
        
        $validation = Validator::make($request->all(), $rule,$messages);

        if ( $validation->fails() )
        {
            return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        else
        {
            session_start();
            if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&$_SESSION["code"]==$_POST["captcha"])
            {
                $now = new DateTime();
                $Quote = new Quote();   //contact=model

                $resume = $request->file('document_name');

                $current_year=date("Y");
                $current_month=date("m");
                $destinationPath='uploads/document/'.$current_year.'/'.$current_month;
                if (!file_exists($destinationPath))
                {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $extension = $resume->getClientOriginalExtension(); // getting image extension
                $random_number = mt_rand(10000, 99999);
                $fileName='document_'.$random_number.'.'.$extension; // renameing image
                $resume->move($destinationPath,$fileName);

                $Quote->name=$request->input('name');
                $Quote->email=$request->input('email');
                $Quote->phone=$request->input('phone');
                $Quote->subject=$request->input('subject');
                $Quote->expected_project_start_date=$request->input('expected_project_start_date');
                $Quote->message=$request->input('message');
                $Quote->file_path=$destinationPath;
                $Quote->document_name=$fileName;
                $Quote->status=1;

                $Quote->created_at=$now;
                $Quote->save();
            }
             else{
                Session::flash('alert', 'Please enter the correct captcha'); 
                return redirect()->back()->withInput();
            }            
            if(isset($Quote->id))
            {
                Session::flash('success', 'Successfully stored in database');
                return redirect()->back();
            }
        }
}
 public function checkCaptcha() {
   session_start();
            if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&& $_SESSION["code"]==$_POST["captcha"])
            {
                echo 'success';
            }else{
                echo 'error';
            }
 }
    public function job_apply($position_name)
    {
        return view('website.job_apply',compact('position_name'));
    }

    public function job_post(Request $request,$position_name) 
    {


        $rule = array('name'    => 'required',
            'email'    => 'required', 
            'subject'    => 'required',            
            'phone'    => 'required',
            'cv_name'=>'required|max:500',
            'cover_letter' => 'required'); 

        $messages = array('name.required' => 'Please enter your name.',
                        'email.required' => 'Please fill your email Address.',
                        'subject.required' => 'Please enter your subject.',
                         'phone.required' => 'Please enter your phone number.',
                          'cv_name.required' => 'Please upload your resume.',
                           'cover_letter.required' => 'Please mention your cover letter.');  
        
        $validation = Validator::make($request->all(), $rule,$messages);

        if ( $validation->fails() )
        {

            return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        else
        {
            session_start();
            if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&$_SESSION["code"]==$_POST["captcha"])
            {
                $now = new DateTime();
                $JobApply = new JobApply();   //contact=model

                $resume = $request->file('cv_name');

                $current_year=date("Y");
                $current_month=date("m");
                $destinationPath='uploads/Resume/'.$current_year.'/'.$current_month;
                if (!file_exists($destinationPath))
                {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $extension = $resume->getClientOriginalExtension(); 
               
                if ($extension=="pdf"||$extension=="doc"||$extension=="docx") {
                    $random_number = mt_rand(10000, 99999);
                    $fileName='Resume'.$random_number.'.'.$extension; // renameing image
                    $resume->move($destinationPath,$fileName);

                    $JobApply->name = $request->input('name');
                    $JobApply->email = $request->input('email');
                    $JobApply->subject = $request->input('subject');
                    $JobApply->phone = $request->input('phone');
                    $JobApply->file_path = $destinationPath;
                    $JobApply->cv_name = $fileName; 
                    $JobApply->position = $position_name;               
                    $JobApply->cover_letter=$request->input('cover_letter');
                    $JobApply->status=1;
                    $JobApply->created_at=$now;
                    $JobApply->save();

                    $JobApply->id;
                }
                else{
                     Session::flash('wrong', 'File format is invalid, please attach .doc, .docx or .pdf file!'); 
                return redirect()->back()->withInput();          
                }
            }
             else{
                Session::flash('alert', 'Please enter the correct captcha'); 
                return redirect()->back();
            }
            if(isset($JobApply->id))
            {
                $applicant=JobApply::where('id',$JobApply->id)->where('status',1)->first();
                $candidate['name']=$applicant->name;
                $candidate['email']=$applicant->email;
                $candidate['position']=$applicant->position;
                $candidate['path']= $applicant->file_path."/".$applicant->cv_name;

                Mail::send('website.emails.job_apply', $candidate, function($message) use ($candidate)
                {
                    $message->from('do-not-reply@acestranetworks.com', 'Acestra Network Recruiting Team');
                    $message->to($candidate['email'], $candidate['name']);
                    $message->subject('Job Apply Acknowledgement');;
                });

                Mail::send('website.emails.job_request', $candidate, function($message) use ($candidate)
                {
                    $message->from('do-not-reply@acestranetworks.com', 'Acestra Network Recruiting Team');
                    $message->to(config('mail.sent_to'), 'Acestra');
                    $message->cc(config('mail.sent_cc'), 'Acestra');
                    $message->subject('Job Request');
                    $message->attach($candidate['path']);

                });

                Session::flash('success', 'Successfully stored in database');
                return redirect()->back()->withInput();

            }
        }

}
public function subsciber_post(Request $request)
{

     $rule = array('email_id'=> 'required'); 

        $messages = array('email_id.required' => 'Please fill your email Address.');  
        
        $validation = Validator::make($request->all(), $rule,$messages);

        if ( $validation->fails() )
        {
          echo "error";
            // return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        else
          {
            $email= $request->input('email_id');
             $now = new DateTime();
            // echo $email ;
            // die();
              $Subscriber_check=Subscriber::where('email_id',$email)->first();

              if($Subscriber_check)
                {
                  // Session::flash('alert', 'Email already exists'); 
                  // return redirect()->back()->withInput();
                    echo "email_alreay";
                }
              else
                {
                  $Subscriber= new Subscriber();
                  $Subscriber->email_id=$email;
                  $Subscriber->created_at=$now;
                  $Subscriber->save();
                  // Session::flash('success', 'Subscription done');
                   // return redirect()->back()->withInput();
                echo "success";
                }
          }
}


    public function index()
    {
        $data['title'] = 'Home'; 
        // $data['test'] = DB::table('ja_sliders')->get();
        return view('website.index',$data);
    }
    public function privacy()
    {
        $data['title'] = 'Privacy'; 
        return view('website.privacy',$data);
    }
    public function terms()
    {
         $data['title'] = 'Terms & Conditions'; 
        return view('website.terms',$data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        echo $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function aboutUs()
    {
        $data['title'] = 'About Us'; 
        return view('website.Aboutus', $data);
    }
    public function english()
    {
        $data['title'] = 'About Us'; 
        return view('website.Aboutus', $data);
    }    
    public function french()
    {
        $data['title'] = 'À propos de nous'; 
        return view('website.french', $data);
    }   
    public function arabic()
    {
        $data['title'] = 'À propos de nous'; 
        return view('website.arabic', $data);
    }        
    public function services()
    {
        $data['title'] = 'Services';  
        return view('website.Services', $data);
    }   
    public function products()
    {
        $data['title'] = 'Products'; 
        return view('website.Products', $data);
    }        
    public function careers()
    {
        $data['title'] = 'Careers'; 
        return view('website.Careers', $data);
    }       
    public function blog()
    {
        return view('website.Blog', $data);
    }      
    public function quote()
    {
        $data['title'] = 'Quote'; 
        return view('website.Quote', $data);
    }            
    public function contact()
    {
        $data['title'] = 'Contact'; 
        return view('website.Contact', $data);
    }      

    public function getSubscriberMail()
    {
        $current_date= CommonClass::CommonDate();  
        $postdata = file_get_contents("php://input");

        $data = json_decode($postdata);

        $email= $data->sub_email;


        $Subscriber_check=Subscriber::where('email_id',$email)->first();

        if($Subscriber_check)
        {
           echo "email_exists";
           // echo "This email is already registered";
       }
       else{
          $Subscriber= new Subscriber();
          $Subscriber->email_id=$email;
          $Subscriber->created_at=$current_date;
          $Subscriber->save();
          echo "success";
          //echo "Thank you for your subscription.";
      }

  }

  public function contact_user_insert()
  {
   $current_date= CommonClass::CommonDate();  

   $postdata = file_get_contents("php://input");

   $data = json_decode($postdata);

   $captcha_real= $data->captcha_real;
   $captcha_user= $data->captcha_user;

   if($captcha_user==$captcha_real)
   {
            //get value from angularjs
       $user_name= $data->name;
       $email= $data->email;
       $subject= $data->subject;
       $message= $data->message;

            //stored in database using model
       $Contact= new Contact();
       $Contact->name=$user_name;
       $Contact->email=$email;
       $Contact->subject=$subject;
       $Contact->message=$message;
       $Contact->status='1';
       $Contact->created_at=$current_date;
       $Contact->save();
       echo "success";
         // echo "Thank you for contact us...";
   }
   else
   {
    echo "captcha_not_match";
}

}

public function get_refresh_captcha()
{
    $cap_text =  rand(10000,99000);
    return $cap_text;
}


    // public function request_quote_insert()
    // {

    //    $captcha_real=input::get('captcha_real');

    //    $captcha_user=input::get('captcha_user');

    //    if($captcha_user==$captcha_real)
    //    {

    //        $current_date= CommonClass::CommonDate();   

    //        $user_name= input::get('name');
    //        $email= input::get('email');
    //        $phone= input::get('phone');
    //        $subject= input::get('subject');
    //        $month= input::get('month');
    //        $file=input::file('file');
    //        $file_size = Input::file('file')->getSize();
    //        $message= input::get('message');
    //       //echo "string";
    //         //die();
    //        if($file!='')
    //        {

    //         $current_year=date("Y");

    //         $current_month=date("m"); 

    //         $file_path=$current_year.'/'.$current_month;

    //         $destinationPath = 'web/uploads/quote/'.$file_path.'/';

    //            // $destinationPath = 'web/images/';

    //             $extension = $file->getClientOriginalExtension(); // getting image extension

    //             $extensions = array("jpeg","jpg","pdf","doc","docx"); 

    //             if(in_array($extension,$extensions )=== false){
    //              $errors[]="This extension not allowed, please choose a correct file.";
    //          }   
    //           if($file_size > 2097152){
    //             $errors[]='File size cannot exceed 2 MB';
    //             }   

    //          if(empty($errors)==true){

    //                 $alphanumeric_val=CommonClass::alphanumericValue();//Call 

    //                 $fileName='quote-'.$alphanumeric_val.'.'.$extension; // renameing image

    //                 $file->move($destinationPath,$fileName);

    //                 $RequestQuote= new RequestQuote();
    //                 $RequestQuote->name=$user_name;
    //                 $RequestQuote->email=$email;
    //                 $RequestQuote->phone=$phone;
    //                 $RequestQuote->expected_project_start_date=$month;
    //                 $RequestQuote->subject=$subject;
    //                 $RequestQuote->message=$message;

    //                 $RequestQuote->file_path=$file_path;
    //                 $RequestQuote->document_name=$fileName;

    //                 $RequestQuote->status='1';
    //                 $RequestQuote->created_at=$current_date;
    //                 $RequestQuote->save();
    //             }
    //             else
    //             {
    //               return "file_error";
    //             }
    //         }

    //         if($RequestQuote->id)
    //         {
    //             return "success";
    //         }

    //     }
    //     elseif($captcha_user=='undefined')
    //     {
    //       die();
    //     }
    //    elseif($captcha_user!='') //captcha not matched
    //     {
    //         return "captcha_not_match";
    //     }

    // }

public function job_apply_insert($position_name)
{
  $captcha_real=input::get('captcha_real');

  $captcha_user=input::get('captcha_user');

  if($captcha_user==$captcha_real)
  {
   $current_date= CommonClass::CommonDate();   

   $user_name= input::get('name');
   $email= input::get('email');
   $phone= input::get('phone');
   $subject= input::get('subject');
   $month= input::get('month');
   $file=input::file('file');
   $cover_letter= input::get('cover_letter');

   if($file!='')
   {

    $current_year=date("Y");

    $current_month=date("m"); 

    $file_path=$current_year.'/'.$current_month;

    $destinationPath = 'web/uploads/cv/'.$file_path.'/';

               // $destinationPath = 'web/images/';

                $extension = $file->getClientOriginalExtension(); // getting image extension

                $extensions = array("pdf","doc","docx"); 

                if(in_array($extension,$extensions )=== false){
                 $errors="This extension not allowed, please choose a correct file.";
             }   

             if(empty($errors)==true){

                    $alphanumeric_val=CommonClass::alphanumericValue();//Call 
                    
                    $fileName='resume-'.$alphanumeric_val.'.'.$extension; // renameing image

                    $file->move($destinationPath,$fileName);

                    $JobApply= new JobApply();
                    $JobApply->name=$user_name;
                    $JobApply->email=$email;
                    $JobApply->phone=$phone;
                    $JobApply->position=$position_name;
                    $JobApply->subject=$subject;
                    $JobApply->cover_letter=$cover_letter;

                    $JobApply->file_path=$file_path;
                    $JobApply->cv_name=$fileName;

                    $JobApply->status='1';
                    $JobApply->created_at=$current_date;
                    $JobApply->save();
                    $new_applicant_id=$JobApply->id;

                    echo "success";
                }
                else{
                   return "file_error";
               }
           }
           /*if($new_applicant_id)
           {
              $applicant=JobApply::where('id',$new_applicant_id)->where('status',1)->first();
              $candidate['name']=$applicant->name;
              $candidate['email']=$applicant->email;
              $candidate['position']=$applicant->position;

              $mail_notification=Mail::send('website.emails.job_apply', $candidate, function($message) use ($candidate)
              {
                  $message->from('do-not-reply@acestranetworks.com', 'Acestra Network Recruiting Team');
                  $message->to($candidate['email'], $candidate['name']);
                  $message->subject('Job Apply Reply');;
              });

              return "success";
          }*/
      }
      elseif($captcha_user=='undefined')
      {
          die();
      }
       elseif($captcha_user!='') //captcha not matched
       {
        return "captcha_not_match";
    }

   }      

   public function testmail() {
                
               
                $applicant=JobApply::where('id',39)->where('status',1)->first();
                $candidate['name']=$applicant->name;
                $candidate['email']=$applicant->email;
                $candidate['position']=$applicant->position;
                 $candidate['path']= $applicant->file_path."/".$applicant->cv_name;

                Mail::send('website.emails.job_apply', $candidate, function($message) use ($candidate)
                {
                    $message->from('do-not-reply@acestranetworks.com', 'Acestra Network Recruiting Team');
                    $message->to(config('mail.sent_to'), $candidate['name']);
                    $message->subject('Job Apply Reply');;
                });

                 Mail::send('website.emails.job_request', $candidate, function($message) use ($candidate)
                {
                    $message->from('do-not-reply@acestranetworks.com', 'Acestra Network Recruiting Team');
                    $message->to(config('mail.sent_to'), $candidate['name']);
                    $message->subject('Job Request');
                    $message->attach($candidate['path']);

                });

   }
}
