<?php namespace App\Http\Controllers;
//namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Redirect;
use Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Models\Contact;
use App\Models\quote;
use App\Models\JobApply;
use App\Models\Subscriber;
use App\Models\postjob;

class AdminController extends Controller {
	public function __construct()
	{
		$this->middleware('auth');
	}
	public function index()
	{
		echo $user = Auth::user('name');
		die();
		// return view('admin/dashboard');
	}
	public function admin()
	{
	
		$total_Contact_count = Contact::count();
		$total_RequestQuote_count = quote::count();
		$total_JobApply_count = JobApply::count();
		$analyst_programmer_count=JobApply::where('position','analyst_programmer')->count();

		$web_developer_count=JobApply::where('position','web_application_developer')->count();
		$project_coordinator_count=JobApply::where('position','project_co-ordinator')->count();
		$copy_writer_count=JobApply::where('position','copy_writer')->count();


		return view('admin/dashboard',compact('total_Contact_count','total_RequestQuote_count','total_JobApply_count',
			'analyst_programmer_count','web_developer_count','project_coordinator_count','copy_writer_count'));
	}
	
        public function register()
        {
            
            return view('auth/register');
        }

	public function visitors_index()
	{
		//$visitors=Contact::where('status','1')->orderBy('id','desc')->paginate(10);
		/*echo "<pre>";
		print_r($visitors);
		die();*/
		return view('admin/visitors');
	}

	public function subscriber_list()
	{
		
		return view('admin/subscriber_list');
	}

	public function quote_index()
	{
		//$quote_info=RequestQuote::where('status','1')->orderBy('id','desc')->paginate(10);
		/*echo "<pre>";
		print_r($quote_info);
		die();*/
		return view('admin/project_portfolio');
	}

	public function adminpostJob()
	{
		
		return view('admin/adminpostJob');
	}

	public function job_board_index()
	{
		//$job_board_info=JobApply::where('status','1')->orderBy('id','desc')->paginate(10);
		/*echo "<pre>";
		print_r($job_board_info);
		die();*/
		return view('admin/job_board');
	}

	public function add_job_data () {
          
          $rule = array('location'    => 'required', 
            'description'    => 'required',            
            'closing'    => 'required',
            'requirements'=>'required',
            'opening' => 'required'); 

       /* $messages = array('job_id.required' => 'Please enter your name.',
                        'location.required' => 'Please fill your email Address.',
                        'description.required' => 'Please enter your subject.',
                         'closing.required' => 'Please enter your phone number.',
                         'requirements.required' => 'Please upload your resume.',
                           'opening.required' => 'Please mention your cover letter.');  */
        
        $validation = Validator::make(Input::get(), $rule);

        if ( $validation->fails() )
        {
        
            $res= array('status'=>'wrong','data'=> $validation->errors());
                   
           
             //return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        else
        {
          $id = Input::get('id');
          if(isset($id)){
            $postjob = postjob::find($id);	
          } else{
          	$postjob = new postjob();
          	$get_new_job_id = postjob::get_new_job_id();
            $postjob->job_id = $get_new_job_id;
          }
          
          
          $postjob->title = Input::get('title');
          $postjob->location =  Input::get('location');
          $postjob->description =  Input::get('description');
          $postjob->traning =  Input::get('traning');
          $postjob->requirements =  Input::get('requirements');
          $postjob->opening =  Input::get('opening');
          $postjob->closing =  Input::get('closing');
          $postjob->save();

           $res= array('status'=>'success','data'=> $postjob);

          }

           echo json_encode($res);

	}
	public function posted_jobs () {
		  $postjob = postjob::get();
		  $res= array('status'=>'success','data'=> $postjob);
		  echo json_encode($res);

	}

	public function edit_posted_jobs () {
		   $id = Input::get('id');
		   $postjob = postjob::find($id);
		   $res= array('status'=>'success','data'=> $postjob);
		   echo json_encode($res);
	}

	public function status_posted_jobs () {
		   $id = Input::get('id');
		   $status = Input::get('status');
		   $postjob = postjob::find($id);
		   $postjob->status = $status;
		   $postjob->save();
		   $res= array('status'=>'success','data'=> $postjob);
		   echo json_encode($res);
	}

	public function get_request_quote_data()
	{
		$quote_info=quote::where('status','1')->orderBy('id','desc')->get();
		echo json_encode($quote_info);
	}

	public function get_contact_us_data()
	{
		$visitors=Contact::where('status','1')->orderBy('id','desc')->get();
		echo json_encode($visitors);
	}

	public function get_apply_job_data()
	{
		$job_board_info=JobApply::where('status','1')->orderBy('id','desc')->get();
		echo json_encode($job_board_info);
	}

	public function get_subscriber_list()
	{
		$subscribers=Subscriber::orderBy('id','desc')->get();
		echo json_encode($subscribers);
	}

	public function get_job_applied_details($job_apply_id)
	{
		
		$person_details=JobApply::where('id',$job_apply_id)->first();
		
		return view('admin/job_board',compact('person_details'));

	}

	public function get_contact_person_details($contact_id)
	{
		$contact_details=Contact::where('id',$contact_id)->first();
		
		return view('admin/visitors',compact('contact_details'));

	}

	public function get_project_quote_details($quote_id)
	{
		$quote_details=quote::where('id',$quote_id)->first();
		
		return view('admin/project_portfolio',compact('quote_details'));

	}
	


	
} 

