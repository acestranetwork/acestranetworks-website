<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use DateTime;
use Input;
use App\Http\Requests;
use App\Models\Contact;
use App\Models\quote;
use App\Models\JobApply;
use App\Models\Subscriber;
use File;
use DB;
use Mail;
use App\Models\postjob;

class WebsiteV2Controller extends Controller
{
    public function index()
    {
        $data['title'] = 'Home';
        // $data['test'] = DB::table('ja_sliders')->get();
        return view('websiteV2.index',$data);
    }
    public function privacy()
    {
        $data['title'] = 'Privacy';
        return view('websiteV2.privacy',$data);
    }
    public function terms()
    {
         $data['title'] = 'Terms & Conditions';
        return view('websiteV2.terms',$data);
    }
    public function aboutUs()
    {
        $data['title'] = 'About Us';
        return view('websiteV2.Aboutus', $data);
    }
    public function english()
    {
        $data['title'] = 'About Us';
        return view('websiteV2.Aboutus', $data);
    }
    public function french()
    {
        $data['title'] = 'À propos de nous';
        return view('websiteV2.french', $data);
    }
    public function arabic()
    {
        $data['title'] = 'معلومات عنا';
        return view('websiteV2.arabic', $data);
    }
    public function services()
    {
        $data['title'] = 'Services';
        return view('websiteV2.services', $data);
    }
    public function products()
    {
        $data['title'] = 'Products';
        return view('websiteV2.Products', $data);
    }
    public function portfolio()
    {
        $data['title'] = 'Portfolio';
        return view('websiteV2.portfolio', $data);
    }
    public function careers()
    {
        $data['title'] = 'Careers';
        $data['jobs'] = postjob::where('status','=',1)->get();
        return view('websiteV2.Careers', $data);
    }

    public function quote()
    {
        $data['title'] = 'Quote';
        return view('websiteV2.Quote', $data);
    }
    public function contact()
    {
        $data['title'] = 'Contact';
        return view('websiteV2.Contact', $data);
    }

    public function job_apply($position_name)
    {
        $data['title'] = 'Career Section'; 
        $data['jobs'] = postjob::where('status','=',1)->get();
        return view('websiteV2.job_apply',compact('position_name'),$data);
    }

    public function job_post(Request $request,$position_name)
    {
        $rule = array('name'    => 'required',
            'email'    => 'required|email',
            'subject'    => 'required',
            'phone'    => 'required',
            'cv_name'=>'required',
            'cover_letter' => 'required');

        $messages = array('name.required' => 'Please enter your name.',
                        'email.required' => 'Please fill your email Address.',
                        'subject.required' => 'Please enter your subject.',
                         'phone.required' => 'Please enter your phone number.',
                       'cv_name.required' => 'Please upload your resume.',
                           'cover_letter.required' => 'Please mention your cover letter.');

        $validation = Validator::make($request->all(), $rule,$messages);

        if ( $validation->fails() )
        {

            $res= array('status'=>'wrong','data'=> $validation->errors());

            //echo json_encode($res);
             return Redirect::back()->withErrors($validation)->withInput($request->all());
        }
        else
        {

            session_start();
            if(isset($_POST["captcha"])&&$_POST["captcha"]!=""&&$_SESSION["code"]==$_POST["captcha"])
            {
                $now = new DateTime();
                $JobApply = new JobApply();   //contact=model

                $resume = $request->file('cv_name');

                $current_year=date("Y");
                $current_month=date("m");
                $destinationPath='uploads/Resume/'.$current_year.'/'.$current_month;
                if (!file_exists($destinationPath))
                {
                    File::makeDirectory($destinationPath, 0777, true);
                }
                $extension = $resume->getClientOriginalExtension();

                 $size = $resume->getSize();

                if ($extension=="pdf"||$extension=="doc"||$extension=="docx") {


                  if($size <= 5242880)
                    {

                    $random_number = mt_rand(10000, 99999);
                    $fileName='Resume'.$random_number.'.'.$extension; // renameing image
                    $resume->move($destinationPath,$fileName);

                    $JobApply->name = $request->input('name');
                    $JobApply->email = $request->input('email');
                    $JobApply->subject = $request->input('subject');
                    $JobApply->phone = $request->input('phone');
                    $JobApply->file_path = $destinationPath;
                    $JobApply->cv_name = $fileName;
                    $JobApply->position = $request->input('post');
                    $JobApply->cover_letter=$request->input('cover_letter');
                    $JobApply->status=1;
                    $JobApply->created_at=$now;
                    $JobApply->save();

                    $JobApply->id;
                    $res = array('status' => 'success', 'data' => $JobApply->id);
                    // echo json_encode($res);
                }
                  else
            {
                    $validation->getMessageBag()->add('cv_name', 'File size should be less than 5MB');
                    return Redirect::back()->withErrors($validation)->withInput($request->all());
            }
                 }
                else{
                      // $res= array('status'=>'wrong','data'=> 'File format is invalid, please attach .doc, .docx or .pdf file!');
                     // echo json_encode($res);
                    $validation->getMessageBag()->add('cv_name', 'File format is invalid, please attach .doc, .docx or .pdf file!');
                    return Redirect::back()->withErrors($validation)->withInput($request->all());
                }
            }
             else{

                      //$res= array('status'=>'alert','data'=> 'Please enter the correct captcha');
                     // echo json_encode($res);
                   $validation->getMessageBag()->add('captcha', 'Please enter the correct captcha');
                   return Redirect::back()->withErrors($validation)->withInput($request->all());
            }

            if( $request->input('post') == 'P1402'){
                config(['mail.sent_cc' => config('mail.P1402')]);
            }else if( $request->input('post') == 'P1405'){
                config(['mail.sent_cc' => config('mail.P1405')]);
            } else if( $request->input('post') == 'P1411'){
                config(['mail.sent_cc' => config('mail.P1411')]);
            }


            if(isset($JobApply->id))
            {
                $applicant=JobApply::where('id',$JobApply->id)->where('status',1)->first();
                $candidate['name']=$applicant->name;
                $candidate['email']=$applicant->email;
                $candidate['position']=$applicant->position;
                $candidate['path']= $applicant->file_path."/".$applicant->cv_name;

                Mail::send('website.emails.job_apply', $candidate, function($message) use ($candidate)
                {
                    $message->from('do-not-reply@acestranetworks.com', 'Acestra Networks Recruiting Team');
                    $message->to($candidate['email'], $candidate['name']);
                    $message->subject('Job Apply Acknowledgement');;
                });

                Mail::send('website.emails.job_request', $candidate, function($message) use ($candidate)
                {
                    $message->from('do-not-reply@acestranetworks.com', 'Acestra Networks Recruiting Team');
                    $message->to(config('mail.sent_to'), 'Acestra');
                    $message->cc(config('mail.sent_cc'), 'Acestra');
                    $message->subject('Job Request');
                    $message->attach($candidate['path']);

                });

                   $validation->getMessageBag()->add('success_msg', 'Job posted successfully');
                   return Redirect::back()->withErrors($validation);

            }
            $res= array('status'=>'wrong','data'=> 'File format is invalid, please attach .doc, .docx or .pdf file!');

        }
        $res= array('status'=>'wrong','data'=> 'File format is invalid, please attach .doc, .docx or .pdf file!');

        return redirect()->back();

    }
}
