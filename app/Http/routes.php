<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// For Admin
Route::get('/', 'LoginController@adminLogin');
Route::get('/adminRegister', 'LoginController@adminRegister');
Route::post('/adminRegister', 'LoginController@adminRegisterPost');
Route::get('/adminlogin', 'LoginController@adminLogin');
Route::post('/adminlogin', 'LoginController@adminLoginPost');
Route::any('/admin_logout', 'LoginController@adminLogout');
Route::any('/dashboard','AdminController@admin');
Route::any("register",'AdminController@register');
// Sarvesh
Route::any("admin_page",'AdminController@admin');
Route::any("admin/contact",'AdminController@visitors_index');
Route::any("admin/request_quote",'AdminController@quote_index');
Route::any("admin/job_apply",'AdminController@job_board_index');
Route::any("/admin/postJob",'AdminController@adminpostJob');
Route::any("/admin/add_job_data",'AdminController@add_job_data');
Route::any("/admin/posted_jobs",'AdminController@posted_jobs');
Route::any("/admin/edit_posted_jobs",'AdminController@edit_posted_jobs');
Route::any("/admin/status_posted_jobs",'AdminController@status_posted_jobs');

//Route::any('register','AdminController@register_user');

Route::any('request_quote_data','AdminController@get_request_quote_data');

Route::any('contact_us_data','AdminController@get_contact_us_data');

Route::any('apply_job_data','AdminController@get_apply_job_data');

//For get full details of the form

Route::any("admin/job_apply/{job_apply_id}",'AdminController@get_job_applied_details');

Route::any("admin/contact_us/{contact_id}",'AdminController@get_contact_person_details');

Route::any("admin/request_quote/{quote_id}",'AdminController@get_project_quote_details');

Route::any("admin/subcribe_info",'AdminController@subscriber_list');

Route::any("subscriber_details",'AdminController@get_subscriber_list');


/*Foe website version 2*/

if( env('APP_VERSION') == 'v2'){
      Route::get('/', 'WebsiteV2Controller@index');
      Route::get('index','WebsiteV2Controller@index');
      Route::get('aboutus','WebsiteV2Controller@aboutUs');
      Route::get('services','WebsiteV2Controller@services');
      Route::get('products','WebsiteV2Controller@products');
      Route::get('portfolio','WebsiteV2Controller@portfolio');
      Route::get('careers','WebsiteV2Controller@careers');
	//Route::get('blog','WebsiteController@blog');
	Route::get('quote','WebsiteV2Controller@quote');
	Route::get('contact','WebsiteV2Controller@contact');
	Route::any("career/{position_name}/job_apply",'WebsiteV2Controller@job_apply');
	Route::any("career/{position_name}/job_apply_insert",'WebsiteV2Controller@job_post');
	Route::any("career/{position_name}/job_apply_insert",'WebsiteV2Controller@job_post');
	Route::any("/privacy",'WebsiteV2Controller@privacy');
    Route::any("/terms",'WebsiteV2Controller@terms');
	Route::get('english','WebsiteV2Controller@english');
	Route::get('french','WebsiteV2Controller@french');
	Route::get('arabic','WebsiteV2Controller@arabic');
	Route::get('errors',function(){
		return view('errors.503');
	});

}else{
	// For web
	Route::get('/', 'WebsiteController@index');
	Route::get('index','WebsiteController@index');
	Route::get('aboutus','WebsiteController@aboutUs');
	Route::get('products','WebsiteController@products');
	Route::get('services','WebsiteController@services');
	Route::get('careers','WebsiteController@careers');
	//Route::get('blog','WebsiteController@blog');
	Route::get('quote','WebsiteController@quote');
	Route::get('contact','WebsiteController@contact');
	Route::get('english','WebsiteController@english');
	Route::get('french','WebsiteController@french');
	Route::get('arabic','WebsiteController@arabic');
	Route::any("career/{position_name}/job_apply",'WebsiteController@job_apply');
	Route::any("career/{position_name}/job_apply_insert",'WebsiteController@job_post');

	Route::any("/privacy",'WebsiteController@privacy');
    Route::any("/terms",'WebsiteController@terms');

}

Route::any("/contact_post",'WebsiteController@contact_post');
Route::any("captcha",'WebsiteController@captcha'); // captcha
Route::any("checkCaptcha",'WebsiteController@checkCaptcha'); // captcha
Route::any("quote_post",'WebsiteController@quote_post');

Route::any("subsciber_post",'WebsiteController@subsciber_post');



Route::any("testmail",'WebsiteController@testmail');
