<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use App\CustomStuff\CustomDirectory\AdminLog;
use Illuminate\Support\Facades\Session;

class MultiAuth {

	
	public function handle($request, Closure $next)
	{
            
		 if (Auth::admin()->guest())
		{
                    
			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
                            
				return redirect()->guest('login');
			}
                         
		}
            
		return $next($request);
	}

}
