<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class postjob extends Model
{
    //

    public static function get_new_job_id() {
    	$word = 'P';
        $postjob = postjob::max('id');
        $new = postjob::find($postjob);
        $new_id = explode($word, $new->job_id);
        $conj =  $new_id[1]+1;
        $new_job_id = $word.$conj;
        return $new_job_id;
    }
}
